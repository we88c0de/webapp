import { DEFAULT_TITLE, DEFAULT_IMAGE, DEFAULT_DESCRIPTION } from './constants';

const hydrateHTML = (indexHTML: string, newTitle: string, newDescription?: string, newOgImage?: string) => {
  const escape = string => string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  const titleRegex = new RegExp(escape(DEFAULT_TITLE), 'g');
  const imageRegex = new RegExp(escape(DEFAULT_IMAGE), 'g');
  const descriptionRegex = new RegExp(escape(DEFAULT_DESCRIPTION), 'g');

  let hydratedHTML = indexHTML.replace(titleRegex, newTitle);

  if (newDescription) hydratedHTML = hydratedHTML.replace(descriptionRegex, newDescription)
  if (newOgImage) hydratedHTML = hydratedHTML.replace(imageRegex, newOgImage);

  return hydratedHTML;
};

export default hydrateHTML;
