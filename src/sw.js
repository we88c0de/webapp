/* eslint-disable */

self.addEventListener('push', function (event) {
  const { title, ...payload } = event.data.json();

  const promiseChain = registration.getNotifications()
    .then(notifications => {
      let currentNotification;

      for (let i = 0; i < notifications.length; i++) {
        if (
          notifications[i].tag === payload.tag
          || (payload.type === 'chatread' && notifications[i].tag === 'chat')
          || (payload.type === 'alertsread' && notifications[i].tag === 'alert')
        ) {
          currentNotification = notifications[i];
        }
      }

      return currentNotification;
    })
    .then((currentNotification) => {
      let notificationTitle = title;
      let send = true;

      let options = {
        vibrate: [200, 100, 200],
        badge: '/images/monologo.png',
        renotify: !!payload.tag,
      };

      if (currentNotification && payload.type === 'chatread') {
        const chats = currentNotification.data.chats.filter(c => c.messengerId !== payload.data.messengerId);
        if (chats.length > 0) {
          options.data = { chats };
          options.tag = 'chat';
          options.renotify = false;
          options.vibrate = undefined;
          options.silent = true;

          if (options.data.chats.length > 1) {
            options.body = options.data.chats.map(c => `${c.displayname}: ${c.lastMessage}`).join('\n');
            options.icon = 'https://mazmo.net/images/isologo.png';
            notificationTitle = `${options.data.chats.length} nuevos mensajes`;
          } else {
            options.body = options.data.chats[0].lastMessage;
            options.icon = options.data.chats[0].avatar;
            notificationTitle = options.data.chats[0].displayname;
          }
        } else {
          send = false;
        }

        currentNotification.close();
      } else if (payload.type === 'alertsread') {
        send = false;
        if (currentNotification) currentNotification.close();
      } else if (currentNotification && currentNotification.tag === 'alert' && currentNotification.data.alertId !== payload.data.alertId) {
        const count = (currentNotification.data.count || 1) + 1;
        const currentBody = count > 2 ? currentNotification.body : currentNotification.title;
        const newBody = (title && title !== 'Mazmo') ? title : payload.body;
        options.body = `${currentBody}\n${newBody}`;
        options.data = { count };
        options.tag = 'alert';
        options.icon = 'https://mazmo.net/images/isologo.png';
        notificationTitle = `${count} nuevas alertas`;

        currentNotification.close();
      } else if (currentNotification && payload.tag === 'chat') {
        const chat = {
          messengerId: payload.data.messengerId,
          lastMessage: payload.body,
          displayname: title,
          avatar: payload.icon,
        };
        const otherChats = currentNotification.data.chats.filter(c => c.messengerId !== payload.data.messengerId);
        options.data = { chats: [chat, ...otherChats] };
        options.tag = 'chat';

        if (options.data.chats.length > 1) {
          options.body = options.data.chats.map(c => `${c.displayname}: ${c.lastMessage}`).join('\n');
          options.icon = 'https://mazmo.net/images/isologo.png';
          notificationTitle = `${options.data.chats.length} nuevos mensajes`;
        } else {
          options.body = `${currentNotification.body}\n${payload.body}`;
          options.icon = payload.icon;
          notificationTitle = title;
        }

        currentNotification.close();
      } else if (payload.tag === 'chat') {
        const chat = {
          messengerId: payload.data.messengerId,
          lastMessage: payload.body,
          displayname: title,
          avatar: payload.icon,
        };
        payload.data = { chats: [chat] };
        options = {
          ...options,
          ...payload,
        }
      } else {
        options = {
          ...options,
          ...payload
        }
      }

      if (send) {
        return registration.showNotification(
          notificationTitle,
          options
        );
      }
    });

  event.waitUntil(promiseChain);
});

self.addEventListener('notificationclick', function(event) {
  const clickUrl = (event.notification.data.clickUrl || '').replace('https://mazmo.net', '');
  const urlToOpen = new URL(clickUrl, self.location.origin).href;

  const promiseChain = clients.matchAll({
    type: 'window',
    includeUncontrolled: true
  }).then((windowClients) => {
    let matchingClient = null;

    for (let i = 0; i < windowClients.length; i++) {
      const windowClient = windowClients[i];
      if (windowClient.url.includes(self.location.origin)) {
        matchingClient = windowClient;
        break;
      }
    }

    event.notification.close();

    if (matchingClient) {
      return matchingClient.navigate(urlToOpen).focus();
    } else {
      return clients.openWindow(urlToOpen);
    }
  });

  event.waitUntil(promiseChain);
});
