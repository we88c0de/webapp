import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';
import { SelectableListItem } from 'components/SelectableList';

const User = ({ userId }) => {
  const user = useSelector(userSelectors.getById(userId), shallowEqual);

  const avatar = useMemo(() => (
    <UserAvatar userId={userId} />
  ), [userId]);

  const displayname = useMemo(() => (
    <UserDisplayName userId={userId} />
  ), [userId]);

  return (
    <SelectableListItem
      avatar={avatar}
      title={displayname}
      to={`/@${user.username}`}
    />
  );
};

User.propTypes = {
  userId: PropTypes.number.isRequired,
};

export default User;
