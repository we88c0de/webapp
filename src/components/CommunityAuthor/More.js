import styled from 'styled-components';

const More = styled.div`
  color: #6F6666;
  font-size: 14px;
`;
More.displayName = 'More';

export default More;
