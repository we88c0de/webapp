import React from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation } from 'hooks';
import colors from 'utils/css/colors';
import Button from 'components/Button';

import locales from './i18n';
import ModalOpacity from './ModalOpacity';

const Box = styled.div`
  background-color: white;
  padding: 32px;
  border-radius: 8px;
  width: calc(100% - 16px * 2);
  max-height: calc(100% - 16px * 2);
  max-width: 600px;
  overflow: auto;
  box-sizing: border-box;
  z-index: 1200;
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  ${props => props.fullHeight && `
    height: calc(100% - 16px * 2);
  `}
`;

const Content = styled.div`
  color: ${colors.blackRed};
  display: flex;
  flex-direction: column;
  ${props => props.fullHeight && `
    height: 100%;
  `}
`;

const Title = styled.span`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 16px;
`;

const Wrapper = styled.div`
  ${props => props.fullHeight && `
    flex: 1;
    overflow: hidden;
  `}
`;
Wrapper.displayName = 'Wrapper';

const ActionsWrapper = styled.div`
  color: ${colors.blackRed};
  margin-top: 32px;
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;

  @media(max-width: 768px) {
    button {
      margin-bottom: 16px;
    }
  }
`;

const Modal = ({
  show,
  title,
  actions,
  onCancel,
  onClose,
  children,
  boxStyle,
  showActions,
  onlyActions,
  fullHeight,
  backgroundBlur,
}) => {
  const { t } = useTranslation(locales);

  return createPortal((
    <>
      <ModalOpacity show={show} onClick={onClose} backgroundBlur={backgroundBlur} />
      <Box style={boxStyle} fullHeight={fullHeight}>
        <Content fullHeight={fullHeight}>
          <Title>{title}</Title>
          <Wrapper fullHeight={fullHeight}>{children}</Wrapper>
          {showActions && (
            <ActionsWrapper>
              {onCancel && !onlyActions && <Button onClick={onCancel} className="empty">{t('global:Cancel')}</Button>}
              {onClose && !onlyActions && <Button onClick={onClose} className="empty">{t('Close')}</Button>}
              {actions}
            </ActionsWrapper>
          )}
        </Content>
      </Box>
    </>
  ), document.body);
};

Modal.propTypes = {
  show: PropTypes.bool,
  title: PropTypes.string,
  actions: PropTypes.arrayOf(PropTypes.node),
  onCancel: PropTypes.func,
  onClose: PropTypes.func,
  children: PropTypes.node,
  // eslint-disable-next-line react/forbid-prop-types
  boxStyle: PropTypes.object,
  showActions: PropTypes.bool,
  onlyActions: PropTypes.bool,
  fullHeight: PropTypes.bool,
  backgroundBlur: PropTypes.bool,
};

Modal.defaultProps = {
  show: true,
  title: '',
  actions: [],
  onCancel: null,
  onClose: null,
  children: null,
  boxStyle: {},
  showActions: true,
  onlyActions: false,
  fullHeight: false,
  backgroundBlur: false,
};

export default Modal;
