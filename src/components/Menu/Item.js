import PropTypes from 'prop-types';
import styled from 'styled-components';

const MenuItem = styled.div`
  ${props => props.danger && `
    color: ${props.theme.colors.main};
  `}

  a {
    width: 100%;
    height: 100%;
  }
`;

MenuItem.propTypes = {
  danger: PropTypes.bool,
};

MenuItem.defaultProps = {
  danger: false,
};

export default MenuItem;
