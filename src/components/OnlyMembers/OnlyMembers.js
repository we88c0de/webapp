import React from 'react';
import PropTypes from 'prop-types';

import EmptyState from 'components/EmptyState';

const OnlyMembers = ({ title, communityId }) => {
  return <EmptyState title={title} />;
};

OnlyMembers.propTypes = {
  title: PropTypes.string.isRequired,
  communityId: PropTypes.string.isRequired,
};

OnlyMembers.defaultProps = {
};

export default OnlyMembers;
