import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const WarningWrapper = styled.div`
  color: #856404;
  background-color: #fff3cd;
  border: 1px solid #ffeeba;
  border-radius: .25rem;
  margin: 16px 0px;
  padding: 8px;
`;
WarningWrapper.displayName = 'WarningWrapper';

const Warning = ({ children }) => (
  <WarningWrapper>
    {children}
  </WarningWrapper>
);

Warning.propTypes = {
  children: PropTypes.node.isRequired,
};

Warning.defaultProps = {
};

export default Warning;
