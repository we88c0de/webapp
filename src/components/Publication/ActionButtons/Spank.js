import React, {
  useCallback, useState, useEffect, useRef,
} from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';
import * as feedActions from 'state/feed/actions';
import * as appActions from 'state/app/actions';

import ActionIcon from 'components/ActionIcon';
import Hoverable from 'components/Hoverable';

import SpankButton from './SpankButton';
import { ActionWrapper } from '../UI';
import locales from '../i18n';

const Spank = ({ publicationId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const userHasSpanked = useSelector(feedSelectors.publications.userHasSpanked(publicationId));
  const hasLocktoberHashtag = useSelector(
    feedSelectors.publications.hasLocktoberHashtag(publicationId),
  );

  const [spanking, setSpanking] = useState(false);
  const [unspanking, setUnspanking] = useState(false);
  const spanked = !unspanking && (spanking || userHasSpanked);

  const [locktober, setLocktober] = useState(false);
  const locktoberTimer = useRef(null);
  const inited = useRef(false);

  const spankClick = useCallback(async () => {
    try {
      if (spanked) {
        setUnspanking(true);
        await dispatch(feedActions.deletePublicationReaction(publicationId));
        setUnspanking(false);
      } else {
        setSpanking(true);
        await dispatch(feedActions.createPublicationReaction(publicationId));
        setSpanking(false);
      }
    } catch (error) {
      setSpanking(false);
      setUnspanking(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, spanked, publicationId]);

  useEffect(() => {
    const timer = locktoberTimer.current;

    if (spanked && inited.current && hasLocktoberHashtag) {
      setLocktober(true);
      locktoberTimer.current = setTimeout(() => setLocktober(false), 2000);
    }

    if (!inited.current) inited.current = true;

    return () => {
      if (timer) clearTimeout(timer);
    };
  }, [spanked, hasLocktoberHashtag]);

  return (
    <ActionWrapper>
      <ActionIcon onClick={spankClick} animate locktober={locktober}>
        <div className="locktober">
          <div className="base">
            <div className="base-bottom" />
            <div className="lock-inside-top" />
            <div className="lock-inside-bottom" />
          </div>
          <div className="lock-cirlce">
            <div className="lock-circle-inside" />
          </div>
          <div className="lock-box" />
        </div>

        <Hoverable
          normal={<SpankButton publicationId={publicationId} outline={!spanked} />}
          hover={<SpankButton publicationId={publicationId} outline={spanked} />}
          tooltip={t(spanked ? 'global:Unspank' : 'global:Spank')}
        />
      </ActionIcon>
    </ActionWrapper>
  );
};

Spank.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Spank.defaultProps = {
};

export default Spank;
