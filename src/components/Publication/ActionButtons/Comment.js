import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import ActionIcon from 'components/ActionIcon';
import Hoverable from 'components/Hoverable';

import CommentButton from './CommentButton';
import { ActionWrapper } from '../UI';
import locales from '../i18n';

const Comment = ({ publicationId }) => {
  const { t } = useTranslation(locales);
  const history = useHistory();

  const userHasCommented = useSelector(feedSelectors.publications.userHasCommented(publicationId));

  const goToComments = () => {
    history.push(`/publications/${publicationId}?comment`);
  };

  return (
    <ActionWrapper>
      <ActionIcon onClick={goToComments}>
        <Hoverable
          normal={<CommentButton publicationId={publicationId} outline={!userHasCommented} />}
          hover={<CommentButton publicationId={publicationId} outline={false} />}
          tooltip={t('Comments')}
        />
      </ActionIcon>
    </ActionWrapper>
  );
};

Comment.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Comment.defaultProps = {
};

export default Comment;
