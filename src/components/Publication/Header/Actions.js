import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import ContextMenu from 'components/ContextMenu';
import SpankListModal from 'components/SpankListModal';

import Follow from '../Follow';
import CloseComments from '../CloseComments';
import RemoveModal from '../Modals/Remove';
import locales from '../i18n';

const Actions = ({ publicationId }) => {
  const { t } = useTranslation(locales);

  const userIsAuthor = useSelector(feedSelectors.publications.userIsAuthor(publicationId));

  const [showingSpanksModal, setShowingSpanksModal] = useState(false);
  const showSpanksModal = () => setShowingSpanksModal(true);
  const hideSpanksModal = () => setShowingSpanksModal(false);

  const [showingRemoveModal, setShowingRemoveModal] = useState(false);
  const showRemoveModal = () => setShowingRemoveModal(true);
  const hideRemoveModal = () => setShowingRemoveModal(false);

  const contextItems = [
    { key: 'showspanks', onClick: showSpanksModal, component: t('Show spanks') },
    { key: 'follow', component: <Follow publicationId={publicationId} /> },
  ];
  if (userIsAuthor) {
    contextItems.push({
      key: 'closecomments', component: <CloseComments publicationId={publicationId} />,
    });
    contextItems.push({
      key: 'remove',
      onClick: showRemoveModal,
      component: t('Remove'),
      danger: true,
    });
  }

  return (
    <>
      <ContextMenu
        items={contextItems}
        size={5}
      />

      {/* Modals */}
      {showingSpanksModal && (
        <SpankListModal type="publication" entityId={publicationId} close={hideSpanksModal} />
      )}
      {showingRemoveModal && (
        <RemoveModal publicationId={publicationId} close={hideRemoveModal} />
      )}
    </>
  );
};

Actions.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Actions.defaultProps = {
};

export default Actions;
