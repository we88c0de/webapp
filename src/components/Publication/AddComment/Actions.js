import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import * as appSelectors from 'state/app/selectors';

import { UploadMediaButton } from 'components/CommunityUploadMedia';

import SubmitButton from './SubmitButton';
import Wrapper from './UI/Actions';

const Actions = ({ openMedia, publicationId }) => {
  const id = `comment-${publicationId}`;

  const ref = useSelector(appSelectors.getComposerRef(id));
  const hasContent = useSelector(appSelectors.composerHasContent(id));

  const [hasFocus, setHasFocus] = useState(false);

  const showActions = hasFocus || hasContent;

  useEffect(() => {
    const handleFocus = () => { setHasFocus(true); };
    const handleBlur = () => { setTimeout(() => { setHasFocus(false); }, 100); };

    if (ref) {
      ref.on('focus', handleFocus);
      ref.on('blur', handleBlur);
    }
  }, [ref]);

  return (
    <Wrapper showing={showActions}>
      <div>
        <UploadMediaButton className="action left" open={openMedia} />
      </div>
      <SubmitButton publicationId={publicationId} />
    </Wrapper>
  );
};

Actions.propTypes = {
  publicationId: PropTypes.string.isRequired,
  openMedia: PropTypes.func.isRequired,
};

Actions.defaultProps = {
};

export default Actions;
