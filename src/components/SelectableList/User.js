import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';

import { SelectableListItem } from 'components/SelectableList';
import UserAvatar from 'components/UserAvatar';

const Role = styled.div`
  color: #666;
  font-size: 14px;
  margin-top: 6px;
`;

const UserItem = ({ userId, subtitle, actions }) => {
  const dispatch = useDispatch();
  const user = useSelector(userSelectors.getById(userId), shallowEqual);
  if (user.loading) dispatch(userActions.fetchData(userId));

  return (
    <SelectableListItem
      avatar={(<UserAvatar user={user} />)}
      title={user.displayname}
      actions={actions}
    >
      <Role>{subtitle}</Role>
    </SelectableListItem>
  );
};

UserItem.propTypes = {
  userId: PropTypes.number.isRequired,
  subtitle: PropTypes.string,
  actions: PropTypes.arrayOf(PropTypes.node),
};

UserItem.defaultProps = {
  subtitle: '',
  actions: [],
};

export default UserItem;
