import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import styled from 'styled-components';
import colors from 'utils/css/colors';

const Wrapper = styled.div`
  background-color: white;
  font-weight: 500;
  line-height: 73px;
  height: 73px;
  padding: 0 15px;
  box-shadow: 0px -4px 8px rgba(0, 0, 0, 0.08);
  cursor: pointer;
  user-select: none;
  flex-shrink: 0;
  margin-top: auto;

  a {
    color: ${colors.redReactions};
    text-decoration: none;
  }

  svg {
    width: 24px;
    height: 24px;
    margin-right: 8px;
    fill: ${colors.redReactions};
  }
`;

const SidebarButton = ({ link, children }) => (
  <Wrapper>
    <Link to={link}>{children}</Link>
  </Wrapper>
);

SidebarButton.propTypes = {
  link: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default SidebarButton;
