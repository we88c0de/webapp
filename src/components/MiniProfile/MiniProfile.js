import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import * as messengerActions from 'state/messengers/actions';
import * as authSelectors from 'state/auth/selectors';

import { useTranslation } from 'hooks';
import UserType from 'state/users/type';

import Modal from 'components/Modal';
import UserAvatar from 'components/UserAvatar';
import UserFollowButton from 'components/UserFollowButton';
import Button from 'components/Button';
import Loading from 'components/Loading';

import Counter from './Counter';
import locales from './i18n';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  margin-top: 45px;
`;

const Displayname = styled.div`
  font-weight: bold;
  font-size: 24px;
`;

const Username = styled.div`
  color: #666;
  margin-bottom: 30px;
`;

const Location = styled.div`
  font-size: 18px;

  img {
    height: 16px;
    margin-right: 8px;
  }
`;

const Description = styled.div`
  color: #333;

  ${props => props.secondary && `
    color: #999;
    font-size: 15px;
  `}
`;

const Counters = styled.div`
  margin: 28px 0;
  display: flex;
  justify-content: space-around;
`;

const avatarStyle = { position: 'absolute', top: '-75px', alignSelf: 'center' };

const MiniProfile = ({ user, close, history }) => {
  const { t } = useTranslation(locales);

  const dispatch = useDispatch();
  const messengerId = useSelector(messengerSelectors.getByUserId(user.id));
  const me = useSelector(authSelectors.getMe(), shallowEqual);

  const openProfile = () => {
    history.push(`/@${user.username}`);
    close();
  };

  const openDirectMessage = async () => {
    if (!me) {
      history.push('/login');
    } else {
      if (messengerId) history.push(`/chat/messengers/${messengerId}`);

      const messenger = await dispatch(messengerActions.create([user.id]));
      history.push(`/chat/messengers/${messenger.id}`);
    }
    close();
  };

  const description = [];
  if (!user.is_organization && user.gender) description.push(user.gender.text);
  if (!user.is_organization && user.role) description.push(user.role.text);
  if (!user.is_organization && user.orientation) description.push(user.orientation.text);
  if (!user.is_organization && (user.gender || user.role || user.orientation)) description.push('de');
  if (!user.is_organization) description.push(`${user.age} años`);

  const actions = [];

  if (me) {
    actions.push(
      <Button key="miniprofile-gotoprofile" onClick={openProfile} className="empty">{t('View Profile')}</Button>,
    );
  }

  if (!me || (!user.is_organization && me.id !== user.id)) {
    actions.push(
      <Button key="miniprofile-sendpm" onClick={openDirectMessage} className="empty">{t('Private Message')}</Button>,
    );
  }

  if (!me || me.id !== user.id) {
    actions.push(
      <UserFollowButton key="miniprofile-follow" userId={user.id} />,
    );
  }

  return (
    <Modal
      onClose={close}
      boxStyle={{ overflow: 'inherit' }}
      actions={actions}
      onlyActions
    >
      <Wrapper>
        {user.loading
          ? <Loading />
          : (
            <>
              <UserAvatar userId={user.id} size="150px" style={avatarStyle} showOnline={false} />
              <Displayname>{user.displayname}</Displayname>
              <Username>{`@${user.username}`}</Username>

              {!user.is_organization && (
                <>
                  <Location>
                    {user.countryISO && <img src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${user.countryISO.toLowerCase()}.png`} alt={user.country} />}
                    {user.location}
                  </Location>
                  <Description>{description.join(' ')}</Description>
                  <Description secondary>
                    {t('Pronoun')}
                    {': '}
                    {t(`global:PRONOUN.${user.pronoun}`)}
                  </Description>
                </>
              )}
              <Counters>
                {!user.is_organization && <Counter label={t('global:Known by')} number={user.known} />}
                {!user.is_organization && <Counter label={t('global:Events')} number={user.event_count} />}
                <Counter label={t('global:Followers')} number={user.followers} />
                <Counter label={t('global:Following')} number={user.following} />
              </Counters>
            </>
          )
        }
      </Wrapper>
    </Modal>
  );
};

MiniProfile.propTypes = {
  user: UserType.isRequired,
  close: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

MiniProfile.defaultProps = {
};

export default withRouter(MiniProfile);
