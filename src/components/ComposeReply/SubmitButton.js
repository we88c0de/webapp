import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import * as appSelectors from 'state/app/selectors';
import * as appActions from 'state/app/actions';

import { Send } from 'components/Icons';
import Button from 'components/Button';

const ButtonComponent = styled.button.attrs(props => ({
  onClick: props.onClick,
  loading: undefined,
}))`
  background-color: transparent;
  color: ${colors.grey};
  font-weight: bold;
  font-size: 14px;
  text-transform: uppercase;
  padding: 0;
  border: 0;
  display: flex;
  align-items: center;
  transition: all 250ms ease-out;
  cursor: pointer;
  flex-shrink: 0;

  &:focus {
    outline: none;
  }

  &:hover:not([disabled]) {
    color: ${colors.red};
    svg {
      fill: ${colors.red};
      transform: translate3d(5px, 0, 0);
    }
  }
`;

const SendWrapper = styled.div`
  width: 16px;
  margin-left: 8px;
  svg {
    fill: ${colors.grey};
    transition: all 250ms ease-out;
  }
`;

const SubmitText = styled.span`
  ${props => !props.big && `
    @media(max-width: 768px) {
      display: none;
    }
  `}
`;

const SubmitButton = ({
  id,
  text,
  afterSend,
  className,
  big,
  getData,
  buttonProps,
}) => {
  const dispatch = useDispatch();

  const canSubmit = useSelector(appSelectors.isEditorReadyToSubmit(id));
  const isCreating = useSelector(appSelectors.isEditorCreating(id));
  const disabled = !canSubmit || isCreating;

  const onButtonClick = async () => {
    const data = getData ? getData() : null;
    await dispatch(appActions.editorSendToServer(id, data));
    if (afterSend) afterSend();
  };

  const Component = big ? Button : ButtonComponent;

  return (
    <Component
      onClick={!disabled ? onButtonClick : undefined}
      disabled={disabled}
      className={className}
      loading={isCreating}
      {...buttonProps}
    >
      <>
        <SubmitText big={big}>{text}</SubmitText>
        {!big && (
          <SendWrapper>
            <Send color={disabled ? colors.grey : 'black'} />
          </SendWrapper>
        )}
      </>
    </Component>
  );
};

SubmitButton.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string,
  afterSend: PropTypes.func,
  className: PropTypes.string,
  big: PropTypes.bool,
  getData: PropTypes.func,
  buttonProps: PropTypes.shape({}),
};

SubmitButton.defaultProps = {
  text: 'Send',
  afterSend: null,
  className: '',
  big: false,
  getData: null,
  buttonProps: {},
};

export default SubmitButton;
