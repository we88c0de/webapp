import React, { useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';

import colors from 'utils/css/colors';
import { useUploadMedia, useTranslation } from 'hooks';

import { Gif } from 'components/Icons';
import Editor from 'components/Editor';
import CommunityUploadMedia, { DropZone, UploadMediaButton } from 'components/CommunityUploadMedia';

import Submit from './SubmitButton';
import GifSearch from './GifSearch';
import locales from './i18n';

const Wrapper = styled.div`
  line-height: 1.8;
  max-width: 100%;
  min-height: 56px;

  box-sizing: border-box;
  flex-shrink: 0;
  display: flex;
  justify-content: space-between;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  flex-direction: column;
  position: relative;

  ${props => (props.big ? `
    padding: 16px 16px 64px;
  ` : `
    border-top: 1px solid ${colors.borderRed};
    box-shadow: 0px -4px 8px rgba(0, 0, 0, 0.08);
    margin-top: auto;
    background-color: white;
    max-height: 40vh;
    padding: 16px 32px;
  `)}

  &:focus {
    outline: none;
  }

  @media(max-width: 768px) {
    ${props => !props.big && `
      padding: 5px 16px;
      height: auto;
    `}
  }
`;

const EditorWrapper = styled.div`
  display: flex;
  align-items: center;

  ${props => props.big && `
    > .action {
      position: absolute;
      bottom: 0;

      &.left {
        left: 24px;
        height: 40px;
      }
      &.right {
        right: 24px;
      }
    }
  `}

  @media(min-width: 768px) {
    flex-grow: 1;
  }
`;

const Action = styled.div`
  width: 24px;
  cursor: pointer;
  margin-right: 8px;

  &:hover {
    background-color: #f3f3f3;
  }
`;

const ComposeReply = React.forwardRef(({
  id,
  placeholder,
  submitOnEnter,
  onTypingState,
  uploadMediaPath,
  mentions,
  canGif,
  emoji,
  afterSend,
  big,
  getData,
  buttonProps,
}, ref) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const lastKeystrokeTime = useRef(0);
  const [suggestionsOpen, setSuggestionsOpen] = useState(false);
  const [showingGifSearch, setShowingGifSearch] = useState(false);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    open,
    handlePastedFiles,
  } = useUploadMedia(id, uploadMediaPath);

  const showGifSearch = useCallback(() => {
    setShowingGifSearch(true);
  }, [setShowingGifSearch]);
  const hideGifSearch = useCallback(() => {
    setShowingGifSearch(false);
  }, [setShowingGifSearch]);

  const sendGif = useCallback(async (gif) => {
    hideGifSearch();
    dispatch(appActions.editorSendToServer(id, { media: { gif } }));
  }, [dispatch, hideGifSearch, id]);

  const onContentChange = useCallback((state) => {
    if (onTypingState) {
      const isEmpty = (typeof state === 'string' && state.length === 0) || (typeof state !== 'string' && !state.getCurrentContent().hasText());
      const keystrokeReportEnlapsed = (
        (new Date()).getTime() - lastKeystrokeTime.current > (15 * 1000)
      );

      if (!isEmpty) {
        if (keystrokeReportEnlapsed) onTypingState('STARTED');
        lastKeystrokeTime.current = (new Date()).getTime();
      } else {
        if (!keystrokeReportEnlapsed) onTypingState('STOPPED');
        lastKeystrokeTime.current = 0;
      }
    }
  }, [lastKeystrokeTime, onTypingState]);

  const handleReturn = useCallback((e) => {
    if (submitOnEnter && !e.shiftKey && !suggestionsOpen) {
      const data = getData ? getData() : null;
      dispatch(appActions.editorSendToServer(id, data));
      lastKeystrokeTime.current = 0;
      return 'handled';
    }

    if (!submitOnEnter && e.metaKey) {
      const data = getData ? getData() : null;
      dispatch(appActions.editorSendToServer(id, data));
      lastKeystrokeTime.current = 0;
      return 'handled';
    }

    return 'not-handled';
  }, [dispatch, submitOnEnter, suggestionsOpen, id, getData]);

  const onSend = useCallback(() => {
    lastKeystrokeTime.current = 0;
    if (afterSend) afterSend();
  }, [afterSend]);

  return (
    <Wrapper {...getRootProps()} big={big}>
      <>
        {isDragActive && (
          <DropZone>{t('global:Drop the files here...')}</DropZone>
        )}

        <EditorWrapper big={big}>
          <UploadMediaButton className="action left" open={open} />
          {canGif && (
            <Action onClick={showGifSearch}>
              <Gif color={colors.grey} close={hideGifSearch} />
            </Action>
          )}
          <Editor
            ref={ref}
            id={id}
            placeholder={placeholder}
            onContentChange={onContentChange}
            handleReturn={handleReturn}
            handlePastedFiles={handlePastedFiles}
            emoji={emoji}
            linkify
            markdown
            onSuggestionsOpenChange={setSuggestionsOpen}
            mentions={mentions}
            big={big}
          />
          <Submit
            id={id}
            text={t('Send')}
            afterSend={onSend}
            className="action right"
            big={big}
            getData={getData}
            buttonProps={buttonProps}
          />
        </EditorWrapper>

        {canGif && showingGifSearch && (
          <GifSearch close={hideGifSearch} onSelect={sendGif} />
        )}
        <CommunityUploadMedia
          id={id}
          open={open}
          getInputProps={getInputProps}
        />
      </>
    </Wrapper>
  );
});

ComposeReply.propTypes = {
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  submitOnEnter: PropTypes.bool,
  onTypingState: PropTypes.func,
  uploadMediaPath: PropTypes.string.isRequired,
  mentions: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  canGif: PropTypes.bool,
  afterSend: PropTypes.func,
  big: PropTypes.bool,
  emoji: PropTypes.bool,
  getData: PropTypes.func,
  buttonProps: PropTypes.shape({}),
};

ComposeReply.defaultProps = {
  placeholder: '',
  submitOnEnter: false,
  onTypingState: null,
  mentions: false,
  canGif: false,
  afterSend: null,
  big: false,
  emoji: true,
  getData: null,
  buttonProps: null,
};

export default ComposeReply;
