import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import locales from './i18n';

const LoadError = ({ message }) => {
  const { t } = useTranslation(locales);

  return (
    <div>
      <span>{t('There was a problem')}</span>
      <span>{message}</span>
    </div>
  );
};

LoadError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default LoadError;
