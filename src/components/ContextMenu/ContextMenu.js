import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { MoreActions } from 'components/Icons';
import Menu, { Item as MenuItem } from 'components/Menu';

const Wrapper = styled.div`
  margin-left: auto;
  position: relative;
`;

const Button = styled.button`
  width: ${props => props.size * 8}px;
  height: ${props => props.size * 8}px;
  cursor: pointer;
  border: 0;
  background-color: transparent;

  &:active, &:focus {
    outline: none;
  }

  svg {
    width: ${props => props.size}px;
  }
`;

const ContextMenu = ({ items, size, color }) => {
  const [showingContextMenu, setShowingContextMenu] = useState(false);
  const show = (e, setFunction) => {
    e.preventDefault();
    e.stopPropagation();
    setFunction(true);
  };
  const showContextMenu = e => show(e, setShowingContextMenu);
  const hideContextMenu = () => setShowingContextMenu(false);

  return (
    <Wrapper>
      <Button onClick={showContextMenu} className="nouserlink" size={size}><MoreActions color={color} /></Button>
      <Menu open={showingContextMenu} onClose={hideContextMenu} className="nouserlink" size={size}>
        {items.map(({
          key, onClick, component, danger,
        }) => (
          <MenuItem key={`contextmenu-${key}`} onClick={onClick} danger={danger}>
            {component}
          </MenuItem>
        ))}
      </Menu>
    </Wrapper>
  );
};

ContextMenu.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    component: PropTypes.node.isRequired,
    danger: PropTypes.bool,
  })).isRequired,
};

ContextMenu.defaultProps = {
  size: 4,
  color: '#999',
};

export default ContextMenu;
