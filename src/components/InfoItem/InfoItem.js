import PropTypes from 'prop-types';
import styled from 'styled-components';

const InfoItem = styled.span`
  margin-right: 15px;

  svg {
    width: ${props => props.size};
    height: ${props => props.size};
    margin-right: 4px;
  }
`;

InfoItem.propTypes = {
  size: PropTypes.string,
};

InfoItem.defaultProps = {
  size: '20px',
};

export default InfoItem;
