import styled from 'styled-components';

const ImageBox = styled.div`
  width: 100%;
  text-align: center;

  img {
    max-width: 100%;
    max-height: 600px;
    margin: 0 auto;
  }
`;

export default ImageBox;
