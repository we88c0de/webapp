import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import * as channelActions from 'state/channels/actions';

import Busy from 'components/UserDisplayName/Busy';

const DisplayName = styled.span.attrs({
  className: 'displayname',
})`
  position: relative;
`;
DisplayName.displayName = 'DisplayName';

const botEqual = (prevUser, nextUser) => (
  prevUser.id === nextUser.id
  && prevUser.name === nextUser.name
  && prevUser.loading === nextUser.loading
);

const BotName = ({ botId }) => {
  const dispatch = useDispatch();

  const bot = useSelector(channelSelectors.getBotById(botId), botEqual);

  useEffect(() => {
    if (!bot) {
      dispatch(channelActions.fetchBotData(botId));
    }
  }, [bot, dispatch, botId]);

  if (!bot) return <Busy className="displayname" />;

  return (
    <DisplayName>
      <span>{bot.name}</span>
    </DisplayName>
  );
};

BotName.propTypes = {
  botId: PropTypes.string.isRequired,
};

BotName.defaultProps = {
};

export default BotName;
