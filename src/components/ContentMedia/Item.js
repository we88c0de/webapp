import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const MediaItemWrapper = styled.li`
  position: relative;
  cursor: pointer;
  @media(max-width: 767px) {
    width: 100%;
    margin-bottom: 20px;
  }
  &:not(:last-child) {
    @media(min-width: 768px) {
      margin-right: 12px;
    }
  }
  img {
    width: 100%;
    height: auto;
    object-fit: contain;
    display: inline-block;
    vertical-align: middle;
  }
`;

const MediaItem = ({ image, onClick }) => (
  <MediaItemWrapper onClick={onClick}>
    <img src={image.filename || image} alt="" />
  </MediaItemWrapper>
);

MediaItem.propTypes = {
  image: PropTypes.oneOfType([
    PropTypes.shape({
      filename: PropTypes.string.isRequired,
    }),
    PropTypes.string,
  ]).isRequired,
  onClick: PropTypes.func,
};

MediaItem.defaultProps = {
  onClick: null,
};

export default MediaItem;
