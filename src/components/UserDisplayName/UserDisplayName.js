import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';

import UserOnlineIndicator from 'components/UserOnlineIndicator';

import Busy from './Busy';

const DisplayName = styled.span.attrs({
  className: 'displayname',
})`
  position: relative;
`;
DisplayName.displayName = 'DisplayName';

const UserDisplayName = ({ userId, onlineIndicator }) => {
  const dispatch = useDispatch();

  const displayname = useSelector(userSelectors.getDisplayName(userId));
  const isLoaded = useSelector(userSelectors.isLoaded(userId));

  useEffect(() => {
    if (!isLoaded && userId) {
      dispatch(userActions.fetchData(userId));
    }
  }, [isLoaded, dispatch, userId]);

  if (!isLoaded) return <Busy className="displayname" />;

  return (
    <DisplayName>
      {onlineIndicator && <UserOnlineIndicator userId={userId} />}
      <span>{displayname}</span>
    </DisplayName>
  );
};

UserDisplayName.propTypes = {
  userId: PropTypes.number.isRequired,
  onlineIndicator: PropTypes.bool,
};

UserDisplayName.defaultProps = {
  onlineIndicator: false,
};

export default UserDisplayName;
