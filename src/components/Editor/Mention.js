import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

const Mention = ({ theme, className, mention }) => {
  const combinedClassName = `${theme.mention} ${className}`;
  const uId = typeof mention === 'number' ? mention : mention.userId;
  const user = useSelector(userSelectors.getById(uId), shallowEqual);

  return (
    <a href={`/@${user.username}`} className={combinedClassName} spellCheck={false}>
      {user.displayname}
    </a>
  );
};

Mention.propTypes = {
  theme: PropTypes.shape({
    mention: PropTypes.string,
  }).isRequired,
  className: PropTypes.string.isRequired,
  mention: PropTypes.oneOfType([PropTypes.number, PropTypes.object]).isRequired,
};

Mention.defaultProps = {
};

export default Mention;
