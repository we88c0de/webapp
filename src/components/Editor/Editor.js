import React from 'react';
import ismobile from 'ismobilejs';

import MobileEditor from './MobileEditor';
import DesktopEditor from './DesktopEditor';

const mobile = ismobile.phone || ismobile.tablet;

const ContentEditor = React.forwardRef((props, ref) => {
  if (mobile) return <MobileEditor ref={ref} {...props} />;
  return <DesktopEditor ref={ref} {...props} />;
});

export default ContentEditor;
