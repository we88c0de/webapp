import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import * as appSelectors from 'state/app/selectors';
import Editor from 'draft-js-plugins-editor';
import createEmojiPlugin from 'draft-js-emoji-plugin';
import createLinkifyPlugin from 'draft-js-linkify-plugin';
import createMarkdownPlugin from 'draft-js-md-keyboard-plugin';
import createMentionPlugin from 'draft-js-mention-plugin';
import {
  linkifyPluginOptions,
  emojiPluginOptions,
  markdownPluginOptions,
  mentionPluginOptions,
} from 'components/Editor/options';

import EditorStyled from './EditorStyled';

const plugins = [
  createLinkifyPlugin(linkifyPluginOptions),
  createMentionPlugin(mentionPluginOptions),
  createEmojiPlugin(emojiPluginOptions),
  createMarkdownPlugin(markdownPluginOptions),
];

const ReadOnlyEditor = ({
  content,
}) => {
  const [editorState, setEditorState] = useState(content);

  const isSafeForWork = useSelector(appSelectors.isSafeForWork());

  const readOnlyEditorChange = useCallback((state) => {
    setEditorState(state);
  }, []);

  return (
    <EditorStyled isSafeForWork={isSafeForWork}>
      <Editor
        editorState={editorState}
        onChange={readOnlyEditorChange}
        plugins={plugins}
        readOnly
      />
    </EditorStyled>
  );
};

ReadOnlyEditor.propTypes = {
  content: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]).isRequired,
};

ReadOnlyEditor.defaultProps = {
};

export default React.memo(ReadOnlyEditor);
