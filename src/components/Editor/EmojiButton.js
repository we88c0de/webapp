import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as appSelectors from 'state/app/selectors';

const EmojiButton = ({ id }) => {
  const emojiPlugin = useSelector(appSelectors.getEditorEmojiPlugin(id), shallowEqual);
  const { EmojiSelect } = emojiPlugin || {};

  return <EmojiSelect />;
};

EmojiButton.propTypes = {
  id: PropTypes.string.isRequired,
};

EmojiButton.defaultProps = {
};

export default React.memo(EmojiButton);
