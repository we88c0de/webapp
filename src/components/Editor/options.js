import Mention from 'components/Editor/Mention';

import { emojiPluginTheme, linkifyPluginTheme, mentionsPluginTheme } from './themes';

export const linkifyPluginOptions = ({
  theme: linkifyPluginTheme,
  target: '_blank',
});

export const emojiPluginOptions = ({
  theme: emojiPluginTheme,
});

export const mentionPluginOptions = ({
  theme: mentionsPluginTheme,
  supportWhitespace: true,
  mentionTrigger: '@',
  mentionComponent: Mention,
});

export const markdownPluginOptions = ({
  inline: [
    'BOLD',
    'ITALIC',
    'STRIKETHROUGH',
    'LINK',
    'IMAGE',
  ],
  block: [
    [
      'ordered-list-item',
      'unordered-list-item',
      'blockquote',
    ],
  ],
});
