import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Editor from 'draft-js-plugins-editor';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as appSelectors from 'state/app/selectors';
import * as appActions from 'state/app/actions';
import { contentEqual } from 'state/app/equalityFunctions';

import EditorStyled from './EditorStyled';
import EmojiButton from './EmojiButton';
import Mentions from './Mentions';

const ContentEditor = React.forwardRef(({
  id,
  onContentChange,
  linkify,
  markdown,
  emoji,
  // image,
  // orderedList,
  // unorderedList,
  placeholder,
  handleReturn,
  handlePastedFiles,
  popupPosition,
  onSuggestionsOpenChange,
  mentions,
  big,
}, ref) => {
  const dispatch = useDispatch();
  const content = useSelector(appSelectors.getEditorContent(id), contentEqual);
  const emojiPlugin = useSelector(appSelectors.getEditorEmojiPlugin(id), shallowEqual);
  const mentionPlugin = useSelector(appSelectors.getEditorMentionPlugin(id), shallowEqual);
  const markdownPlugin = useSelector(appSelectors.getEditorMarkdownPlugin(id), shallowEqual);
  const linkifyPlugin = useSelector(appSelectors.getEditorLinkifyPlugin(id), shallowEqual);

  useEffect(() => {
    dispatch(appActions.conciliateEditorContent(id));
  }, [dispatch, id, popupPosition]);

  useEffect(() => {
    if (!emojiPlugin) {
      dispatch(appActions.createEditorEmojiPlugin(id, popupPosition));
    }
  }, [emojiPlugin, dispatch, id, popupPosition]);

  useEffect(() => {
    if (!mentionPlugin) {
      dispatch(appActions.createEditorMentionPlugin(id, popupPosition));
    }
  }, [mentionPlugin, dispatch, id, popupPosition]);

  useEffect(() => {
    if (!markdownPlugin) {
      dispatch(appActions.createEditorMarkdownPlugin(id));
    }
  }, [markdownPlugin, dispatch, id]);

  useEffect(() => {
    if (!linkifyPlugin) {
      dispatch(appActions.createEditorLinkifyPlugin(id));
    }
  }, [linkifyPlugin, dispatch, id]);

  const isSafeForWork = useSelector(appSelectors.isSafeForWork());

  const suggestionsOpened = useCallback(() => {
    if (onSuggestionsOpenChange) onSuggestionsOpenChange(true);
  }, [onSuggestionsOpenChange]);
  const suggestionsClosed = useCallback(() => {
    if (onSuggestionsOpenChange) onSuggestionsOpenChange(false);
  }, [onSuggestionsOpenChange]);

  const onChange = useCallback((state) => {
    dispatch(appActions.setEditorContent(id, state));

    const contentHasChanged = typeof state === 'string' || content.getCurrentContent() !== state.getCurrentContent();
    if (onContentChange && contentHasChanged) onContentChange(state);
  }, [content, dispatch, id, onContentChange]);

  if (
    emojiPlugin === null
    || linkifyPlugin === null
    || markdownPlugin === null
  ) return null; // Not initialized

  const plugins = [];
  if (linkify) plugins.push(linkifyPlugin);
  if (markdown) plugins.push(markdownPlugin);
  if (emoji) plugins.push(emojiPlugin);
  if (mentions) plugins.push(mentionPlugin);

  const { EmojiSuggestions } = emojiPlugin || {};

  return (
    <EditorStyled isSafeForWork={isSafeForWork} popupPosition={popupPosition} big={big}>
      <Editor
        ref={ref}
        editorState={content}
        onChange={onChange}
        plugins={plugins}
        placeholder={placeholder}
        handleReturn={handleReturn}
        handlePastedFiles={handlePastedFiles}
        stripPastedStyles
      />
      {emoji && (
        <EmojiSuggestions onOpen={suggestionsOpened} onClose={suggestionsClosed} />
      )}
      {emoji && <EmojiButton id={id} />}
      {mentions && (
        <Mentions
          plugin={mentionPlugin}
          suggestions={mentions}
          onOpen={suggestionsOpened}
          onClose={suggestionsClosed}
        />
      )}
    </EditorStyled>
  );
});

ContentEditor.propTypes = {
  id: PropTypes.string.isRequired,
  onContentChange: PropTypes.func,
  linkify: PropTypes.bool,
  markdown: PropTypes.bool,
  emoji: PropTypes.bool,
  placeholder: PropTypes.string,
  handleReturn: PropTypes.func,
  handlePastedFiles: PropTypes.func,
  popupPosition: PropTypes.string,
  onSuggestionsOpenChange: PropTypes.func,
  mentions: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  big: PropTypes.bool,
};

ContentEditor.defaultProps = {
  onContentChange: null,
  linkify: true,
  markdown: true,
  emoji: true,
  // image: true,
  // orderedList: false,
  // unorderedList: false,
  placeholder: null,
  handleReturn: null,
  handlePastedFiles: null,
  popupPosition: 'top',
  onSuggestionsOpenChange: null,
  mentions: false,
  big: false,
};

export default ContentEditor;
