import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';
import { suggestionsEqual } from 'state/users/equalityFunctions';

import Suggestion from './Suggestion';

const Mentions = ({
  plugin,
  suggestions,
  onOpen,
  onClose,
}) => {
  const [filter, setFilter] = useState('');
  const mentionSuggestions = useSelector(
    userSelectors.getSuggestions(suggestions, filter),
    suggestionsEqual,
  );

  const onSearchChange = useCallback(({ value }) => {
    setFilter(value);
  }, []);

  const { MentionSuggestions } = plugin;

  return (
    <MentionSuggestions
      onSearchChange={onSearchChange}
      suggestions={mentionSuggestions}
      onOpen={onOpen}
      onClose={onClose}
      entryComponent={Suggestion}
    />
  );
};

Mentions.propTypes = {
  plugin: PropTypes.shape({
    MentionSuggestions: PropTypes.func.isRequired,
  }).isRequired,
  suggestions: PropTypes.shape({
    type: PropTypes.oneOf(['channel']).isRequired,
    id: PropTypes.string.isRequired,
  }).isRequired,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
};

Mentions.defaultProps = {
  onOpen: null,
  onClose: null,
};

const equalProps = (prevProps, nextProps) => {
  if (!shallowEqual(prevProps.plugin, nextProps.plugin)) return false;
  return (
    prevProps.suggestions.type === nextProps.suggestions.type
    && prevProps.suggestions.id === nextProps.suggestions.id
  );
};
export default React.memo(Mentions, equalProps);
