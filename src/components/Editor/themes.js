export const emojiPluginTheme = {
  emojiSelect: 'emojiSelect',
  emojiSelectButton: 'emojiSelectButton',
  emojiSelectButtonPressed: 'emojiSelectButtonPressed',
  emojiSelectPopover: 'emojiSelectPopover',
  emojiSelectPopoverClosed: 'emojiSelectPopoverClosed',
  emojiSelectPopoverTitle: 'emojiSelectPopoverTitle',
  emojiSelectPopoverGroups: 'emojiSelectPopoverGroups',
  emojiSelectPopoverGroup: 'emojiSelectPopoverGroup',
  emojiSelectPopoverGroupTitle: 'emojiSelectPopoverGroupTitle',
  emojiSelectPopoverGroupList: 'emojiSelectPopoverGroupList',
  emojiSelectPopoverGroupItem: 'emojiSelectPopoverGroupItem',
  emojiSelectPopoverToneSelect: 'emojiSelectPopoverToneSelect',
  emojiSelectPopoverToneSelectList: 'emojiSelectPopoverToneSelectList',
  emojiSelectPopoverToneSelectItem: 'emojiSelectPopoverToneSelectItem',
  emojiSelectPopoverEntry: 'emojiSelectPopoverEntry',
  emojiSelectPopoverEntryFocused: 'emojiSelectPopoverEntryFocused',
  emojiSelectPopoverEntryIcon: 'emojiSelectPopoverEntryIcon',
  emojiSelectPopoverNav: 'emojiSelectPopoverNav',
  emojiSelectPopoverNavItem: 'emojiSelectPopoverNavItem',
  emojiSelectPopoverNavEntry: 'emojiSelectPopoverNavEntry',
  emojiSelectPopoverNavEntryActive: 'emojiSelectPopoverNavEntryActive',
  emojiSelectPopoverScrollbar: 'emojiSelectPopoverScrollbar',
  emojiSelectPopoverScrollbarThumb: 'emojiSelectPopoverScrollbarThumb',
  emoji: 'emoji',
  emojiSuggestionsEntry: 'emojiSuggestionsEntry',
  emojiSuggestionsEntryFocused: 'emojiSuggestionsEntryFocused',
  emojiSuggestionsEntryText: 'emojiSuggestionsEntryText',
  emojiSuggestionsEntryIcon: 'emojiSuggestionsEntryIcon',
  emojiSuggestions: 'emojiSuggestions',
};

export const mentionsPluginTheme = {
  mention: 'mention',
  mentionSuggestions: 'mentionSuggestions',
  mentionSuggestionsEntry: 'mentionSuggestionsEntry',
  mentionSuggestionsEntryFocused: 'mentionSuggestionsEntryFocused',
  mentionSuggestionsEntryText: 'mentionSuggestionsEntryText',
  mentionSuggestionsEntryAvatar: 'mentionSuggestionsEntryAvatar',
};

export const linkifyPluginTheme = {
  link: 'linkifyLink',
};
