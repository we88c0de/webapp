import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';

const Suggestion = ({
  mention,
  theme,
  searchValue, // eslint-disable-line no-unused-vars, react/prop-types
  isFocused, // eslint-disable-line no-unused-vars, react/prop-types
  ...parentProps
}) => {
  const user = useSelector(userSelectors.getById(mention.userId), shallowEqual);

  if (user.loading) return null;

  return (
    <div {...parentProps}>
      <UserAvatar userId={user.id} className={theme.mentionSuggestionsEntryAvatar} size="32px" />
      <span className={theme.mentionSuggestionsEntryText}>{user.displayname}</span>
    </div>
  );
};

Suggestion.propTypes = {
  mention: PropTypes.shape({
    userId: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  theme: PropTypes.shape({
    mentionSuggestionsEntryAvatar: PropTypes.string,
    mentionSuggestionsEntryText: PropTypes.string,
  }).isRequired,
};

Suggestion.defaultProps = {
};

export default Suggestion;
