import React, {
  useState,
  useCallback,
  useRef,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import * as appSelectors from 'state/app/selectors';
import * as appActions from 'state/app/actions';
import * as userSelectors from 'state/users/selectors';
import { suggestionsEqual } from 'state/users/equalityFunctions';
import { editorMentionsEqual } from 'state/app/equalityFunctions';

import EditorStyled from './EditorStyled';
import Suggestion from './Suggestion';

const Mirror = styled.div`
  z-index: 1;
  overflow-y: scroll;
  height: 38px;
  width: 100%;
  margin-top: 9px;
  position: absolute;
`;
Mirror.displayName = 'Mirror';

const MobileEditorWrapper = styled.div`
  flex: 1;
  position: relative;
  background: white;
  ${props => props.big && `
    padding: 16px;
    height: 250px;

    ${Mirror} {
      height: calc(100% - 32px);
      width: calc(100% - 32px);
    }
  `}

  textarea {
    outline: none;
    resize: none;
    font-family: inherit;
    font-size: 16px;
    border: 0;
    padding: 0;
    margin-top: 9px;
    width: 100%;
    z-index: 10;
    position: relative;
    background: transparent;
    height: 100%;

    ${props => !props.big && `
      height: 38px;

      &:not(:focus)::placeholder {
        line-height: 38px;
        z-index: 1;
      }
    `}
  }
`;
MobileEditorWrapper.displayName = 'MobileEditorWrapper';

const Suggestions = styled.div`
  position: fixed;
  bottom: 70px;
  left: 0px;
  width: 100%;
  box-shadow: 0 0 1px 0px black;

  .suggestion {
    color: ${colors.red};
    background-color: ${colors.borderRed};
    text-decoration: none;
    padding: 4px 16px;
    display: flex;
  }
`;
Suggestions.displayName = 'Suggestions';

const MentionsBackground = styled.div.attrs({
  contentEditable: true,
})`
  line-height: 19px;
  color: transparent;
  margin: 0;
  padding: 0;
  width: 100%;

  span.highlight {
    background-color: ${colors.borderRed};
  }
`;
MentionsBackground.displayName = 'MentionsBackground';

const suggestionTheme = {
  mentionSuggestionsEntryAvatar: 'mentionSuggestionsEntryAvatar',
  mentionSuggestionsEntryText: 'mentionSuggestionsEntryText',
};

const MobileEditor = React.forwardRef(({
  id,
  placeholder,
  onContentChange,
  mentions,
  big,
}, ref) => {
  const dispatch = useDispatch();
  const mirror = useRef(null);
  const content = useSelector(appSelectors.getEditorContent(id));
  const editorMentions = useSelector(appSelectors.getEditorMentions(id), editorMentionsEqual);

  const [showingSuggestions, setShowingSuggestions] = useState(false);
  const suggestionDropDownPosition = useRef(null);
  const [suggestionFilter, setSuggestionFilter] = useState('');
  const mentionSuggestions = useSelector(
    userSelectors.getSuggestions(mentions, suggestionFilter),
    suggestionsEqual,
  );

  useEffect(() => {
    const el = ref.current;

    const mirrorScroll = () => {
      if (mirror.current.scrollTop !== ref.current.scrollTop) {
        mirror.current.querySelector('div').style.height = `${ref.current.scrollHeight}px`;
        mirror.current.scrollTop = ref.current.scrollTop;
      }
    };

    el.addEventListener('scroll', mirrorScroll);
    return () => el.removeEventListener('scroll', mirrorScroll);
  }, [ref]);

  const closeSuggestions = useCallback(() => {
    setSuggestionFilter('');
    suggestionDropDownPosition.current = null;
    setShowingSuggestions(false);
  }, []);

  const onChange = useCallback((v) => {
    const state = typeof v === 'string' ? v : v.target.value;
    dispatch(appActions.setEditorContent(id, state));

    const { selectionStart, selectionEnd } = ref.current;

    if (showingSuggestions && selectionEnd <= suggestionDropDownPosition.current) {
      closeSuggestions();
    }

    // Trigger suggestions if a @ was typed
    if (
      !!mentions
      && !showingSuggestions
      && state.slice((selectionStart - 1), selectionStart) === '@'
      && selectionStart === selectionEnd
      && (
        selectionStart === 1
        || state.slice((selectionStart - 2), (selectionStart - 1)) === ' '
      )
    ) {
      suggestionDropDownPosition.current = selectionStart - 1;
      setShowingSuggestions(true);
    }

    // Update suggestions filter
    if (showingSuggestions) {
      const filter = state.slice((suggestionDropDownPosition.current + 1), selectionEnd);
      if (filter.includes(' ')) closeSuggestions();
      else setSuggestionFilter(filter);
    }

    if (onContentChange) onContentChange(state);
  }, [dispatch, id, onContentChange, closeSuggestions, ref, mentions, showingSuggestions]);

  useEffect(() => {
    // If content in mention range changed, remove them
    const newEditorMentions = editorMentions.filter(mention => (
      content.substring(mention.start, mention.end) === mention.mention.name
    ));
    dispatch(appActions.setEditorMentions(id, newEditorMentions));
  }, [editorMentions, content, dispatch, id]);

  const onBlur = useCallback(() => {
    setTimeout(closeSuggestions, 500);
  }, [closeSuggestions]);

  const onMentionSelect = useCallback(mention => () => {
    const { selectionEnd, value } = ref.current;
    const newValue = (
      `${value.substring(0, suggestionDropDownPosition.current)
      + mention.name
      + value.substring(selectionEnd + 1)
      } `
    );

    dispatch(appActions.setEditorMentions(id, [
      ...editorMentions,
      {
        start: suggestionDropDownPosition.current,
        end: suggestionDropDownPosition.current + mention.name.length,
        mention,
      },
    ]));
    closeSuggestions();
    onChange(newValue);
    ref.current.focus();
  }, [closeSuggestions, ref, onChange, editorMentions, dispatch, id]);

  const renderMentionsBackground = useCallback(() => {
    let output = '<span>';
    let prevEnd = 0;
    editorMentions.forEach((suggestion) => {
      output += `${content.substring(prevEnd, suggestion.start)}<span class="highlight">${content.substring(suggestion.start, suggestion.end)}</span>`;
      prevEnd = suggestion.end;
    });
    output += `${content.substring(prevEnd)}</span>`;
    output = output.replace(/(\r\n|\n|\r)/gm, '<br />');

    return <MentionsBackground dangerouslySetInnerHTML={{ __html: output }} />;
  }, [content, editorMentions]);

  return (
    <EditorStyled big={big}>
      {showingSuggestions && (
        <Suggestions>
          {mentionSuggestions.map(mention => (
            <Suggestion
              key={`mobilemention-${mention.userId}`}
              mention={mention}
              className="suggestion"
              onClick={onMentionSelect(mention)}
              theme={suggestionTheme}
            />
          ))}
        </Suggestions>
      )}
      <MobileEditorWrapper big={big}>
        <Mirror ref={mirror}>
          {renderMentionsBackground()}
        </Mirror>
        <textarea
          ref={ref}
          placeholder={placeholder}
          value={content}
          onChange={onChange}
          onBlur={onBlur}
        />
      </MobileEditorWrapper>
    </EditorStyled>
  );
});

MobileEditor.propTypes = {
  id: PropTypes.string.isRequired,
  onContentChange: PropTypes.func,
  placeholder: PropTypes.string,
  mentions: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      type: PropTypes.oneOf(['channel']).isRequired,
      id: PropTypes.string.isRequired,
    }),
  ]),
  big: PropTypes.bool,
};

MobileEditor.defaultProps = {
  onContentChange: null,
  placeholder: null,
  mentions: null,
  big: false,
};

export default MobileEditor;
