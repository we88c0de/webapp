import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as appSelectors from 'state/app/selectors';

import EmojiPicker from 'components/EmojiPicker';

const Picker = ({ id }) => {
  const cm = useSelector(appSelectors.getComposerRef(id));

  const onSelect = useCallback((emoji) => {
    cm.replaceSelection(emoji.colons);
    cm.refresh();
  }, [cm]);

  return (
    <EmojiPicker onSelect={onSelect} />
  );
};

Picker.propTypes = {
  id: PropTypes.string.isRequired,
};

Picker.defaultProps = {
};

export default Picker;
