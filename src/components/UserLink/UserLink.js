import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

const userEqual = (prevUser, nextUser) => (
  prevUser.id === nextUser.id
  && prevUser.username === nextUser.username
  && prevUser.loading === nextUser.loading
);
const UserLink = ({
  userId, children, style, onClick,
}) => {
  const user = useSelector(userSelectors.getById(userId), userEqual);

  if (user.loading) return <span style={style}>{children}</span>;
  return <Link to={`/@${user.username}`} className="userlink" style={style} onClick={onClick}>{children}</Link>;
};

UserLink.propTypes = {
  userId: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  style: PropTypes.shape({}),
  onClick: PropTypes.func,
};

UserLink.defaultProps = {
  style: null,
  onClick: null,
};

export default UserLink;
