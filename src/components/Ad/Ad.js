import React, { useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Cookies from 'js-cookie';
import shortid from 'shortid';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import isMobile from 'utils/isMobile';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Information from 'components/Icons/Information';
import BoxDivider from 'components/BoxDivider';

import { GUEST_MAX_THREADS } from '../../constants';

const BoxWrapper = styled.div`
  margin-bottom: 32px;
  background: white;
  padding: 16px 16px 24px;
  position: relative;
  border-radius: 4px;
  overflow: hidden;
  text-align: center;
  display: none;

  ${props => props.topDivider && `
    padding-top: 36px;
  `}

  img {
    max-width: 100%;
    height: auto;
  }
`;
BoxWrapper.displayName = 'BoxWrapper';

const Info = styled.div`
  text-align: right;
  position: absolute;
  right: 4px;
  bottom: 8px;
  cursor: pointer;
  z-index: 10;
  opacity: 0.5;
  font-size: 12px;
  line-height: 16px;
  display: flex;

  svg {
    width: 16px;
    height: 16px;
    margin-left: 2px;
  }
`;
Info.displayName = 'Info';

const Ad = ({ id, divider, data }) => {
  const dispatch = useDispatch();
  const elementRef = useRef(null);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn());
  const userHasEmailConfirmed = useSelector(authSelectors.isEmailConfirmed());
  const niches = useSelector(authSelectors.getNiches(), shallowEqual);

  const zoneName = `${id.replace(/(\s)/g, '').toLowerCase()}-${shortid.generate()}`;
  const zoneId = `adzone-${zoneName}${(data && typeof data.index !== 'undefined') ? `-${data.index}` : ''}`;

  useEffect(() => {
    const device = isMobile ? 'M' : 'D';

    let usertype = 'Unlogg';
    if (userIsLoggedIn) usertype = 'UnConf';
    if (userHasEmailConfirmed) usertype = 'BasicR';
    if (
      !userIsLoggedIn
      && parseInt(Cookies.get('threadsRead'), 10) >= GUEST_MAX_THREADS
      && !/bot|crawler|spider|crawling/i.test(navigator.userAgent)
    ) usertype = 'Lurker';

    if (elementRef.current) {
      elementRef.current.style = 'display: none';
      elementRef.current.querySelector(`#${zoneId}`).innerHTML = '';
    }

    window.dataLayer.push({
      event: id,
      zoneId,
      device,
      usertype,
      niches,
      data,
    });
  }, [id, userIsLoggedIn, userHasEmailConfirmed, zoneId, data, niches]);

  const onInfoClick = useCallback(() => {
    dispatch(appActions.openAdInformation());
  }, [dispatch]);

  return (
    <BoxWrapper
      id={`${zoneId}-container`}
      topDivider={divider === 'top'}
      ref={elementRef}
    >
      {divider === 'top' && <BoxDivider top />}

      <div id={zoneId} />

      <Info onClick={onInfoClick}>
        Ad
        <Information color="#666" />
      </Info>

      {divider === 'bottom' && <BoxDivider />}
    </BoxWrapper>
  );
};

Ad.propTypes = {
  id: PropTypes.string.isRequired,
  divider: PropTypes.oneOf(['bottom', 'top']),
  data: PropTypes.shape({
    index: PropTypes.number,
  }),
};

Ad.defaultProps = {
  divider: 'bottom',
  data: {},
};

const isSameAd = (prevState, nextState) => {
  if (prevState.id !== nextState.id) return false;
  if (prevState.data && nextState.data && prevState.data.index !== nextState.data.index) {
    return false;
  }
  return true;
};

export default React.memo(Ad, isSameAd);
