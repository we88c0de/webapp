const positionSuggestions = (popupPosition = 'top') => (settings) => {
  const positioning = {
    left: `${settings.decoratorRect.left}px`,
    display: 'block',
    transform: 'scale(1) translateY(-100%)',
    transformOrigin: '1em 0% 0px',
    transition: 'all 0.25s cubic-bezier(0.3, 1.2, 0.2, 1)',
  };

  if (popupPosition === 'top') positioning.top = `${settings.decoratorRect.top - 40}px`;
  else positioning.top = `${settings.decoratorRect.bottom + 40}px`;

  return positioning;
};

export default positionSuggestions;
