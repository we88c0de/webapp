export const SITE_URL = 'https://mazmo.net';
export const EMAIL = 'hello@mazmo.net';
export const SALES_EMAIL = 'ads@mazmo.net';
export const API_URL = process.env.REACT_APP_API_URL || 'https://production.mazmoapi.net';
export const LEGACY_URL = 'https://old.mazmo.net';

export const DEFAULT_TITLE = 'Mazmo';
export const POST_TITLE = ` - ${DEFAULT_TITLE}`;

export const GIPHY_API_KEY = 'mvFSeyGlQEzhaIR9GZ0SrKLVj0MJLDMU';

export const GA_TRACKING_ID = 'UA-28359298-1';

export const META_DESCRIPTION = 'Red social de sexualidad y BDSM libre. Chat BDSM, Foros sado, masoquismo, fetish';

export const THREADS_LIST_LIMIT = 30;
export const REPLIES_LIST_LIMIT = 30;
export const PUBLICATIONS_LIST_LIMIT = 30;
export const GUEST_MAX_THREADS = 10;

export const COMMUNITY_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
};

export const CHANNEL_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
};

export const PUBLICATION_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  CONTACTS: 'CONTACTS',
  LISTS: 'LISTS',
};

// ORDER MATTTERS!! - Upper levels has privileges to its belows
export const MEMBERSHIP_ROLES = {
  OWNER: 'OWNER',
  ADMIN: 'ADMIN',
  MODERATOR: 'MODERATOR',
  USER: 'USER',
};

export const CHANNEL_ROLES = {
  OWNER: 'OWNER',
  MOD: 'MOD',
  USER: 'USER',
};

export const GENDERS = {
  MALE: {
    label: 'MALE',
    defaultPronoun: 'MALE',
  },
  FEMALE: {
    label: 'FEMALE',
    defaultPronoun: 'FEMALE',
  },
  CROSSDRESSER: {
    label: 'CROSSDRESSER',
    defaultPronoun: 'NEUTRAL',
  },
  TRAVESTI: {
    label: 'TRAVESTI',
    defaultPronoun: 'NEUTRAL',
  },
  TRANS: {
    label: 'TRANS',
    defaultPronoun: 'NEUTRAL',
  },
  INTERSEX: {
    label: 'INTERSEX',
    defaultPronoun: 'NEUTRAL',
  },
  UNDEFINED: {
    label: 'UNDEFINED',
    defaultPronoun: 'NEUTRAL',
  },
  WOMAN_CIS: {
    label: 'WOMAN_CIS',
    defaultPronoun: 'FEMALE',
  },
  MALE_CIS: {
    label: 'MALE_CIS',
    defaultPronoun: 'FEMALE',
  },
  ANDROGYNOUS: {
    label: 'ANDROGYNOUS',
    defaultPronoun: 'NEUTRAL',
  },
  MALE_TRANS: {
    label: 'MALE_TRANS',
    defaultPronoun: 'MALE',
  },
  FEMALE_TRANS: {
    label: 'FEMALE_TRANS',
    defaultPronoun: 'NEUTRAL',
  },
  TRANSEXUAL: {
    label: 'TRANSEXUAL',
    defaultPronoun: 'NEUTRAL',
  },
  TRANSGENDER: {
    label: 'TRANSGENDER',
    defaultPronoun: 'NEUTRAL',
  },
  FEMALE_TRANSGENDER: {
    label: 'FEMALE_TRANSGENDER',
    defaultPronoun: 'FEMALE',
  },
  MALE_TRANSGENDER: {
    label: 'MALE_TRANSGENDER',
    defaultPronoun: 'MALE',
  },
  QUEER: {
    label: 'QUEER',
    defaultPronoun: 'NEUTRAL',
  },
  NEUTRAL: {
    label: 'NEUTRAL',
    defaultPronoun: 'NEUTRAL',
  },
  NOT_BINARY: {
    label: 'NOT_BINARY',
    defaultPronoun: 'NEUTRAL',
  },
};

export const USERS_PRONOUNS = {
  MALE: 'MALE',
  FEMALE: 'FEMALE',
  NEUTRAL: 'NEUTRAL',
};

export const SOCKET_ACTIONS = {
  COMMUNITIES: {
    COMMUNITY_CREATED: 'community:created',
    COMMUNITY_EDITED: 'community:edited',
    COMMUNITY_DELETED: 'community:deleted',

    MEMBERSHIP_CREATED: 'community:membership:created',
    MEMBERSHIP_APPROVED: 'community:membership:approved',
    MEMBERSHIP_DELETED: 'community:membership:deleted',

    THREAD_CREATED: 'community:thread:created',
    THREAD_EDITED: 'community:thread:edited',
    THREAD_DELETED: 'community:thread:deleted',
    THREAD_RESTORED: 'community:thread:restored',

    REPLY_CREATED: 'community:reply:created',
    REPLY_EDITED: 'community:reply:edited',
    REPLY_DELETED: 'community:reply:deleted',
    REPLY_RESTORED: 'community:reply:restored',

    COMMUNITY_JOINED: 'community:joined',
    COMMUNITY_LEFT: 'community:left',
  },

  MESSENGERS: {
    DM_CREATED: 'messengers:directmessage:created',
    DM_UPDATED: 'messengers:directmessage:updated',
    DM_REMOVED: 'messengers:directmessage:removed',
    MESSENGER_UPDATED: 'messengers:messenger:updated',
    MESSENGER_TYPING: 'messengers:messenger:typing',
  },

  CHANNELS: {
    CM_CREATED: 'chat:channelmessage:created',
    CM_UPDATED: 'chat:channelmessage:updated',
    CM_REMOVED: 'chat:channelmessage:removed',
    CHANNEL_UPDATED: 'chat:channel:updated',
    CHANNEL_DELETED: 'chat:channel:deleted',
    CHANNEL_TYPING: 'chat:channel:typing',
  },

  ALERTS: {
    ALERT_CREATED: 'alerts:created',
    ALERT_UPDATED: 'alerts:updated',
  },

  USERS: {
    ONLINE_LIST: 'users:online:list',
    ONLINE: 'users:online:new',
    OFFLINE: 'users:offline:new',
  },

  FEED: {
    NEW_PUBLICATION_IN_FEED: 'feed:publication:added',
    PUBLICATION_UPDATED: 'feed:publication:updated',
    PUBLICATION_REMOVED: 'feed:publication:removed',
  },
};

export const APP_NAMES = {
  web: 'mazmo-webapp',
};

export const SW = {
  NEW_AUTH: 'notifications/newAuth',
  NEW_THREADS: 'notifications/newThreads',
  NEW_REPLIES: 'notifications/newReplies',
};

export const ALERT_TYPES = {
  COMMUNITY_INVITE_CREATED: 'COMMUNITY_INVITE_CREATED',
  REACTION_REPLY_CREATED: 'REACTION_REPLY_CREATED',
  REACTION_THREAD_CREATED: 'REACTION_THREAD_CREATED',
  COMMUNITIES_THREAD_MENTION: 'COMMUNITIES_THREAD_MENTION',

  SADES_TRANSACTION: 'SADES_TRANSACTION',
  SADES_ASSIGNMENT: 'SADES_ASSIGNMENT',
  FOLLOW_CREATED: 'FOLLOW_CREATED',
  KNOW_CREATED: 'KNOW_CREATED',
  COMMENT_CREATED: 'COMMENT_CREATED',

  REACTION_COMMENT_CREATED: 'REACTION_COMMENT_CREATED',
  REACTION_PUBLICATION_CREATED: 'REACTION_PUBLICATION_CREATED',
  RELATIONSHIP_REQUESTED: 'RELATIONSHIP_REQUESTED',
  RELATIONSHIP_APPROVED: 'RELATIONSHIP_APPROVED',
  RELATIONSHIP_REJECTED: 'RELATIONSHIP_REJECTED',
  RELATIONSHIP_REMOVED: 'RELATIONSHIP_REMOVED',

  MESSENGER_APPROVED: 'MESSENGER_APPROVED',
  MESSENGER_REQUESTED: 'MESSENGER_REQUESTED',
  CHAT_CHANNEL_DELETED: 'CHAT_CHANNEL_DELETED',
  CHANNELINVITATION_CREATED: 'CHANNELINVITATION_CREATED',

  FEED_PUBLICATION_REACTED: 'FEED_PUBLICATION_REACTED',
  FEED_COMMENT_REACTED: 'FEED_COMMENT_REACTED',
  FEED_COMMENT_CREATED: 'FEED_COMMENT_CREATED',
  FEED_PUBLICATION_MENTION: 'FEED_PUBLICATION_MENTION',
};

export const CHANNEL_MESSAGES_TYPES = {
  MESSAGE: 'MESSAGE',
  JOIN: 'JOIN',
  PART: 'PART',
  BAN: 'BAN',
  UNBAN: 'UNBAN',
};

export const PUBLICATION_TYPES = {
  TEXT: 'TEXT',
  MEDIA: 'MEDIA',
  GIF: 'GIF',
  LINK: 'LINK',
};

export const CM_AUTHOR_TYPES = {
  USER: 'USER',
  BOT: 'BOT',
  AUTORESPONDER: 'AUTORESPONDER',
};

export const REPORT_REASONS = {
  SPAM: 'SPAM',
  HACKED: 'HACKED',
  FALSE_IDENTITY: 'FALSE_IDENTITY',
  HATE: 'HATE',
};
