import moment from 'moment';

const sortAlerts = (a, b) => moment(b.updatedAt).diff(a.updatedAt);

export const getAll = () => ({ alerts: state }) => [...state.data].sort(sortAlerts);

export const isFetching = () => ({ alerts: state }) => state.fetching;

export const hasFailed = () => ({ alerts: state }) => !!state.fetchError;

export const totalUnread = () => ({ alerts: state }) => (
  state.data.reduce((sum, alert) => (sum + (alert.read ? 0 : 1)), 0)
);
