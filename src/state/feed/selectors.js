export const getFeed = () => ({ feed: state }) => state.feed;

export const isFeedFullyLoaded = () => ({ feed: state }) => state.feedFullyLoaded;

export const isFeedEmpty = () => ({ feed: state }) => !state.feed.length && state.feedFullyLoaded;

export const getPublication = id => ({ feed: state }) => state.publications[id];

export const getCommentIds = publicationId => ({ feed: state }) => (
  state.publications[publicationId].comments
);

export const getComment = id => ({ feed: state }) => state.comments[id];

export const publications = {
  getReactionsList: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return [];
    return publication.reactedByUserIds;
  },

  following: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return [];
    return publication.following;
  },

  commentsBlocked: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return false;
    return publication.commentsBlocked;
  },

  getPrivacy: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return null;
    return publication.privacy;
  },

  getAuthorId: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return null;
    return publication.author;
  },

  userIsAuthor: id => (state) => {
    const publication = state.feed.publications[id];
    const { me } = state.auth;
    if (!publication || !me) return false;
    return publication.author === me.id;
  },

  getCreatedAt: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return null;
    return publication.createdAt;
  },

  getType: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return null;
    return publication.type;
  },

  getUrl: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication || !publication.payload) return null;
    return publication.payload.url;
  },

  hasLocktoberHashtag: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication || !publication.hashtags) return false;
    return publication.hashtags.includes('locktober');
  },

  getMedia: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication || !publication.payload) return null;
    return publication.payload.media;
  },

  userHasSpanked: id => (state) => {
    const publication = state.feed.publications[id];
    const { me } = state.auth;
    if (!publication || !me) return false;
    return publication.reactedByUserIds.includes(me.id);
  },

  getSpankCount: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return false;
    return publication.reactedByUserIds.length;
  },

  userHasCommented: id => (state) => {
    const publication = state.feed.publications[id];
    const { me } = state.auth;
    if (!publication || !me) return false;
    return publication.commentedByUserIds.includes(me.id);
  },

  getCommentCount: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication) return false;
    return publication.commentedByUserIds.length;
  },

  getRawContent: id => (state) => {
    const publication = state.feed.publications[id];
    if (!publication || !publication.payload) return null;
    return publication.payload.rawContent;
  },
};

export const comments = {
  getReactionsList: commentId => (state) => {
    const comment = state.feed.comments[commentId];
    if (!comment) return [];
    return comment.reactedByUserIds;
  },
  getMedia: commentId => (state) => {
    const comment = state.feed.comments[commentId];
    if (!comment) return [];
    return comment.media;
  },
};

export const getFeedQueueCount = () => state => state.feed.feedQueue.length;

export const getLatestHashtags = () => state => state.feed.latestHashtags;
