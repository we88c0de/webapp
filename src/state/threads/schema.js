import { schema } from 'normalizr';

import user from 'state/users/schema';

const thread = new schema.Entity('threads', {
  author: user,
  lastReply: {
    user,
  },
}, {
  idAttribute: '_id',
});

export default thread;
