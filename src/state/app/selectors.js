import { EditorState } from 'draft-js';
import ismobile from 'ismobilejs';

const mobile = ismobile.phone || ismobile.tablet;

export const getErrors = () => ({ app: state }) => state.errors;

export const getToasts = () => ({ app: state }) => state.toasts;

// export const isSafeForWork = () => ({ app: state }) => state.safeForWork;
export const isSafeForWork = () => () => false;

export const showingAdInfoModal = () => ({ app: state }) => state.ui.adInfo;

export const getMiniprofile = () => ({ app: state, users }) => {
  const profile = state.miniprofile;
  if (typeof profile === 'object') return profile;

  return Object.values(users.data).find(u => u.username === profile);
};

export const isUiLeftColumnActive = () => ({ app: state }) => state.ui.leftColumn;

export const isUiRightColumnActive = () => ({ app: state }) => state.ui.rightColumn;

export const isNavLeftActive = () => ({ app: state }) => state.navLeftActive;

export const isShowingParticipants = () => ({ app: state }) => (
  state.ui.channels.showingParticipants
);

export const urlHistory = (match = /(.*)/g) => ({ app: state }) => (
  state.locationHistory.map(l => l.pathname).filter(l => l.match(match))
);

export const historyCanGoBack = () => ({ app: state }) => (
  state.locationHistory.length > 1
);

export const getMedia = id => ({ app: state }) => {
  if (!state.composers[id] || !state.composers[id].media) return [];
  return state.composers[id].media;
};

// Service Worker
export const getRegisteredSW = () => ({ app: state }) => state.sw.registration;

export const getWaitingSW = () => ({ app: state }) => state.sw.waiting;

// Composers
export const isComposerReadyToSubmit = id => ({ app: state }) => {
  if (!state.composers[id]) return false;

  const { value, media = [] } = state.composers[id];

  const hasText = value && value.length > 0;
  const hasMediaReady = media.length === media.filter(m => m.uploaded).length;

  if (!hasMediaReady) return false;
  return media.length > 0 || hasText;
};

export const isComposerSaving = id => ({ app: state }) => {
  if (!state.composers[id]) return false;
  return state.composers[id].saving;
};

export const isComposerDisabled = id => ({ app: state }) => {
  if (!state.composers[id]) return false;
  return !!state.composers[id].readOnly;
};

export const getComposerRef = id => ({ app: state }) => {
  if (!state.composers[id]) return false;
  return state.composers[id].ref;
};

export const composerHasContent = id => ({ app: state }) => {
  if (!state.composers[id]) return false;

  const { value, media = [] } = state.composers[id];

  const hasText = value && value.length > 0;
  const hasMedia = media.length > 0;

  return !!hasMedia || !!hasText;
};

// Editors
export const getEditorContent = id => ({ app: state }) => {
  if (!id) return null;
  if (state.editors[id] && state.editors[id].content) return state.editors[id].content;

  return mobile ? '' : EditorState.createEmpty();
};

export const editorHasContent = id => ({ app: state }) => {
  if (!state.editors[id]) return false;

  const { content, media = [] } = state.editors[id];

  const hasText = content && (mobile
    ? (content.length > 0)
    : (content.getCurrentContent().getPlainText().length > 0));

  const hasMedia = media.length > 0;

  return hasMedia || hasText;
};

export const isEditorReadyToSubmit = id => ({ app: state }) => {
  if (!state.editors[id]) return false;

  const { content, media = [] } = state.editors[id];

  const hasText = content && (mobile
    ? (content.length > 0)
    : (content.getCurrentContent().getPlainText().length > 0));

  const hasMediaReady = media.length === media.filter(m => m.uploaded).length;

  if (!hasMediaReady) return false;
  return media.length > 0 || hasText;
};

export const isEditorCreating = id => ({ app: state }) => {
  if (!state.editors[id]) return false;
  return state.editors[id].creating;
};

const getEditorPlugin = (id, name) => ({ app: state }) => {
  if (!id) return null;
  const editor = state.editors[id];
  if (editor && editor.plugins && editor.plugins[name]) {
    return editor.plugins[name];
  }
  return null;
};

export const getEditorEmojiPlugin = id => state => getEditorPlugin(id, 'emoji')(state);
export const getEditorMentionPlugin = id => state => getEditorPlugin(id, 'mentions')(state);
export const getEditorLinkifyPlugin = id => state => getEditorPlugin(id, 'linkify')(state);
export const getEditorMarkdownPlugin = id => state => getEditorPlugin(id, 'markdown')(state);

export const getEditorMentions = id => ({ app: state }) => {
  if (!state.editors[id]) return [];
  if (!state.editors[id].mentions) return [];
  return state.editors[id].mentions;
};
