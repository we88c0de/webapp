import React from 'react';
import {
  EditorState,
  ContentState,
  Modifier,
  SelectionState,
  convertToRaw,
} from 'draft-js';
import ismobile from 'ismobilejs';
import createEmojiPlugin from 'draft-js-emoji-plugin';
import createLinkifyPlugin from 'draft-js-linkify-plugin';
import createMarkdownPlugin from 'draft-js-md-keyboard-plugin';
import createMentionPlugin from 'draft-js-mention-plugin';

import positionSuggestions from 'utils/positionSuggestions';
import * as messengerActions from 'state/messengers/actions';
import * as channelActions from 'state/channels/actions';
import * as replyActions from 'state/replies/actions';
import * as threadActions from 'state/threads/actions';
import * as feedActions from 'state/feed/actions';
import * as communitySelectors from 'state/communities/selectors';
import * as threadSelectors from 'state/threads/selectors';
import * as replySelectors from 'state/replies/selectors';
import * as channelSelectors from 'state/channels/selectors';
import * as messengerSelectors from 'state/messengers/selectors';

import {
  linkifyPluginOptions,
  emojiPluginOptions,
  markdownPluginOptions,
  mentionPluginOptions,
} from 'components/Editor/options';
import { Emoticon } from 'components/Icons';

import {
  ERRORS_ADD,
  ERRORS_CLEAR,
  TOASTS_ADD,
  TOASTS_REMOVE,
  ADINFO_OPEN,
  ADINFO_CLOSE,
  MINIPROFILE_SET,
  NAVLEFT_ACTIVE,
  NEW_LOCATION,
  UI_LEFT_COLUMN,
  UI_RIGHT_COLUMN,
  UI_CHANNELS_SHOWING_PARTICIPANTS,
  TOGGLE_SAFE_FOR_WORK,
  MEDIA_SET,
  MEDIA_ADD,
  MEDIA_REMOVE,
  MEDIA_CLEAR,
  MEDIA_UPLOAD_COMPLETE,
  EDITOR_SET,
  EDITOR_CREATING,
  EDITOR_PLUGIN,
  EDITOR_MENTIONS,
  COMPOSER_REF,
  COMPOSER_VALUE,
  COMPOSER_SAVING,
  COMPOSER_READONLY,
  CHAT_LIST_SET,
  CHAT_LIST_UPDATE,
  CHAT_LIST_REMOVE,
  SW_REGISTERED,
  SW_WAITING,
} from './constants';

const mobile = ismobile.phone || ismobile.tablet;

export const addError = error => (dispatch) => {
  if (error === 'NOT_LOGGED_IN') {
    document.location.href = '/login';
  } else {
    dispatch({ type: ERRORS_ADD, data: error });
  }
};

export const clearErrors = () => dispatch => dispatch({ type: ERRORS_CLEAR });

export const addToast = toast => dispatch => dispatch({ type: TOASTS_ADD, data: toast });

export const removeToast = index => dispatch => dispatch({ type: TOASTS_REMOVE, index });

export const openAdInformation = () => dispatch => dispatch({ type: ADINFO_OPEN });

export const closeAdInformation = () => dispatch => dispatch({ type: ADINFO_CLOSE });

export const toggleNavLeft = () => (dispatch, getState) => {
  const isActive = getState().app.navLeftActive;
  dispatch({ type: NAVLEFT_ACTIVE, active: !isActive });
};

export const uiLeftColumn = active => dispatch => dispatch({ type: UI_LEFT_COLUMN, active });

export const uiRightColumn = active => dispatch => dispatch({ type: UI_RIGHT_COLUMN, active });

export const setMiniprofile = data => dispatch => dispatch({ type: MINIPROFILE_SET, data });

export const setShowingParticipants = show => dispatch => dispatch({
  type: UI_CHANNELS_SHOWING_PARTICIPANTS,
  show,
});

export const toggleSafeForWork = () => dispatch => dispatch({ type: TOGGLE_SAFE_FOR_WORK });

export const newLocation = location => (dispatch, getState) => {
  const history = getState().app.locationHistory;
  const prevLocation = history[history.length - 1];
  if (!prevLocation || location.key !== prevLocation.key) {
    dispatch({ type: NEW_LOCATION, location });
  }
};

// Service worker
export const swRegistered = registration => (dispatch) => {
  dispatch({ type: SW_REGISTERED, registration });
};

export const swWaiting = waiting => (dispatch) => {
  dispatch({ type: SW_WAITING, waiting });
};

// Chat list
const sortChats = (state) => {
  const channels = channelSelectors.getAll()(state);
  const messengers = messengerSelectors.getAll()(state);

  const myMessengers = messengers.filter(data => !data.archived).map(data => ({ type: 'messenger', data }));
  const myChannels = channels.map(data => ({ type: 'channel', data }));

  const entities = myMessengers.concat(myChannels);
  const lastMessagesByChannel = channelSelectors.allLastMessages()(state);
  const lastMessagesByMessenger = messengerSelectors.allLastMessages()(state);

  const sortedEntities = entities
    .sort((a, b) => {
      const lastMessageA = a.type === 'messenger' ? lastMessagesByMessenger[a.data.id] : lastMessagesByChannel[a.data.id];
      const lastMessageB = b.type === 'messenger' ? lastMessagesByMessenger[b.data.id] : lastMessagesByChannel[b.data.id];

      if (!lastMessageA && !lastMessageB) return 0;
      if (lastMessageA && !lastMessageB) return -1;
      if (!lastMessageA && lastMessageB) return 1;

      const createdAtA = (
        lastMessageA.createdAt
        || (lastMessageA.payload && lastMessageA.payload.createdAt)
      );
      const createdAtB = (
        lastMessageB.createdAt
        || (lastMessageB.payload && lastMessageB.payload.createdAt)
      );
      if (createdAtA > createdAtB) return -1;

      return 1;
    })
    .map(entity => ({ type: entity.type, id: entity.data.id }));

  return sortedEntities;
};

export const computeChatList = () => (dispatch, getState) => {
  const list = sortChats(getState());
  dispatch({ type: CHAT_LIST_SET, list });
};

export const updateChatList = (entityType, entityId) => (dispatch, getState) => {
  const firstChat = getState().app.chatList[0];

  if (firstChat && firstChat !== entityId) {
    dispatch({ type: CHAT_LIST_UPDATE, entityType, entityId });
  }
};

export const removeFromChatList = (entityType, entityId) => (dispatch) => {
  dispatch({ type: CHAT_LIST_REMOVE, entityType, entityId });
};

export const addToChatList = (entityType, entityId) => (dispatch) => {
  dispatch({ type: CHAT_LIST_UPDATE, entityType, entityId });
};

// Media upload
export const mediaUploadSet = (id, value) => (dispatch) => {
  const data = value.map(v => ({
    ...v,
    uploaded: true,
    file: { path: v.id },
    preview: v.filename,
  }));
  dispatch({ type: MEDIA_SET, id, data });
};

export const mediaUploadAdd = (id, data) => (dispatch) => {
  dispatch({ type: MEDIA_ADD, id, data });
};

export const mediaUploadRemove = (id, index) => (dispatch) => {
  dispatch({ type: MEDIA_REMOVE, id, index });
};

export const mediaUploadClear = id => (dispatch) => {
  dispatch({ type: MEDIA_CLEAR, id });
};

export const mediaUploadComplete = (id, name, filename) => (dispatch) => {
  dispatch({
    type: MEDIA_UPLOAD_COMPLETE,
    id,
    name,
    filename,
  });
};

// Composers
export const setComposerRef = (id, ref) => (dispatch) => {
  dispatch({ type: COMPOSER_REF, id, ref });
};

export const setComposerValue = (id, value) => (dispatch) => {
  dispatch({ type: COMPOSER_VALUE, id, value });
};

export const clearComposer = id => (dispatch, getState) => {
  const { ref } = getState().app.composers[id];
  ref.setValue('');
  ref.clearHistory();
  dispatch(mediaUploadClear(id));
};

export const disableComposer = id => (dispatch, getState) => {
  const { ref } = getState().app.composers[id];
  ref.setOption('readOnly', 'nocursor');
  dispatch({ type: COMPOSER_READONLY, id, readOnly: true });
};

export const enableComposer = id => (dispatch, getState) => {
  const { ref } = getState().app.composers[id];
  ref.setOption('readOnly', false);
  dispatch({ type: COMPOSER_READONLY, id, readOnly: false });
};

export const composerSendToServer = (key, data) => async (dispatch, getState) => {
  try {
    const { ref, saving, media = [] } = getState().app.composers[key];
    const value = ref.getValue(); // We don't use state's because in mobile is not always up to date

    if (!saving && (value || media.length > 0 || data)) {
      const [type, id] = key.split('-');
      const optimistic = ['messenger', 'channel'].includes(type);

      dispatch({ type: COMPOSER_SAVING, id: key, saving: true });
      if (optimistic) dispatch(clearComposer(key));

      let result;
      if (type === 'messenger') {
        const replyingTo = getState().messengers.replyTo[id];
        dispatch(messengerActions.clearReplyTo(id));

        const payload = data || {
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
          createdAt: (new Date()).toISOString(),
          replyingTo,
        };
        ref.focus();
        result = await dispatch(messengerActions.createDirectMessage(id, payload));
        dispatch(messengerActions.sendTypingState(id, 'STOPPED'));
      } else if (type === 'messengeredit') {
        const messageId = getState().messengers.editing[id];
        const payload = data || {
          rawContent: value,
        };
        result = await dispatch(messengerActions.editDirectMessage(id, messageId, payload));
        const messengerRef = getState().app.composers[`messenger-${id}`].ref;
        messengerRef.focus();
        dispatch(messengerActions.cancelEditing(id));
      } else if (type === 'channel') {
        const replyingTo = getState().channels.replyTo[id];
        dispatch(channelActions.clearReplyTo(id));

        const payload = data || {
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
          createdAt: (new Date()).toISOString(),
          replyingTo,
        };
        ref.focus();
        result = await dispatch(channelActions.createChannelMessage(id, payload));
      } else if (type === 'channeledit') {
        const messageId = getState().channels.editing[id];
        const payload = data || {
          rawContent: value,
        };
        result = await dispatch(channelActions.editChannelMessage(id, messageId, payload));
        const channelRef = getState().app.composers[`channel-${id}`].ref;
        channelRef.focus();
        dispatch(channelActions.cancelEditing(id));
      } else if (type === 'publication') {
        const payload = {
          type: data.type,
          payload: {
            rawContent: value,
            ...data.payload,
            media: media.map(m => m.filename),
          },
          privacy: data.privacy,
          privacyLists: data.privacyLists,
        };

        await dispatch(feedActions.publish(payload));
      } else if (type === 'comment') {
        const payload = {
          rawContent: value,
          media: media.map(m => m.filename),
        };
        await dispatch(feedActions.addComment(id, payload));
      } else if (type === 'createthread') {
        if (!id) throw new Error('Debes seleccionar una comunidad');
        const allSlugs = communitySelectors.getAllSlugs()(getState());
        const communitySlug = allSlugs[id];

        const payload = {
          title: data.title,
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
        };

        result = await dispatch(threadActions.create(communitySlug, payload));
        result.communitySlug = communitySlug;
      } else if (type === 'thread') {
        const payload = data || {
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
        };
        result = await dispatch(replyActions.create(id, payload));
      } else if (type === 'editthread') {
        const thread = threadSelectors.getById(id)(getState());
        const payload = {
          ...data,
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
        };

        await dispatch(threadActions.edit(thread, payload));
      } else if (type === 'editreply') {
        const reply = replySelectors.getById(id)(getState());
        const payload = {
          ...data,
          rawContent: value,
          media: {
            images: media.map(m => m.filename),
          },
        };

        await dispatch(replyActions.edit(reply, payload));
      } else {
        throw new Error('Type not valid');
      }

      dispatch({ type: COMPOSER_SAVING, id: key, saving: false });
      if (!optimistic) dispatch(clearComposer(key));

      return result;
    }
  } catch (error) {
    dispatch(addError(error));
    dispatch({ type: COMPOSER_SAVING, id: key, saving: false });
  }

  return null;
};

// Editors
export const setEditorContent = (id, content) => (dispatch) => {
  if (content === null) {
    if (mobile) dispatch({ type: EDITOR_SET, id, content: '' });
    else {
      dispatch({
        type: EDITOR_SET,
        id,
        content: EditorState.moveFocusToEnd(EditorState.createEmpty()),
      });
    }
  } else {
    dispatch({ type: EDITOR_SET, id, content });
  }
};

export const emptyEditorContent = id => (dispatch, getState) => {
  if (mobile) dispatch({ type: EDITOR_SET, id, content: '' });
  else {
    const { content } = getState().app.editors[id];
    dispatch({
      type: EDITOR_SET,
      id,
      content: EditorState.moveFocusToEnd(EditorState.push(content, ContentState.createFromText(''), 'remove-range')),
    });
  }
};

export const conciliateEditorContent = id => (dispatch, getState) => {
  const { content } = getState().app.editors[id] || {};
  if (!mobile && content) {
    // Hack needed by editor's plugins to be in sync with content
    setTimeout(() => {
      dispatch({
        type: EDITOR_SET,
        id,
        content: EditorState.moveFocusToEnd(EditorState.push(content, content.getCurrentContent(), 'remove-range')),
      });
    }, 0);
  }
};

const parseContent = (value, editorMentions = []) => {
  if (mobile) {
    let contentState = ContentState.createFromText(value || '');
    const block = contentState.getFirstBlock();
    editorMentions.forEach((suggestion) => {
      const contentStateWithEntity = contentState.createEntity('mention', 'SEGMENTED', {
        mention: suggestion.mention,
      });
      const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
      const currentSelectionState = SelectionState.createEmpty(block.getKey());
      const selectionState = currentSelectionState.merge({
        anchorOffset: suggestion.start,
        focusOffset: suggestion.start + suggestion.mention.name.length,
      });
      contentState = Modifier.applyEntity(
        contentStateWithEntity,
        selectionState,
        entityKey,
      );
    });

    const userMentions = editorMentions.map(m => m.mention.userId);

    return {
      content: JSON.stringify(convertToRaw(contentState)),
      rawContent: value,
      userMentions,
    };
  }

  const state = value.getCurrentContent();
  const contentState = convertToRaw(state);
  const userMentions = Object.values(contentState.entityMap)
    .filter(v => v.type === 'mention')
    .map(v => v.data.mention.userId);

  return {
    content: JSON.stringify(contentState),
    rawContent: state.getPlainText(),
    userMentions,
  };
};

export const editorSendToServer = (key, data) => async (dispatch, getState) => {
  try {
    const {
      content,
      creating,
      media = [],
      mentions,
    } = getState().app.editors[key];
    const state = parseContent(content, mentions);

    if (!creating && (state.rawContent || media.length > 0 || data)) {
      const [type, id] = key.split('-');

      dispatch({ type: EDITOR_CREATING, id: key, creating: true });
      dispatch(emptyEditorContent(key));
      dispatch(mediaUploadClear(key));

      let result;
      if (type === 'messenger') {
        const payload = data || {
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
          createdAt: (new Date()).toISOString(),
        };
        result = await dispatch(messengerActions.createDirectMessage(id, payload));
        dispatch(messengerActions.sendTypingState(id, 'STOPPED'));
      } else if (type === 'channel') {
        const payload = data || {
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
          createdAt: (new Date()).toISOString(),
        };
        result = await dispatch(channelActions.createChannelMessage(id, payload));
      } else if (type === 'thread') {
        const payload = data || {
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
        };
        result = await dispatch(replyActions.create(id, payload));
      } else if (type === 'createthread') {
        if (!id) throw new Error('Debes seleccionar una comunidad');
        const allSlugs = communitySelectors.getAllSlugs()(getState());
        const communitySlug = allSlugs[id];

        const payload = {
          title: data.title,
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
        };

        result = await dispatch(threadActions.create(communitySlug, payload));
        result.communitySlug = communitySlug;
      } else if (type === 'editthread') {
        const thread = threadSelectors.getById(id)(getState());
        const payload = {
          ...data,
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
        };

        await dispatch(threadActions.edit(thread, payload));
      } else if (type === 'editreply') {
        const reply = replySelectors.getById(id)(getState());
        const payload = {
          ...data,
          ...state,
          media: {
            images: media.map(m => m.filename),
          },
        };

        await dispatch(replyActions.edit(reply, payload));
      } else if (type === 'publication') {
        const payload = {
          type: data.type,
          payload: {
            ...state,
            ...data.payload,
            media: media.map(m => m.filename),
          },
          privacy: data.privacy,
          privacyLists: data.privacyLists,
        };

        await dispatch(feedActions.publish(payload));
      } else if (type === 'comment') {
        const payload = {
          ...state,
          media: media.map(m => m.filename),
        };
        await dispatch(feedActions.addComment(id, payload));
      } else {
        throw new Error('Type not valid');
      }

      dispatch({ type: EDITOR_CREATING, id: key, creating: false });
      dispatch({ type: EDITOR_MENTIONS, id: key, mentions: [] });
      return result;
    }
  } catch (error) {
    dispatch(addError(error));
    dispatch({ type: EDITOR_CREATING, id: key, creating: false });
  }

  return null;
};

export const createEditorEmojiPlugin = (key, position) => (dispatch) => {
  const plugin = createEmojiPlugin({
    ...emojiPluginOptions,
    selectButtonContent: <Emoticon color="#666" />,
    positionSuggestions: positionSuggestions(position),
  });

  dispatch({
    type: EDITOR_PLUGIN,
    name: 'emoji',
    id: key,
    plugin,
  });
};

export const createEditorMentionPlugin = (key, position) => (dispatch) => {
  const plugin = createMentionPlugin({
    ...mentionPluginOptions,
    positionSuggestions: positionSuggestions(position),
  });

  dispatch({
    type: EDITOR_PLUGIN,
    name: 'mentions',
    id: key,
    plugin,
  });
};

export const createEditorLinkifyPlugin = key => (dispatch) => {
  const plugin = createLinkifyPlugin(linkifyPluginOptions);

  dispatch({
    type: EDITOR_PLUGIN,
    name: 'linkify',
    id: key,
    plugin,
  });
};

export const createEditorMarkdownPlugin = key => (dispatch) => {
  const plugin = createMarkdownPlugin(markdownPluginOptions);

  dispatch({
    type: EDITOR_PLUGIN,
    name: 'markdown',
    id: key,
    plugin,
  });
};

export const setEditorMentions = (key, mentions) => (dispatch) => {
  dispatch({ type: EDITOR_MENTIONS, id: key, mentions });
};
