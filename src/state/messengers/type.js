import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  participants: PropTypes.arrayOf(PropTypes.object).isRequired,
  unreadCount: PropTypes.number.isRequired,
});
