import isEmojiOnly from 'utils/isEmojiOnly';
import * as userSelectors from 'state/users/selectors';

export const getAll = messengerId => ({ messengers: state, auth }) => state.messengers
  .map(m => state.data.messengers[m])
  .filter((m) => {
    if (m.id === messengerId || !!m.lastMessage) return true;

    const hasRequestMessage = m.participants.some(p => !!p.requestMessage);
    const meParticipant = m.participants.find(p => p.userId === auth.me.id);
    if (!meParticipant) return false;
    const approvedByUser = meParticipant.approved;

    return hasRequestMessage && approvedByUser;
  });

export const getRequested = () => ({ messengers: state, auth }) => state.messengers
  .map(m => state.data.messengers[m])
  .filter((m) => {
    const hasRequestMessage = m.participants.some(p => !!p.requestMessage);
    const user = m.participants.find(p => p.userId === auth.me.id);
    if (!user) return false;

    return hasRequestMessage && !user.approved;
  });

export const getById = id => ({ messengers: state }) => state.data.messengers[id];

export const isFetching = () => ({ messengers: state }) => state.fetching;

export const isInitialized = () => ({ messengers: state }) => state.initialized;

export const hasFailed = () => ({ messengers: state }) => !!state.fetchError;

export const getByUserId = userId => ({ messengers: state }) => {
  const matches = state.messengers.filter((messengerId) => {
    const messenger = state.data.messengers[messengerId];
    const participantIds = messenger.participants.map(p => p.userId);
    return participantIds.length === 2 && participantIds.includes(userId);
  });

  return matches.length > 0 && matches[0];
};

export const getMessagesByMessengerId = messengerId => ({ messengers: state }) => (
  (state.directmessages[messengerId] || []).map(id => state.data.directmessages[id])
);

export const getLastMessageByMessengerId = messengerId => ({ messengers: state }) => {
  const messages = getMessagesByMessengerId(messengerId)({ messengers: state });
  return messages[messages.length - 1];
};

export const isFullyLoaded = messengerId => ({ messengers: state }) => (
  state.fullyLoaded.includes(messengerId)
);

export const totalUnread = () => ({ messengers: state }) => (
  state.messengers.filter(mId => state.data.messengers[mId].unreadCount > 0).length
);

export const getActive = () => ({ messengers: state }) => state.active;

export const getIsTyping = messengerId => ({ messengers: state }) => (
  (state.typing[messengerId] || []).length > 0
);

export const isMessageOutgoing = messageId => ({ messengers: state, auth }) => {
  const message = state.data.directmessages[messageId];
  if (!message || !message.authorId) return true;
  return message.authorId === auth.me.id;
};

export const messageIsEdited = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return false;
  return message.edited;
};

export const isMessageEmojiOnly = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return false;
  return isEmojiOnly(message);
};

export const getMessageReactions = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return null;
  return message.reactions;
};

export const getMessageAuthorId = messageId => ({ messengers: state, auth }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return auth.me.id;
  return message.authorId || auth.me.id;
};

export const getMessageAuthorType = () => () => 'USER';

export const getMessageCreatedAt = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return null;
  return message.createdAt;
};

export const getMessageState = messageId => ({ messengers: state, auth }) => {
  if (!messageId) return null;

  const message = state.data.directmessages[messageId];
  const outgoing = message.authorId === auth.me.id;
  const messengerId = typeof message.messenger === 'object' ? message.messenger.id : message.messenger;
  const entity = state.data.messengers[messengerId];
  if (!entity) return 'SENDING';
  const otherParticipants = entity.participants.filter(p => p.userId !== auth.me.id);

  const timeIsGreater = (a, b) => (new Date(a)).getTime() >= (new Date(b).getTime());

  let messageState = 'SENT';
  if (outgoing) {
    if (!message.id) messageState = 'SENDING';
    else if (otherParticipants.every(p => timeIsGreater(p.readAt, message.createdAt))) {
      messageState = 'READ';
    }
    if (messageState === 'SENT' && otherParticipants.every(p => timeIsGreater(p.receivedAt, message.createdAt))) {
      messageState = 'RECEIVED';
    }
  } else {
    messageState = null;
  }

  return messageState;
};

export const getMessageGif = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message || !message.media) return null;
  return message.media.gif;
};

export const getMessageMedia = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return null;
  return message.media;
};

export const messageHasAudio = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return false;
  return !!message.audio;
};

export const getMessageAudioFile = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message || !message.audio || message.audio.loading) return null;
  return message.audio.file;
};

export const getReplyingTo = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return null;
  return message.replyingTo;
};

export const getMessageContentForEditor = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return null;
  return message.editorContent;
};

export const getMessageContent = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message || !message.rawContent) return null;
  return message.rawContent;
};

export const messageHasReactions = messageId => ({ messengers: state }) => {
  const message = state.data.directmessages[messageId];
  if (!message) return false;
  return (message.reactions || []).filter(r => r.authorIds.length > 0).length > 0;
};

export const messengerCount = () => ({ messengers: state }) => state.messengers.length;

export const allLastMessages = () => ({ messengers: state }) => {
  const messengerIds = state.messengers;
  const lastMessagesDates = {};
  messengerIds.forEach((id) => {
    lastMessagesDates[id] = getLastMessageByMessengerId(id)({ messengers: state });
  });

  return lastMessagesDates;
};

export const notRejectedRequestsCount = () => ({ messengers: state, auth }) => {
  const requested = getRequested()({ messengers: state, auth });
  return requested.filter(m => !m.rejected).length;
};

const getStatusData = messengerId => ({ messengers: state, auth }) => {
  const { participants } = state.data.messengers[messengerId];

  const approvedByMe = participants.find(p => p.userId === auth.me.id).approved;
  const approvedByOthers = participants.filter(p => p.userId !== auth.me.id).every(p => p.approved);
  const requestMessage = participants.map(p => p.requestMessage).find(Boolean);

  return {
    approvedByMe,
    approvedByOthers,
    requestMessage,
  };
};

export const needsRequest = messengerId => ({ messengers: state, auth }) => {
  const {
    approvedByMe,
    approvedByOthers,
    requestMessage,
  } = getStatusData(messengerId)({ messengers: state, auth });

  return (
    (!approvedByMe && !approvedByOthers)
    || (approvedByMe && !approvedByOthers && !requestMessage)
  );
};

export const isWaitingForApproval = messengerId => ({ messengers: state, auth }) => {
  const {
    approvedByMe,
    approvedByOthers,
    requestMessage,
  } = getStatusData(messengerId)({ messengers: state, auth });

  return approvedByMe && !approvedByOthers && requestMessage;
};

export const needsResolveRequest = messengerId => ({ messengers: state, auth }) => {
  const {
    approvedByMe,
    approvedByOthers,
    requestMessage,
  } = getStatusData(messengerId)({ messengers: state, auth });

  return !approvedByMe && approvedByOthers && requestMessage;
};

export const getUserId = messengerId => ({ messengers: state, auth }) => {
  const messenger = state.data.messengers[messengerId];
  const { userId } = messenger.participants.find(p => p.userId !== auth.me.id);
  return userId;
};

export const isArchived = messengerId => ({ messengers: state }) => {
  const messenger = state.data.messengers[messengerId];
  return messenger.archived;
};

export const isBlocked = messengerId => ({ messengers: state }) => {
  const messenger = state.data.messengers[messengerId];
  return messenger.blocked;
};

export const isRejected = messengerId => ({ messengers: state }) => {
  const messenger = state.data.messengers[messengerId];
  return messenger.rejected;
};

export const getUnreadCount = messengerId => ({ messengers: state }) => {
  const messenger = state.data.messengers[messengerId];
  return messenger.unreadCount;
};

export const getRequestMessage = messengerId => ({ messengers: state }) => {
  const { participants } = state.data.messengers[messengerId];
  return participants.map(p => p.requestMessage).find(Boolean);
};

export const getSortedArchived = () => ({ messengers: state, auth, users }) => {
  const messengers = getAll()({ messengers: state, auth });

  const myMessengers = messengers.filter(data => data.archived).map(data => ({ type: 'messenger', data }));
  const lastMessagesByMessenger = allLastMessages()({ messengers: state });

  const getEntityName = (entity) => {
    const { userId } = entity.data.participants.find(p => p.userId !== auth.me.id);
    const user = userSelectors.getById(userId)({ users });
    return user.displayname;
  };

  const sortedEntities = myMessengers
    .sort((a, b) => {
      const lastMessageA = lastMessagesByMessenger[a.data.id];
      const lastMessageB = lastMessagesByMessenger[b.data.id];

      if (!lastMessageA && !lastMessageB) return 0;
      if (lastMessageA && !lastMessageB) return -1;
      if (!lastMessageA && lastMessageB) return 1;

      const createdAtA = (
        lastMessageA.createdAt
        || (lastMessageA.payload && lastMessageA.payload.createdAt)
      );
      const createdAtB = (
        lastMessageB.createdAt
        || (lastMessageB.payload && lastMessageB.payload.createdAt)
      );
      if (createdAtA > createdAtB) return -1;

      return 1;
    })
    .map(entity => ({ type: entity.type, id: entity.data.id, name: getEntityName(entity) }));

  return sortedEntities;
};

export const getReplyTo = messageId => ({ messengers: state }) => state.replyTo[messageId];

export const isEditing = messengerId => ({ messengers: state }) => !!state.editing[messengerId];

export const getEditingMessageId = messengerId => ({ messengers: state }) => (
  state.editing[messengerId]
);

export const messageIsBeingRemoved = messageId => ({ messengers: state }) => (
  state.data.directmessages[messageId] && state.data.directmessages[messageId].removing
);
