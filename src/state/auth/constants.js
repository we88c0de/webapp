export const LOGIN = 'mazmo/auth/LOGIN';
export const LOGIN_SUCCESS = 'mazmo/auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = 'mazmo/auth/LOGIN_FAIL';

export const LOGOUT = 'mazmo/auth/LOGOUT';

export const EMAIL_CONFIRMED = 'mazmo/auth/EMAIL_CONFIRMED';

export const ADD_TO_FOLLOW_LIST = 'mazmo/auth/ADD_TO_FOLLOW_LIST';
export const REMOVE_FROM_FOLLOWING = 'mazmo/auth/REMOVE_FROM_FOLLOWING';
export const SET_TO_FOLLOW_LISTS = 'mazmo/auth/SET_TO_FOLLOW_LISTS';

export const ADD_TO_KNOWING = 'mazmo/auth/ADD_TO_KNOWING';
export const REMOVE_FROM_KNOWING = 'mazmo/auth/REMOVE_FROM_KNOWING';

export const ADD_TO_BLOCKS = 'mazmo/auth/ADD_TO_BLOCKS';
export const REMOVE_FROM_BLOCKS = 'mazmo/auth/REMOVE_FROM_BLOCKS';
