import { schema } from 'normalizr';

import user from 'state/users/schema';

const followList = new schema.Entity('followLists', {
  users: new schema.Values(user),
});

const auth = new schema.Entity('auth', {
  follow_lists: new schema.Values(followList),
});

export default auth;
