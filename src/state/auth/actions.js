import Api from 'state/api';
import * as Sentry from '@sentry/browser';
import ReactGA from 'react-ga';

import * as alertActions from 'state/alerts/actions';
import * as channelActions from 'state/channels/actions';
import * as messengerActions from 'state/messengers/actions';

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  EMAIL_CONFIRMED,
  ADD_TO_FOLLOW_LIST,
  REMOVE_FROM_FOLLOWING,
  SET_TO_FOLLOW_LISTS,
  ADD_TO_KNOWING,
  REMOVE_FROM_KNOWING,
  ADD_TO_BLOCKS,
  REMOVE_FROM_BLOCKS,
} from './constants';
import { LEGACY_URL } from '../../constants';

const parseRawData = rawData => async () => {
  // const normalizedData = normalize(rawData, authSchema);

  const followingUserIds = new Set();
  const followLists = [];
  Object.values(rawData.followLists).forEach((fl) => {
    followLists.push(fl);
    fl.users.forEach((uId) => {
      followingUserIds.add(uId);
    });
  });

  const data = {
    ...rawData,
    following_users: [...followingUserIds],
    follow_lists: followLists,
  };

  return data;
};

export const logout = () => async (dispatch) => {
  Api.setJWT('');
  const { notificationToken } = window.localStorage;
  localStorage.removeItem('jwt');
  if (notificationToken !== null) {
    localStorage.removeItem('notificationToken');
  }

  dispatch({ type: LOGOUT });

  dispatch(alertActions.wipe());
  dispatch(channelActions.wipe());
  dispatch(messengerActions.wipe());

  // REFACTOR - WIPE
  // memberships.dispatcher.wipe();
  // communities.dispatcher.wipe();
  // threads.dispatcher.wipe();
  // replies.dispatcher.wipe();
};

export const login = (username, password) => async (dispatch) => {
  try {
    dispatch({ type: LOGIN, auto: false });
    const { data: rawData } = await Api.req.post('/auth', { username, password });
    const data = await dispatch(parseRawData(rawData));

    if (data.status < 2) {
      Api.setJWT(data.jwt);
      localStorage.setItem('jwt', data.jwt);
      dispatch({ type: LOGIN_SUCCESS, data });

      Sentry.configureScope((scope) => {
        scope.setUser({
          id: data.id,
          username: data.username,
        });
      });
    }

    ReactGA.event({
      category: 'Users',
      action: 'Login',
      label: data.username,
    });

    return data;
    // TODO: Add me to users reducer
  } catch (err) {
    let error = err.message;
    if (err.response) {
      // eslint-disable-next-line
      error = err.response.data.error;
    } else if (err.request) {
      error = err.request;
    }

    dispatch(logout());
    dispatch({ type: LOGIN_FAIL, error });
    throw err;
  }
};

export const jwtLogin = jwt => async (dispatch) => {
  try {
    dispatch({ type: LOGIN, auto: true });
    Api.setJWT(jwt);
    const { data: rawData } = await Api.req.get('/auth/me');
    const data = await dispatch(parseRawData(rawData));

    dispatch({ type: LOGIN_SUCCESS, data: { ...data, jwt } });

    Sentry.configureScope((scope) => {
      scope.setUser({
        id: data.id,
        username: data.username,
      });
    });
    // TODO: Add me to users reducer
  } catch (error) {
    dispatch({ type: LOGOUT });
    dispatch({ type: LOGIN_FAIL, error });
  }
};

export const signup = params => async (dispatch) => {
  const { data: rawData } = await Api.req.post('/auth/signup', params);
  const data = await dispatch(parseRawData(rawData));

  Api.setJWT(data.jwt);
  localStorage.setItem('jwt', data.jwt);
  dispatch({ type: LOGIN_SUCCESS, data });

  ReactGA.event({
    category: 'Users',
    action: 'Register',
    label: data.username,
  });

  Sentry.configureScope((scope) => {
    scope.setUser({
      id: data.id,
      username: data.username,
    });
  });

  return data;
  // TODO: Add me to users reducer
};

export const recover = email => async () => Api.req.post('/auth/password-recovery', { email });

export const reset = (hash, password) => async (dispatch) => {
  const { data } = await Api.req.post('/auth/password-reset', { hash, password });
  Api.setJWT(data.jwt);
  localStorage.setItem('jwt', data.jwt);
  dispatch({ type: LOGIN_SUCCESS, data });

  ReactGA.event({
    category: 'Users',
    action: 'Recover',
    label: data.username,
  });

  Sentry.configureScope((scope) => {
    scope.setUser({
      id: data.id,
      username: data.username,
    });
  });
};

export const reactivate = (username, password) => async (dispatch) => {
  const { data } = await Api.req.put('/auth/reactivate', { username, password });
  Api.setJWT(data.jwt);
  localStorage.setItem('jwt', data.jwt);
  dispatch({ type: LOGIN_SUCCESS, data });

  ReactGA.event({
    category: 'Users',
    action: 'Reactivate',
    label: data.username,
  });

  Sentry.configureScope((scope) => {
    scope.setUser({
      id: data.id,
      username: data.username,
    });
  });
};

export const confirmEmail = hash => async (dispatch) => {
  await Api.req.post('/users/emailconfirmation', { hash });
  dispatch({ type: EMAIL_CONFIRMED });
};

export const follow = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const listId = getState().auth.me.follow_lists[0].id;
  const { username } = user;
  await Api.req.post(`${LEGACY_URL}/api/users/${username}/followers`, { listId });

  ReactGA.event({
    category: 'Users',
    action: 'Follow',
  });

  dispatch({
    type: ADD_TO_FOLLOW_LIST,
    listId,
    userId,
  });
};

export const unfollow = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.delete(`${LEGACY_URL}/api/users/${username}/followers`);

  ReactGA.event({
    category: 'Users',
    action: 'Unfollow',
  });

  dispatch({
    type: REMOVE_FROM_FOLLOWING,
    userId,
  });
};

export const know = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.post(`${LEGACY_URL}/api/users/${username}/knowed`);

  ReactGA.event({
    category: 'Users',
    action: 'Know',
  });

  dispatch({
    type: ADD_TO_KNOWING,
    userId,
  });
};

export const unknow = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.delete(`${LEGACY_URL}/api/users/${username}/knowed`);

  ReactGA.event({
    category: 'Users',
    action: 'Unknow',
  });

  dispatch({
    type: REMOVE_FROM_KNOWING,
    userId,
  });
};

export const block = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.post(`${LEGACY_URL}/api/users/${username}/blocks`);

  ReactGA.event({
    category: 'Users',
    action: 'Block',
  });

  dispatch({
    type: ADD_TO_BLOCKS,
    userId,
  });
};

export const unblock = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.delete(`${LEGACY_URL}/api/users/${username}/blocks`);

  ReactGA.event({
    category: 'Users',
    action: 'Unblock',
  });

  dispatch({
    type: REMOVE_FROM_BLOCKS,
    userId,
  });
};

export const report = (userId, reason, blockUser) => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  await Api.req.post(`${LEGACY_URL}/api/users/${username}/reports`, { reason, block: blockUser });

  ReactGA.event({
    category: 'Users',
    action: 'Report Created',
  });
};

export const listsChange = (userId, lists) => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return;

  const { username } = user;
  const followLists = getState().auth.me.follow_lists;
  const promises = followLists.map((list) => {
    const alreadyInList = list.users.some(id => id === userId);
    const shouldBeInList = lists.includes(list.id);

    if (alreadyInList === shouldBeInList) return null;
    if (shouldBeInList) {
      return Api.req.post(`${LEGACY_URL}/api/users/${username}/followers`, { listId: list.id });
    }
    return Api.req.delete(`${LEGACY_URL}/api/users/${username}/followers`, { listId: list.id });
  });

  await Promise.all(promises);

  ReactGA.event({
    category: 'Users',
    action: 'Follow Update',
  });

  dispatch({
    type: SET_TO_FOLLOW_LISTS,
    userId,
    lists,
  });
};

export const getInFeedMuted = () => async () => {
  const { data } = await Api.req.get('/feed/users/muted');
  return data;
};

export const muteInFeed = userId => async () => {
  await Api.req.post('/feed/users/muted', { userId });

  ReactGA.event({
    category: 'Users',
    action: 'Mute',
    label: 'Feed',
  });
};
export const unmuteInFeed = userId => async () => {
  await Api.req.delete('/feed/users/muted', { data: { userId } });

  ReactGA.event({
    category: 'Users',
    action: 'Unmute',
    label: 'Feed',
  });
};
