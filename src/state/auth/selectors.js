export const loggedIn = () => ({ auth: state }) => !!state.me;

export const getMe = () => ({ auth: state }) => state.me;

export const getUserId = () => ({ auth: state }) => {
  if (!state.me) return null;
  return state.me.id;
};

export const getUsername = () => ({ auth: state }) => state.me.username;

export const getEmail = () => ({ auth: state }) => state.me.email;

export const isConfirmed = () => ({ auth: state }) => state.me && state.me.status === 1;

export const isLoggingIn = () => ({ auth: state }) => state.loggingIn;

export const isAutoLoggingIn = () => ({ auth: state }) => state.loginAuto;

export const getLoginError = () => ({ auth: state }) => state.loginError;

export const getFollowingUserIds = () => ({ auth: state, users }) => {
  const isOnline = userId => users.online.includes(userId);

  return state.me.following_users
    .sort((a, b) => {
      if (isOnline(a) && !isOnline(b)) return -1;
      if (isOnline(b) && !isOnline(a)) return 1;

      return b - a;
    });
};

export const amIFollowing = userId => ({ auth: state }) => {
  if (!state.me) return null;
  return state.me.following_users.includes(userId);
};

export const getUserLists = () => ({ auth: state }) => state.me.follow_lists;

export const inListsIds = userId => ({ auth: state }) => {
  if (!state.me) return [];
  return state.me.follow_lists
    .filter(l => l.users.some(uId => uId === userId))
    .map(l => l.id);
};

export const doIKnow = userId => ({ auth: state }) => state.me.knowing.includes(userId);

export const isBlocked = userId => ({ auth: state }) => state.me.blocks.includes(userId);

export const isEmailConfirmed = () => state => state.auth.me && state.auth.me.status > 0;

export const hasEmailBounced = () => state => state.auth.me.bounced;

export const getNiches = () => ({ auth: state }) => {
  if (!state.me) return ['Lesb', 'Gay', 'Maledom', 'Femdom'];

  const { gender = '', role = '', orientation = '' } = state.me;
  const niches = [];

  if (gender === 'Hombre' || gender.includes('Varón') || gender.includes('masculino')) {
    if (orientation === 'Heterosexual') {
      niches.push('Lesb');

      if (role && (role && (role.includes('sumis') || role === 'spankee' || role === 'Masoquista'))) {
        niches.push('Femdom');
      } else if (role && (role === 'Dominante' || role === 'Spanker' || role.includes('Sádic'))) {
        niches.push('Maledom');
      } else {
        niches.push('Femdom');
        niches.push('Maledom');
      }
    } else if (orientation && (orientation.includes('Homo') || orientation === 'Gay' || orientation === 'Puto')) {
      niches.push('Gay');
    } else {
      niches.push('Gay');
      niches.push('Lesb');

      if (role && (role.includes('sumis') || role === 'spankee' || role === 'Masoquista')) {
        niches.push('Femdom');
      } else if (role && (role === 'Dominante' || role === 'Spanker' || role.includes('Sádic'))) {
        niches.push('Maledom');
      } else {
        niches.push('Femdom');
        niches.push('Maledom');
      }
    }
  } else if (gender && (gender.includes('Mujer') || gender.includes('femenin'))) {
    if (orientation === 'Heterosexual') {
      if (role && (role.includes('sumis') || role === 'spankee' || role === 'Masoquista')) {
        niches.push('Maledom');
      } else if (role && (role === 'Dominante' || role === 'Spanker' || role.includes('Sádic'))) {
        niches.push('Femdom');
      } else {
        niches.push('Femdom');
        niches.push('Maledom');
      }
    } else if (orientation && (orientation.includes('Homo') || orientation === 'Lesbiana' || orientation === 'Torta')) {
      niches.push('Lesb');
    } else {
      niches.push('Lesb');

      if (role && (role.includes('sumis') || role === 'spankee' || role === 'Masoquista')) {
        niches.push('Maledom');
      } else if (role && (role === 'Dominante' || role === 'Spanker' || role.includes('Sádic'))) {
        niches.push('Femdom');
      } else {
        niches.push('Femdom');
        niches.push('Maledom');
      }
    }
  } else {
    niches.push('Lesb');
    niches.push('Gay');
    niches.push('Maledom');
    niches.push('Femdom');
  }

  return niches;
};
