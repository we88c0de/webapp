import produce from 'immer';

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  EMAIL_CONFIRMED,
  ADD_TO_FOLLOW_LIST,
  REMOVE_FROM_FOLLOWING,
  SET_TO_FOLLOW_LISTS,
  ADD_TO_KNOWING,
  REMOVE_FROM_KNOWING,
  ADD_TO_BLOCKS,
  REMOVE_FROM_BLOCKS,
} from './constants';

export const initialState = {
  me: null,
  loggingIn: false,
  loginError: null,
  loginAuto: false,
};

const reducer = (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case LOGIN:
      draft.loggingIn = true;
      draft.loginError = null;
      draft.loginAuto = !!action.auto;
      break;

    case LOGIN_SUCCESS:
      draft.me = action.data;
      draft.loggingIn = false;
      draft.loginError = null;
      draft.loginAuto = false;
      break;

    case LOGIN_FAIL:
      draft.loggingIn = false;
      draft.loginError = action.error;
      draft.loginAuto = false;
      break;

    case LOGOUT:
      draft.loggingIn = false;
      draft.loginError = null;
      draft.loginAuto = false;
      draft.me = null;
      break;

    case EMAIL_CONFIRMED:
      draft.me.status = 1;
      break;

    case ADD_TO_FOLLOW_LIST: {
      draft.me.following_users.push(action.userId);
      const index = draft.me.follow_lists.findIndex(l => l.id === action.listId);
      draft.me.follow_lists[index].users = [
        ...state.me.follow_lists[index].users,
        action.userId,
      ];
      break;
    }

    case REMOVE_FROM_FOLLOWING:
      draft.me.following_users = state.me.following_users.filter(id => id !== action.userId);
      state.me.follow_lists.forEach((list, index) => {
        draft.me.follow_lists[index].users = list.users.filter(uId => uId !== action.userId);
      });
      break;

    case SET_TO_FOLLOW_LISTS:
      // We clean from the following users list (if it exists)
      draft.me.following_users = state.me.following_users.filter(id => id !== action.userId);
      if (action.lists.length) {
        // If should be in there, we add it
        draft.me.following_users.push(action.userId);
      }

      state.me.follow_lists.forEach((list, index) => {
        const users = list.users.filter(uId => uId !== action.userId);

        if (action.lists.includes(list.id)) {
          users.push(action.userId);
        }

        draft.me.follow_lists[index].users = users;
      });
      break;

    case ADD_TO_KNOWING:
      draft.me.knowing.push(action.userId);
      break;

    case REMOVE_FROM_KNOWING:
      draft.me.knowing = state.me.knowing.filter(id => id !== action.userId);
      break;

    case ADD_TO_BLOCKS:
      draft.me.blocks.push(action.userId);
      break;

    case REMOVE_FROM_BLOCKS:
      draft.me.blocks = state.me.blocks.filter(id => id !== action.userId);
      break;

    default:
  }
});

export default reducer;
