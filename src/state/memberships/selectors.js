import { MEMBERSHIP_ROLES } from '../../constants';

const adminRoles = [
  MEMBERSHIP_ROLES.OWNER,
  MEMBERSHIP_ROLES.ADMIN,
];
const modRoles = [
  ...adminRoles,
  MEMBERSHIP_ROLES.MODERATOR,
];

export const get = id => ({ memberships: state }) => state.data[id];

export const getById = id => ({ memberships: state }) => state.data[id];

export const getByCommunityId = id => ({ memberships: state }) => {
  if (typeof state.data === 'undefined' || state.data === null) return null;
  return Object.values(state.data).find(m => m.community === id);
};

export const getApproved = () => ({ memberships: state }) => {
  if (!state.data) return [];
  return (
    Object.values(state.data)
      .filter(membership => membership.approved)
  );
};

export const isLoaded = () => ({ memberships: state }) => state.data !== null;

export const hasApprovedMembership = communityId => ({ memberships: state }) => {
  if (!state.data) return false;
  return (
    Object.values(state.data)
      .some(m => m.community === communityId && m.approved)
  );
};

export const hasPendingMembership = communityId => ({ memberships: state }) => {
  if (typeof state.data === 'undefined' || state.data === null) return false;

  return Object
    .values(state.data)
    .some(m => m.community === communityId && !m.approved);
};

export const isLoading = slug => ({ memberships: state }) => state.loading.includes(slug);

export const userIsModOf = communityId => ({ memberships: state }) => {
  if (typeof state.data === 'undefined' || state.data === null) return false;

  return Object
    .values(state.data)
    .some(m => m.community === communityId && modRoles.includes(m.role));
};

export const userIsAdminOf = communityId => ({ memberships: state }) => {
  if (typeof state.data === 'undefined' || state.data === null) return false;

  return Object
    .values(state.data)
    .some(m => m.community === communityId && adminRoles.includes(m.role));
};

export const userIsOwnerOf = communityId => ({ memberships: state }) => {
  if (typeof state.data === 'undefined' || state.data === null) return false;

  return Object
    .values(state.data)
    .some(m => m.community === communityId && m.role === MEMBERSHIP_ROLES.OWNER);
};

export const approvedCount = () => ({ memberships: state }) => (
  getApproved()({ memberships: state }).length
);

export const getUserRole = communityId => ({ memberships: state }) => {
  const membership = Object.values(state.data).find(m => m.community === communityId);
  return membership.role;
};
