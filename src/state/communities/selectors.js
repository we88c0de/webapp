import moment from 'moment';

export const get = id => ({ communities: state }) => state.data[id];

export const getById = id => ({ communities: state }) => state.data[id];

export const getBySlug = slug => ({ communities: state }) => (
  Object.values(state.data).find(t => t.slug === slug)
);

export const isLoading = () => ({ communities: state }) => state.loading;

export const getAllNames = () => ({ communities: state }) => {
  const names = {};
  Object.values(state.data).forEach((community) => {
    names[community._id] = community.name;
  });

  return names;
};

export const getAllSlugs = () => ({ communities: state }) => {
  const slugs = {};
  Object.values(state.data).forEach((community) => {
    slugs[community._id] = community.slug;
  });

  return slugs;
};

export const getAllIdsSortedAndFiltered = (filter = '') => ({ communities: state }) => (
  Object.values(state.data)
    .filter(community => community.name.toLowerCase().includes(filter.toLowerCase()))
    .sort((a, b) => b.membershipCount - a.membershipCount)
    .map(community => community.id)
);

export const getSlugById = id => ({ communities: state }) => {
  const community = state.data[id];
  if (!community) return null;
  return community.slug;
};

export const getIdBySlug = slug => ({ communities: state }) => {
  if (!slug) return null;
  const community = Object.values(state.data).find(t => t.slug === slug);
  if (!community) return null; // Can happen, for example when deleting a community
  return community._id;
};

export const isFetching = slug => ({ communities: state }) => {
  if (state.data[slug]) return state.data[slug].loading;

  const community = Object.values(state.data).find(t => t.slug === slug);
  if (!community) return false;
  return community.loading;
};

export const isNotFound = slug => ({ communities: state }) => {
  const data = state.data[slug];
  if (!data) return false;
  return data.notFound;
};

export const userIsBanned = slug => ({ communities: state }) => {
  const data = state.data[slug];
  if (!data) return false;
  return data.banned;
};

export const hasData = slug => state => (
  Object.values(state.communities.data).some(t => t.slug === slug)
);

export const getPrivacy = id => ({ communities: state }) => {
  const community = state.data[id];
  if (!community) return null;
  return community.privacy;
};

export const getName = id => ({ communities: state }) => {
  const community = state.data[id];
  if (!community) return null;
  return community.name;
};

export const getDescription = id => ({ communities: state }) => state.data[id].description;

export const getAvatar = id => ({ communities: state }) => state.data[id].avatar;

export const getCover = id => ({ communities: state }) => state.data[id].cover;

export const getMembershipCount = id => ({ communities: state }) => state.data[id].membershipCount;

export const getThreadCount = id => ({ communities: state }) => state.data[id].threadCount;

export const getReplyCount = id => ({ communities: state }) => state.data[id].replyCount;

export const getMods = id => ({ communities: state }) => {
  const community = state.data[id];
  if (!community) return [];

  return community.mods || [];
};

export const isEmpty = communityId => (state) => {
  const community = state.communities.data[communityId];
  if (!community) return null;

  return state.threads.fullyLoaded.includes(communityId) && !community.threads.length;
};

const communitySort = (communityA, communityB) => {
  if (!communityA.lastUpdate && !communityB.lastUpdate) return 0;
  if (communityA.lastUpdate && !communityB.lastUpdate) return -1;
  if (!communityA.lastUpdate && communityB.lastUpdate) return 1;
  return moment(communityB.lastUpdate.at).diff(communityA.lastUpdate.at);
};

export const getMyCommunityIdsSorted = () => ({ communities, memberships }) => {
  const myMemberships = Object.values(memberships.data).filter(membership => membership.approved);
  const uniqueMemberships = new Set();
  myMemberships.forEach(mbsp => uniqueMemberships.add(mbsp.community));
  const myCommunities = [...uniqueMemberships]
    .map(id => communities.data[id])
    .sort(communitySort)
    .map(c => c.id);

  return myCommunities;
};
