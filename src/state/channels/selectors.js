import memoizeOne from 'memoize-one';
import moment from 'moment';

import isEmojiOnly from 'utils/isEmojiOnly';
import filterEntities from 'utils/filterEntities';
import * as userSelectors from 'state/users/selectors';

import { CHANNEL_ROLES, CM_AUTHOR_TYPES } from '../../constants';

const MOD_ROLES = [CHANNEL_ROLES.OWNER, CHANNEL_ROLES.MOD];

export const getAll = () => ({ channels: state }) => state.channels
  .map(c => state.data.channels[c]);

export const getById = id => ({ channels: state }) => state.data.channels[id];

export const amISubscribed = id => ({ channels: state }) => state.channels.includes(id);

export const isFetching = () => ({ channels: state }) => state.fetching;

export const isInitialized = () => ({ channels: state }) => state.initialized;

export const hasFailed = () => ({ channels: state }) => !!state.fetchError;

export const getMessagesByChannelId = channelId => ({ channels: state }) => (
  (state.channelmessages[channelId] || []).map(id => state.data.channelmessages[id])
);

export const getLastMessageByChannelId = channelId => ({ channels: state }) => {
  const messages = getMessagesByChannelId(channelId)({ channels: state });
  return messages[messages.length - 1];
};

export const getMessageAuthorId = messageId => ({ channels: state, auth }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return auth.me.id;
  if (message.authorId) return message.authorId;
  if (message.author) {
    if (message.author.type === 'AUTORESPONDER') return message.channel;
    return message.author.id;
  }
  return auth.me.id;
};

export const getMessageAuthorType = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return CM_AUTHOR_TYPES.USER;
  if (message.author) return message.author.type;
  return CM_AUTHOR_TYPES.USER;
};

export const isMessageOutgoing = messageId => ({ channels: state, auth }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return true;
  if (message.authorId) return message.authorId === auth.me.id;
  if (message.author) return message.author.id === auth.me.id;
  if (!message.authorId) return true;
  return message.authorId === auth.me.id;
};

export const messageIsEdited = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return false;
  return message.edited;
};

export const getMessageCreatedAt = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return null;
  return message.createdAt;
};

export const isMessageEmojiOnly = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId].payload;
  if (!message) return false;
  return isEmojiOnly(message);
};

export const getMessageReactions = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return null;
  return message.reactions;
};

export const getMessageGif = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message || !message.payload || !message.payload.media) return null;
  return message.payload.media.gif;
};

export const getMessageMedia = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message || !message.payload) return null;
  return message.payload.media;
};

export const getReplyingTo = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return null;
  return message.replyingTo;
};

export const getMessageContentForEditor = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return null;
  return message.editorContent;
};

export const getMessageContent = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message || !message.payload || !message.payload.rawContent) return null;
  return message.payload.rawContent;
};

export const messageHasReactions = messageId => ({ channels: state }) => {
  const message = state.data.channelmessages[messageId];
  if (!message) return false;
  return (message.reactions || []).filter(r => r.authorIds.length > 0).length > 0;
};

export const isFullyLoaded = channelId => ({ channels: state }) => (
  state.fullyLoaded.includes(channelId)
);

export const haveUnread = () => ({ channels: state, auth }) => state.channels.some((c) => {
  const channel = state.data.channels[c];
  if (!channel || !channel.participants) return false;

  const participant = channel.participants.find(p => p.userId === auth.me.id);
  const lastMessage = state.data.channelmessages[channel.lastMessage];
  if (!lastMessage || !participant) return false;
  return channel.id !== state.active && moment(participant.readAt).isBefore(lastMessage.createdAt);
});

export const getUnreadById = id => ({ channels: state }) => state.data.channels[id].unreadCount;

export const totalUnread = () => ({ channels: state }) => state.channels
  .reduce((sum, mId) => (sum + state.data.channels[mId].unreadCount), 0);

export const getActive = () => ({ channels: state }) => state.active;

export const getIsTyping = channelId => ({ channels: state }) => (
  (state.typing[channelId] || []).length > 0
);

export const getUnreadMentions = channelId => ({ channels: state }) => (
  state.unreadMentions[channelId] || 0
);

export const getParticipantIds = channelId => ({ channels: state }) => (
  state.data.channels[channelId].participants.map(p => p.userId).sort()
);

export const getParticipantsByChannelId = channelId => ({ channels: state }) => state
  .data.channels[channelId].participants;

export const channelCount = () => ({ channels: state }) => state.channels.length;

export const allLastMessages = () => ({ channels: state }) => {
  const channelIds = state.channels;
  const lastMessagesDates = {};
  channelIds.forEach((id) => {
    lastMessagesDates[id] = getLastMessageByChannelId(id)({ channels: state });
  });

  return lastMessagesDates;
};

export const onlineCount = channelId => ({ channels: state, users }) => {
  const channel = state.data.channels[channelId];
  if (!channel) return 'N/A';

  return channel
    .participants
    .filter(p => users.online.includes(p.userId))
    .length;
};

export const membersCount = channelId => ({ channels: state }) => (
  state.data.channels[channelId].participants.length
);

export const isOwnerOfChannel = channelId => ({ channels: state, auth }) => (
  state.data.channels[channelId].participants
    .find(p => p.role === CHANNEL_ROLES.OWNER).userId === auth.me.id
);

export const isModOfChannel = channelId => ({ channels: state, auth }) => {
  const myParticipation = state.data.channels[channelId].participants
    .find(p => p.userId === auth.me.id);
  return MOD_ROLES.includes(myParticipation.role);
};

export const iAmSuperior = (channelId, userId) => ({ channels: state, auth }) => {
  const roleIndex = (uId) => {
    const participant = state.data.channels[channelId].participants.find(p => p.userId === uId);
    if (!participant) return 999;
    return Object.values(CHANNEL_ROLES).indexOf(participant.role);
  };

  return roleIndex(auth.me.id) < roleIndex(userId);
};

export const getName = id => ({ channels: state }) => (
  state.data.channels[id] ? state.data.channels[id].name : false
);

export const getDescription = id => ({ channels: state }) => (
  state.data.channels[id] ? state.data.channels[id].description : false
);

export const getPrivacy = id => ({ channels: state }) => (
  state.data.channels[id] ? state.data.channels[id].privacy : false
);

export const getAvatar = id => ({ channels: state }) => {
  const channel = state.data.channels[id];
  if (!channel) return null;
  return channel.avatar;
};

const sortParticipants = (participants, users) => (
  participants
    .slice()
    .sort((a, b) => {
      const roleIndex = role => Object.values(CHANNEL_ROLES).indexOf(role);
      const displayname = userId => (users.data[userId] ? users.data[userId].displayname : 'zzzz'); // Hack to send it to the bottom

      if (users.online.includes(a.userId) && !users.online.includes(b.userId)) return -1;
      if (!users.online.includes(a.userId) && users.online.includes(b.userId)) return 1;
      if (roleIndex(a.role) < roleIndex(b.role)) return -1;
      if (roleIndex(a.role) > roleIndex(b.role)) return 1;
      if (displayname(a.userId) < displayname(b.userId)) return -1;
      if (displayname(a.userId) > displayname(b.userId)) return 1;

      return 0;
    })
    .map(p => p.userId)
);

// Used to only recompute memoized function if participants length or online users changed
// Note: In participants we only check length because can't change the list maintaining the
// length without passing through this function
const participantSortEquality = ([newParticipants, newUsers], [lastParticipants, lastUsers]) => {
  if (newParticipants.length !== lastParticipants.length) return false;
  if (newUsers.online.length !== lastUsers.online.length) return false;

  return newUsers.online.every(uId => lastUsers.online.includes(uId));
};

const memoizedSortParticipants = memoizeOne(sortParticipants, participantSortEquality);

export const getParticipantsUsersIdInOrder = id => ({ channels: state, users }) => {
  const { participants } = state.data.channels[id];

  const filtered = participants.length < 500
    ? participants
    : participants.filter(p => users.online.includes(p.userId));

  return memoizedSortParticipants(filtered, users);
};

export const getRoleByUserId = (channelId, userId) => ({ channels: state }) => {
  const participant = state.data.channels[channelId].participants.find(p => p.userId === userId);
  if (!participant) return null;
  return participant.role;
};

export const getModsByChannelId = channelId => ({ channels: state }) => (
  state.data.channels[channelId].participants.filter(p => MOD_ROLES.includes(p.role))
);

export const getBansByChannelId = channelId => ({ channels: state }) => (
  state.data.channels[channelId].bans
);

export const getSortedAndFilteredEntities = filter => (state) => {
  const list = state.app.chatList;
  if (!filter) return list;

  const getEntityName = (entity) => {
    if (entity.type === 'messenger') {
      const data = state.messengers.data.messengers[entity.id];
      const { userId } = data.participants.find(p => p.userId !== state.auth.me.id);
      const user = userSelectors.getById(userId)(state);
      return user.displayname;
    }

    const data = state.channels.data.channels[entity.id];
    return data.name;
  };

  return filterEntities(
    list
      .map(entity => ({ type: entity.type, id: entity.id, name: getEntityName(entity) })),
    filter,
  );
};

export const hasUnread = channelId => ({ channels: state, auth }) => {
  const channel = state.data.channels[channelId];
  if (!channel.participants) return false;
  const me = channel.participants.find(p => p.userId === auth.me.id);
  if (!me) return false;

  const lastMessage = getLastMessageByChannelId(channelId)({ channels: state });
  return lastMessage && moment(me.readAt).isBefore(lastMessage.createdAt);
};

export const botIds = channelId => ({ channels: state }) => {
  const channel = state.data.channels[channelId];
  if (!channel || !channel.bots) return [];

  return channel.bots.map(b => (typeof b.bot === 'object' ? b.bot.id : b.bot));
};

export const getBotById = botId => ({ channels: state }) => state.data.bots[botId];

export const getReplyTo = channelId => ({ channels: state }) => state.replyTo[channelId];

export const isEditing = channelId => ({ channels: state }) => !!state.editing[channelId];

export const getEditingMessageId = channelId => ({ channels: state }) => state.editing[channelId];

export const messageIsBeingRemoved = messageId => ({ channels: state }) => (
  state.data.channelmessages[messageId] && state.data.channelmessages[messageId].removing
);
