import PropTypes from 'prop-types';

export default PropTypes.shape({
  avatar: PropTypes.string,
  name: PropTypes.string,
  age: PropTypes.number,
  gender: PropTypes.oneOfType([
    PropTypes.shape({
      index: PropTypes.number,
      text: PropTypes.string,
    }),
    PropTypes.string,
  ]),
  role: PropTypes.oneOfType([
    PropTypes.shape({
      index: PropTypes.number,
      text: PropTypes.string,
    }),
    PropTypes.string,
  ]),
  location: PropTypes.string,
  description: PropTypes.string,
  postsQuantity: PropTypes.number,
  joinDate: PropTypes.number,
});
