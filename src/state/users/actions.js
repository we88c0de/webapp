import { normalize } from 'normalizr';

import Api from 'state/api';

import userSchema from './schema';
import {
  ADD,
  USER_QUEUE_ADD,
  FETCHING_USERS,
  USER_QUEUE_CLEAR,
  ONLINE_LIST,
  ONLINE,
  OFFLINE,
  ADD_FAILED_USERNAME,
} from './constants';
import { LEGACY_URL } from '../../constants';

export const fetchData = id => (dispatch, getState) => {
  const user = getState().users.data[id];
  if (!user) {
    if (!getState().users.userFetchQueue.has(id)) dispatch({ type: USER_QUEUE_ADD, id });

    return {
      id,
      username: 'loading...',
      displayname: 'loading...',
      loading: true,
      avatar: '',
    };
  }

  return {
    ...user,
    online: getState().users.online.includes(id),
  };
};

export const add = users => (dispatch, getState) => {
  if (typeof users === 'number') {
    const u = getState().users.data[users];
    if (!u && !getState().users.userFetchQueue.has(users)) {
      dispatch({ type: USER_QUEUE_ADD, id: users });
    }
  } else {
    dispatch({ type: ADD, data: users });
  }
};

export const loadFromIds = ids => (dispatch, getState) => {
  ids.forEach((id) => {
    const user = getState().users.data[id];
    if (!user && !getState().users.userFetchQueue.has(id)) dispatch({ type: USER_QUEUE_ADD, id });
  });
};

export const fetchFromQueue = () => async (dispatch, getState) => {
  const state = getState().users;
  const isFetching = state.userFetching;
  const users = [...state.userFetchQueue].filter(id => !Object.keys(state.data).includes(id));

  if (!isFetching && users.length) {
    dispatch({ type: FETCHING_USERS, fetching: true });

    const { data } = await Api.req.get('/users', {
      params: { ids: users.join(',') },
    });

    dispatch({ type: ADD, data });
    dispatch({ type: USER_QUEUE_CLEAR });
    dispatch({ type: FETCHING_USERS, fetching: false });
  }
};

export const fetchByUsername = username => async (dispatch, getState) => {
  if (getState().users.failedUsernames.includes(username.trim().toLowerCase())) {
    throw new Error('FAILED_USERNAME');
  }

  try {
    const { data: rawData } = await Api.req.get(`https://old.mazmo.net/api/users/${username.trim()}`, {
      params: {
        relationships: true,
      },
    });
    const data = normalize(rawData, userSchema);

    dispatch({
      type: ADD,
      data: {
        ...data.entities.users,
        [data.result]: {
          ...data.entities.users[data.result],
          fullyLoaded: true,
        },
      },
    });

    return { ...rawData };
  } catch (error) {
    dispatch({ type: ADD_FAILED_USERNAME, username: username.trim().toLowerCase() });
    throw error;
  }
};

export const searchByName = (search, max) => (dispatch, getState) => {
  const { online, data: users } = getState().users;

  const match = user => (
    user && search && (
      (user.displayname && user.displayname.toLowerCase().includes(search.toLowerCase()))
      || (user.username && user.username.toLowerCase().includes(search.toLowerCase()))
    )
  );

  const onlines = [];
  online.forEach((userId) => {
    const user = users[userId];
    if (user && match(user)) {
      onlines.push(user);
    }
  });

  if (onlines.length >= max) return onlines.slice(0, max);

  const offlines = Object.values(users)
    .filter(user => !online.includes(user.id))
    .filter(match)
    .slice(0, (max - onlines.length));

  return [...onlines, ...offlines];
};

export const getKnowing = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return [];

  const { username } = user;
  const { data } = await Api.req.get(`${LEGACY_URL}/api/users/${username}/knowed`);
  const ids = data.map(know => know.user.id);
  return ids;
};

export const getFollowers = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return [];

  const { username } = user;
  const { data } = await Api.req.get(`${LEGACY_URL}/api/users/${username}/followers`);
  return data;
};

export const getFollowing = userId => async (dispatch, getState) => {
  const user = getState().users.data[userId];
  if (!user) return [];

  const { username } = user;
  const { data } = await Api.req.get(`${LEGACY_URL}/api/users/${username}/following`);
  return data;
};

export const loadNotificationsConfig = key => async () => {
  const params = {};
  if (key) params.key = key;
  const { data } = await Api.req.get('/notifications/config', { params });
  return data;
};

export const saveNotificationsConfigEmail = (body, key) => async () => {
  const params = {};
  if (key) params.key = key;

  const { data } = await Api.req.put('/notifications/config/email', body, { params });
  return data;
};

export const saveNotificationsConfigPush = (body, key) => async () => {
  const params = {};
  if (key) params.key = key;

  const { data } = await Api.req.put('/notifications/config/push', body, { params });
  return data;
};

export const handleOnlineList = ({ online }) => (dispatch, getState) => {
  const total = getState().users.online.length;

  // $FIXME: Hack to prevent big jumps in online list that freezes the
  // browser in old and mobile devices
  const diff = online.length - total;
  if (diff < 200 || !total) {
    dispatch({
      type: ONLINE_LIST,
      data: online,
    });
  }
};

export const handleOnline = ({ userId }) => dispatch => dispatch({ type: ONLINE, userId });

export const handleOffline = ({ userId }) => dispatch => dispatch({ type: OFFLINE, userId });
