import produce from 'immer';

import {
  ADD,
  USER_QUEUE_ADD,
  USER_QUEUE_CLEAR,
  FETCHING_USERS,
  ADD_FAILED_USERNAME,
  ONLINE_LIST,
  ONLINE,
  OFFLINE,
} from './constants';

export const initialState = {
  data: {},
  online: [],
  userFetchQueue: new Set(),
  userFetching: false,
  failedUsernames: [],
};

const reducer = (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case ADD:
      Object.values(action.data || {}).forEach((user) => {
        draft.data[user.id] = {
          ...state.data[user.id],
          ...user,
        };
      });
      break;

    case USER_QUEUE_ADD:
      if (
        !state.userFetchQueue.has(action.id)
        && (!state.data[action.id] || !state.data[action.id].loading)
      ) {
        draft.userFetchQueue.add(action.id);
      }
      break;

    case FETCHING_USERS:
      draft.userFetching = action.fetching;
      break;

    case USER_QUEUE_CLEAR:
      draft.userFetchQueue.clear();
      break;

    case ADD_FAILED_USERNAME:
      draft.failedUsernames.push(action.username);
      break;

    case ONLINE_LIST:
      draft.online = action.data;
      break;

    case ONLINE:
      draft.online.push(action.userId);
      break;

    case OFFLINE:
      draft.online.splice(draft.online.findIndex(id => id === action.userId), 1);
      break;

    default:
  }
});

export default reducer;
