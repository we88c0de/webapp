export const getById = id => ({ users: state }) => {
  if (!id) return null;
  const user = state.data[id];
  if (!user) {
    return {
      id,
      username: 'loading...',
      displayname: 'loading...',
      loading: true,
      avatar: '',
    };
  }

  const online = state.online.includes(id);
  return {
    ...user,
    online,
  };
};

export const isLoaded = userId => state => userId && !!state.users.data[userId];

export const getByUsername = username => (state) => {
  const user = Object.values(state.users.data)
    .find(u => u.username && username && u.username.toLowerCase() === username.toLowerCase());
  if (!user) return null;
  return user.id;
};

export const getBatchByIds = ids => ({ users: state }) => {
  const result = {};
  ids.forEach((id) => {
    result[id] = getById(id)({ users: state });
  });

  return result;
};

export const getListByIds = ids => ({ users: state }) => (
  ids.map(id => getById(id)({ users: state }))
);

export const isBlockingMe = userId => (state) => {
  const user = getById(userId)(state);
  if (!user) return null;
  return user.blocked;
};

export const isOrganization = userId => (state) => {
  const user = getById(userId)(state);
  if (!user) return null;
  return user.is_organization;
};

export const getUsername = id => (state) => {
  const user = getById(id)(state);
  if (!user) return null;
  return user.username;
};

export const getDisplayName = id => (state) => {
  const user = getById(id)(state);
  if (!user) return null;
  return user.displayname;
};

export const isActive = id => (state) => {
  const user = getById(id)(state);
  if (!user || user.active !== false) return true;
  return false;
};

export const getCover = id => (state) => {
  const user = getById(id)(state);
  if (!user) return null;
  return user.cover;
};

export const getRegdate = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.regdate;
};

export const getWork = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.work;
};

export const getLastLogin = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.lastlogin;
};

export const getRelationships = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.relationships;
};

export const getAboutMe = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.about_me;
};

export const getAge = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.age;
};

export const getGender = id => (state) => {
  const user = state.users.data[id];
  if (!user || !user.gender) return null;
  return user.gender.text;
};

export const getRole = id => (state) => {
  const user = state.users.data[id];
  if (!user || !user.role) return null;
  return user.role.text;
};

export const getOrientation = id => (state) => {
  const user = state.users.data[id];
  if (!user || !user.orientation) return null;
  return user.orientation.text;
};

export const getPronoun = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.pronoun;
};

export const getKnown = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.known;
};

export const getEventCount = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.event_count;
};

export const getFollowers = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.followers;
};

export const getFollowing = id => (state) => {
  const user = state.users.data[id];
  if (!user) return null;
  return user.following;
};

export const usersToFetch = () => ({ users: state }) => (
  [...state.userFetchQueue].filter(id => !Object.keys(state.data).includes(id))
);

export const isFetching = () => ({ users: state }) => state.userFetching;

export const isFullyLoaded = userId => ({ users: state }) => {
  const user = state.data[userId];
  if (!user) return false;
  return user.fullyLoaded;
};

export const onlineList = () => ({ users: state }) => state.online;

export const isOnline = id => ({ users: state }) => state.online.includes(id);

export const onlineCount = ids => ({ users: state }) => ids
  .filter(id => state.online.includes(id))
  .length;

export const getOnlineIdsMatching = ids => ({ users: state }) => ids
  .filter(id => state.online.includes(id));

export const getSuggestions = (suggestions, filter) => ({ users: state, channels }) => {
  const MAX_CAP = 5;

  if (suggestions && suggestions.type === 'channel') {
    const ids = channels.data.channels[suggestions.id].participants.map(p => p.userId);
    const filtered = ids
      .filter((userId) => {
        const user = state.data[userId];

        return (
          user
          && (
            user.username.toLowerCase().includes(filter.toLowerCase())
            || user.displayname.toLowerCase().includes(filter.toLowerCase())
          )
        );
      })
      .map((userId) => {
        const user = state.data[userId];
        return { userId, name: user.displayname };
      });

    const cap = Math.min(filtered.length, MAX_CAP);
    return filtered.slice(0, cap);
  }

  return [];
};

export const infoLoaded = userId => (state) => {
  const user = state.data[userId];
  if (!user) return null;
  return user.infoLoaded;
};
