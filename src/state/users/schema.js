import { schema } from 'normalizr';

const user = new schema.Entity('users');
user.define({
  relationships: [
    { related_to: user },
  ],
});

export default user;
