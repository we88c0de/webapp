export const ADD = 'mazmo/users/ADD';

export const USER_QUEUE_ADD = 'mazmo/users/USER_QUEUE_ADD';
export const USER_QUEUE_CLEAR = 'mazmo/users/USER_QUEUE_CLEAR';
export const FETCHING_USERS = 'mazmo/users/FETCHING_USERS';

export const ADD_FAILED_USERNAME = 'mazmo/users/ADD_FAILED_USERNAME';

export const ONLINE_LIST = 'mazmo/users/ONLINE_LIST';
export const ONLINE = 'mazmo/users/ONLINE';
export const OFFLINE = 'mazmo/users/OFFLINE';
