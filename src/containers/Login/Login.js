import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import * as authActions from 'state/auth/actions';
import * as authSelectors from 'state/auth/selectors';

import colors from 'utils/css/colors';
import { useInputValue, useTranslation, useFocusOnMount } from 'hooks';

import AuthLayout from 'components/AuthLayout';
import Input from 'components/Forms/InputSimple';
import Button from 'components/Button';

import Reactivate from './Reactivate';
import locales from './i18n';

const ForgotPassword = styled.div`
  margin: 20px 0;
  text-align: left;
  margin-top: 25px;

  a {
    color: ${colors.redReactions};
    text-decoration: none;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
  }
`;

const Login = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const loginError = useSelector(authSelectors.getLoginError());

  const usernameEl = useFocusOnMount();
  const [loggingIn, setLoggingIn] = useState(false);
  const username = useInputValue('');
  const password = useInputValue('');

  const [reactivate, setReactivate] = useState(false);

  const login = async () => {
    try {
      setLoggingIn(true);
      const data = await dispatch(authActions.login(username.value, password.value));

      if (data.status === 3) setReactivate(true);
    } catch (error) {
      setLoggingIn(false);
    }
  };

  if (reactivate) return <Reactivate username={username.value} password={password.value} />;

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      login();
    }
  };

  return (
    <AuthLayout
      title={t('Enter your account')}
      subtitle={(
        <>
          {t('You don\'t have an account?')}
          {' '}
          <Link to="/signup">{t('Sign up')}</Link>
        </>
      )}
    >
      <Input
        ref={usernameEl}
        placeholder={t('Username')}
        {...username}
        disabbled={loggingIn}
        onKeyPress={handleKeyPress}
        withBorder
      />
      <Input
        placeholder={t('Password')}
        type="password"
        {...password}
        disabbled={loggingIn}
        onKeyPress={handleKeyPress}
        withBorder
      />

      {loginError && (
        <div>
          {typeof loginError === 'object' ? loginError.message : loginError}
        </div>
      )}

      <ForgotPassword>
        <Link to="/forgot-password">{t('I forgot my password')}</Link>
      </ForgotPassword>

      <Button
        onClick={login}
        loading={loggingIn}
        content={t('Enter')}
      />
    </AuthLayout>
  );
};

export default Login;
