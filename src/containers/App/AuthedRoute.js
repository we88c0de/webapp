import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Redirect,
} from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import Loading from 'components/Loading';

const AuthedRoute = ({ component: Component, loading, ...rest }) => {
  const me = useSelector(authSelectors.getMe(), shallowEqual);

  if (loading) return <Loading />;

  return (
    <Route
      {...rest}
      render={props => (
        me
          ? <Component {...props} />
          : <Redirect to="/login" />
      )}
    />
  );
};

AuthedRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  loading: PropTypes.bool,
};

AuthedRoute.defaultProps = {
  loading: false,
};

export default AuthedRoute;
