import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import isMobile from 'utils/isMobile';
import * as appActions from 'state/app/actions';

import * as serviceWorker from '../../serviceWorker';

const ServiceWorkerWrapper = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const onSWUpdate = (registration) => {
      dispatch(appActions.swWaiting(registration.waiting));
    };

    if (isMobile) {
      serviceWorker.register({
        onUpdate: onSWUpdate,
      });
    } else {
      serviceWorker.unregister();
    }
  }, [dispatch]);

  return null;
};

ServiceWorkerWrapper.propTypes = {
};

ServiceWorkerWrapper.defaultProps = {
};

export default ServiceWorkerWrapper;
