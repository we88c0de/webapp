import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Trans } from 'react-i18next';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from './i18n';

const BouncedAlert = () => {
  const { t } = useTranslation(locales);
  const history = useHistory();

  const [closed, setClosed] = useState(false);
  const hasBounced = useSelector(authSelectors.hasEmailBounced());
  const address = useSelector(authSelectors.getEmail());

  const updateEmail = useCallback(() => {
    history.push('/users/edit/email');
    setClosed(true);
  }, [history]);

  if (!hasBounced || closed) return null;

  return (
    <Modal
      title={t('Your email is invalid')}
      actions={[
        <Button key="invalid-email-button" onClick={updateEmail}>{t('Fix the problem')}</Button>,
      ]}
    >
      <Trans t={t} i18nKey="email.invalid" ns="App" values={{ address }}>
        <p>
          We sent you an email message to
          {' '}
          <strong>{address}</strong>
          {' '}
          that was bounced, so your account is now
          {' '}
          <strong>UNCONFIRMED</strong>
          .
        </p>
      </Trans>
      <p>{t('This means that you won\'t be able to use the site fully until you address this issue. User accounts need to be always confirmed with valid email addresses in order to keep your account reliable.')}</p>
    </Modal>
  );
};

BouncedAlert.propTypes = {
};

BouncedAlert.defaultProps = {
};

export default BouncedAlert;
