import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTitle, useTranslation } from 'hooks';
import * as communitySelectors from 'state/communities/selectors';
import * as membershipSelectors from 'state/memberships/selectors';
import * as communityActions from 'state/communities/actions';

import Info from 'containers/Communities/Info';
import NotFound from 'containers/NotFound';
import NotAuthorized from 'containers/Communities/NotAuthorized';
import Layout from 'components/Layout';
import Loading from 'components/Loading';

import locales from './i18n';
import Content from './Content';

const Pending = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const params = useParams();

  const communityId = useSelector(communitySelectors.getIdBySlug(params.communitySlug));
  const isFetching = useSelector(communitySelectors.isFetching(params.communitySlug));
  const isMod = useSelector(membershipSelectors.userIsModOf(communityId));

  useEffect(() => {
    dispatch(communityActions.fetch(params.communitySlug));
  }, [dispatch, params.communitySlug]);

  useTitle(t('Pending'));

  if (!communityId && !isFetching) return <NotFound />;
  if (isFetching) return <Loading />;
  if (!isMod) return <NotAuthorized communityId={communityId} />;

  return (
    <Layout columns={2}>
      <Info communitySlug={params.communitySlug} />
      <Content communityId={communityId} />
    </Layout>
  );
};

Pending.propTypes = {
};

Pending.defaultProps = {
};

export default Pending;
