import React, { useCallback } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import { useParams, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as communitySelectors from 'state/communities/selectors';
import * as threadSelectors from 'state/threads/selectors';
import * as threadActions from 'state/threads/actions';

import Loading from './Loading';

const LoadMore = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const location = useLocation();

  const userIsLoggedIn = useSelector(authSelectors.loggedIn());

  const communitySlug = (!userIsLoggedIn || (params.communitySlug && !location.search.includes('showAll'))) ? params.communitySlug : null;
  const communityId = useSelector(communitySelectors.getIdBySlug(communitySlug)) || 'recent';

  const loadBeforeDate = useSelector(threadSelectors.getLastLoadedThreadDate(communityId));
  const isFullyLoaded = useSelector(threadSelectors.isFullyLoaded(communityId));
  const isLoading = useSelector(threadSelectors.isLoading(communityId));

  const loadMoreVisibilityChanged = useCallback((isVisible) => {
    if (isVisible && !isLoading) {
      if (communityId === 'recent') {
        dispatch(threadActions.loadRecent(loadBeforeDate));
      } else {
        dispatch(threadActions.loadByCommunity(communityId, loadBeforeDate));
      }
    }
  }, [communityId, isLoading, dispatch, loadBeforeDate]);

  if (isFullyLoaded) return null;

  return (
    <VisibilitySensor onChange={loadMoreVisibilityChanged} partialVisibility>
      <Loading />
    </VisibilitySensor>
  );
};

LoadMore.propTypes = {
};

export default LoadMore;
