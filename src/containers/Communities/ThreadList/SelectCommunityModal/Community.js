import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as communitySelectors from 'state/communities/selectors';

import { SelectableListItem } from 'components/SelectableList';
import CommunityAvatar from 'components/CommunityAvatar';

const Community = ({ communityId }) => {
  const name = useSelector(communitySelectors.getName(communityId));
  const slug = useSelector(communitySelectors.getSlugById(communityId));

  return (
    <SelectableListItem
      key={`languages-${communityId}`}
      title={name}
      to={`/+${slug}/new`}
      avatar={<CommunityAvatar communityId={communityId} />}
    />
  );
};

Community.propTypes = {
  communityId: PropTypes.string.isRequired,
};

Community.defaultProps = {
};

export default Community;
