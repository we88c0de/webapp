import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import * as threadSelectors from 'state/threads/selectors';

import { Comment, Spank } from 'components/Icons';

const Wrapper = styled.ul`
  display: flex;
`;

const Item = styled.li`
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;
  &:first-child {
    margin-right: 16px;
  }
`;

const IconWrapper = styled.div`
  width: 16px;
`;

const Content = styled.span`
  color: ${colors.redReactions};
  font-size: 12px;
  font-weight: 400;
  margin-left: 4px;
`;

const Status = ({ threadId }) => {
  const commentCount = useSelector(threadSelectors.getCommentCount(threadId));
  const spankCount = useSelector(threadSelectors.getReactionsCount(threadId));

  return (
    <Wrapper>
      <Item>
        <IconWrapper>
          <Comment color={colors.redReactions} width="16px" height="16px" />
        </IconWrapper>
        <Content>{commentCount}</Content>
      </Item>
      <Item>
        <IconWrapper>
          <Spank color={colors.redReactions} width="16px" height="16px" />
        </IconWrapper>
        <Content>{spankCount}</Content>
      </Item>
    </Wrapper>
  );
};

Status.propTypes = {
  threadId: PropTypes.string.isRequired,
};

export default Status;
