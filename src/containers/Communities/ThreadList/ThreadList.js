import React from 'react';

import {
  FlexWrapper,
  FlexInnerWrapper,
  FlexContainer,
} from 'components/FlexWrapper';
import Overflow from 'components/Layout/Overflow';

import ThreadListHeader from './Header';
import List from './List';
import LoadMore from './LoadMore';
import CreateThreadButton from './CreateThreadButton';

const ThreadList = React.memo(() => (
  <FlexWrapper canOverflow>
    <FlexInnerWrapper fullHeight>
      <ThreadListHeader />

      <FlexContainer wide fullViewportMobile backgroundColor="#FAFAFA" style={{ height: 'calc(100% - 63px)' }}>
        <Overflow>
          <ul>
            <List />
            <LoadMore />
          </ul>
        </Overflow>
      </FlexContainer>

      <CreateThreadButton />
    </FlexInnerWrapper>
  </FlexWrapper>
));

ThreadList.propTypes = {
};

ThreadList.defaultProps = {
};

export default ThreadList;
