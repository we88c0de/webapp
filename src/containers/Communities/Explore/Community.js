import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as communitySelectors from 'state/communities/selectors';

import Card from 'components/Card';
import MembershipRequestButton from 'components/MembershipRequestButton';

const Community = ({ communityId }) => {
  const userIsLoggedIn = useSelector(authSelectors.loggedIn());
  const avatar = useSelector(communitySelectors.getAvatar(communityId));
  const name = useSelector(communitySelectors.getName(communityId));
  const slug = useSelector(communitySelectors.getSlugById(communityId));
  const description = useSelector(communitySelectors.getDescription(communityId));
  const privacy = useSelector(communitySelectors.getPrivacy(communityId));

  const action = userIsLoggedIn
    ? <MembershipRequestButton communityId={communityId} />
    : null;

  return (
    <Card
      avatar={{
        src: avatar,
        alt: name,
      }}
      link={`/+${slug}`}
      title={name}
      description={(
        <>
          <span>{description}</span>
        </>
      )}
      privacy={privacy}
      action={action}
    />
  );
};

Community.propTypes = {
  communityId: PropTypes.string.isRequired,
};

export default Community;
