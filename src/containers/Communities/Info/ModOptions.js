import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as communitySelectors from 'state/communities/selectors';
import * as membershipSelectors from 'state/memberships/selectors';

import { Cog } from 'components/Icons';
import Menu, { Item } from 'components/Menu';

import locales from './i18n';

const MoreOptionsWrapper = styled.div`
  height: 16px;
  width: 16px;
  padding: 8px;
  border: 0;
  display: flex;
  position: absolute;
  cursor: pointer;
  left: 4px;
  top: 4px;
  z-index: 20;
  background: rgba(0, 0, 0, ${props => (props.active ? '.3' : '.1')});
  border-radius: 100%;

  a {
    display: block;
  }
`;

const ModOptions = ({ communityId }) => {
  const { t } = useTranslation(locales);

  const slug = useSelector(communitySelectors.getSlugById(communityId));
  const isOwner = useSelector(membershipSelectors.userIsOwnerOf(communityId));
  const isAdmin = useSelector(membershipSelectors.userIsAdminOf(communityId));
  const isMod = useSelector(membershipSelectors.userIsModOf(communityId));

  const [showingModMenu, setShowingModMenu] = useState(false);
  const openModMenu = useCallback(() => { setShowingModMenu(true); }, []);
  const closeModMenu = useCallback(() => { setShowingModMenu(false); }, []);

  if (!isMod) return null;

  return (
    <MoreOptionsWrapper onClick={openModMenu} active={showingModMenu}>
      <Cog />
      <Menu width="250px" open={showingModMenu} onClose={closeModMenu} orientation="right" maxHeight="100%">
        {isOwner && (
          <>
            <Item>
              <Link to={`/+${slug}/edit`}>{t('Edit information')}</Link>
            </Item>
            <Item>
              <Link to={`/+${slug}/avatar`}>{t('Change avatar')}</Link>
            </Item>
            <Item>
              <Link to={`/+${slug}/cover`}>{t('Change cover')}</Link>
            </Item>
          </>
        )}
        {isAdmin && (
          <>
            <Item>
              <Link to={`/+${slug}/mods`}>{t('Manage mods')}</Link>
            </Item>
          </>
        )}
        <Item>
          <Link to={`/+${slug}/bans`}>{t('Manage bans')}</Link>
        </Item>
        <Item>
          <Link to={`/+${slug}/pending`}>{t('Pending memberships')}</Link>
        </Item>
        <Item>
          <Link to={`/+${slug}/invite`}>{t('Invite users')}</Link>
        </Item>
      </Menu>
    </MoreOptionsWrapper>
  );
};

ModOptions.propTypes = {
  communityId: PropTypes.string.isRequired,
};

ModOptions.defaultProps = {
};

export default ModOptions;
