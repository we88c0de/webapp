import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Unread from './Unread';
import DeletedHeader from './DeletedHeader';
import Options from './Options';
import AuthorInfo from './AuthorInfo';
import Body from './Body';
import Actions from '../Actions';

const ReplyWrapper = styled.div`
  border-bottom: 1px solid rgba(230, 220, 220, 0.34);
  padding-top: 24px;
  box-shadow: 0px -8px 20px rgba(230, 220, 220, 0.42);
  position: relative;

  @media(min-width: 767px) {
    padding: 24px;
  }
`;
ReplyWrapper.displayName = 'ReplyWrapper';

const Reply = ({ replyId }) => (
  <ReplyWrapper>
    <DeletedHeader replyId={replyId} />
    <Unread replyId={replyId} />
    <Options replyId={replyId} />
    <AuthorInfo replyId={replyId} />
    <Body replyId={replyId} />
    <Actions entityId={replyId} type="reply" />
  </ReplyWrapper>
);

Reply.propTypes = {
  replyId: PropTypes.string.isRequired,
};

Reply.defaultProps = {
};

export default Reply;
