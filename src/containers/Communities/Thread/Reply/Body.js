/* eslint-disable react/no-array-index-key */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactPlayer from 'react-player';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as replySelectors from 'state/replies/selectors';
import * as replyActions from 'state/replies/actions';

import ReadOnlyEditor from 'components/Editor/ReadOnlyEditor';
import ContentMedia from 'components/ContentMedia';
import ParsedContent from 'components/ParsedContent';

import ReplyLoading from './Loading';

const BodyWrapper = styled.div`
  margin: 16px;
  line-height: 20px;
  font-size: 16px;
  text-align: left;

  p {
    padding: 8px 0;
  }
`;
BodyWrapper.displayName = 'BodyWrapper';

const ReplyBody = ({ replyId }) => {
  const dispatch = useDispatch();

  const rawContent = useSelector(replySelectors.getRawContent(replyId));
  const hasOldContent = useSelector(replySelectors.hasOldContent(replyId));
  const content = useSelector(replySelectors.getContentForEditor(replyId), shallowEqual);
  const media = useSelector(replySelectors.getMedia(replyId), shallowEqual);
  const videos = useSelector(replySelectors.getVideoUrls(replyId), shallowEqual);

  useEffect(() => {
    if (typeof content === 'undefined' && hasOldContent) {
      dispatch(replyActions.hydrateContentForEditor(replyId));
    }
  }, [content, replyId, dispatch, hasOldContent]);

  const renderContent = () => {
    if (!hasOldContent) { // New stuff
      if (rawContent) return <ParsedContent content={rawContent} />;
      return null;
    }

    if (typeof content === 'undefined') return <ReplyLoading />;
    if (!content) return null;

    const BodyContent = typeof content === 'string'
      ? (
        <>
          {content.trim().split('\n').map((par, index) => (
            <p key={`reply-${replyId}-par-${index}`}>{par}</p>
          ))}
        </>
      ) : (
        <ReadOnlyEditor
          content={content}
        />
      );

    return BodyContent;
  };


  return (
    <BodyWrapper>
      {renderContent()}
      <ContentMedia media={media || []} />
      {(videos || []).map(url => (
        <ReactPlayer key={url} url={url} width="auto" />
      ))}
    </BodyWrapper>
  );
};

ReplyBody.propTypes = {
  replyId: PropTypes.string.isRequired,
};

ReplyBody.defaultProps = {
};

export default ReplyBody;
