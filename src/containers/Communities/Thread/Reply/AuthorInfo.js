import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import * as replySelectors from 'state/replies/selectors';
import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';
import { Author, Description, More } from 'components/CommunityAuthor';

const AuthorInfo = ({ replyId }) => {
  const authorId = useSelector(replySelectors.getAuthorId(replyId));
  const authorUsername = useSelector(userSelectors.getUsername(authorId));
  const createdAt = useSelector(replySelectors.getCreatedAt(replyId));

  return (
    <Link to={`/@${authorUsername}`} className="userlink">
      <Author>
        <UserAvatar userId={authorId} size="32px" />
        <Description>
          <UserDisplayName userId={authorId} />
          <More>{moment(createdAt).fromNow()}</More>
        </Description>
      </Author>
    </Link>
  );
};

AuthorInfo.propTypes = {
  replyId: PropTypes.string.isRequired,
};

AuthorInfo.defaultProps = {
};

export default AuthorInfo;
