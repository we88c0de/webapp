import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import * as threadSelectors from 'state/threads/selectors';
import * as threadActions from 'state/threads/actions';

import {
  FlexWrapper,
  FlexInnerWrapper,
  FlexContainer,
} from 'components/FlexWrapper';
import Overflow from 'components/Layout/Overflow';
import Ad from 'components/Ad';

import Header from './Header';
import ThreadAuthorInfo from './ThreadAuthorInfo';
import Body from './Body';
import Actions from './Actions';
import ReplyList from './ReplyList';
import ReplyComposer from './ReplyComposer';

const ThreadContentWrapper = styled.div`
  @media(min-width: 767px) {
    padding: 16px 24px;
  }
`;
ThreadContentWrapper.displayName = 'ThreadContentWrapper';

const Content = React.memo(() => {
  const dispatch = useDispatch();
  const params = useParams();

  const threadId = useSelector(threadSelectors.getIdBySlug(params.threadSlug));

  useEffect(() => {
    dispatch(threadActions.markThreadAsRead(threadId));
  }, [dispatch, threadId]);

  return (
    <FlexWrapper canOverflow>
      <FlexInnerWrapper fullHeight>
        <Header />

        <Overflow>
          <FlexContainer padding="0">
            <ThreadContentWrapper>
              <ThreadAuthorInfo threadId={threadId} />
              <Body threadId={threadId} />
              <Actions entityId={threadId} type="thread" />
            </ThreadContentWrapper>

            <Ad id="Thread Body" divider="top" />

            <ReplyList threadId={threadId} />
            <ReplyComposer threadId={threadId} />

            <Ad id="ReplyComposer Bottom" />
          </FlexContainer>
        </Overflow>
      </FlexInnerWrapper>
    </FlexWrapper>
  );
});

Content.propTypes = {
};

Content.defaultProps = {
};

export default Content;
