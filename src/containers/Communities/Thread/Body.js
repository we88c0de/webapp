/* eslint-disable react/no-array-index-key */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactPlayer from 'react-player';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as threadSelectors from 'state/threads/selectors';
import * as threadActions from 'state/threads/actions';

import ReadOnlyEditor from 'components/Editor/ReadOnlyEditor';
import ContentMedia from 'components/ContentMedia';
import ParsedContent from 'components/ParsedContent';

import ReplyLoading from './Reply/Loading';

const BodyWrapper = styled.div`
  padding: 16px;
  line-height: 24px;
  font-size: 18px;
  text-align: left;

  h1 {
    text-align: left;
  }

  p {
    padding: 8px 0;
  }

  ul, ol {
    display: block;
    list-style-type: decimal;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-inline-start: 40px;
  }

  ul {
    list-style-type: disc;

    &.content-media {
      list-style-type: none;
      padding: 0;
    }
  }

  li p {
    padding: 0;
  }

  h1, h2, h3 {
    .emoji-mart-emoji {
      height: 25px;
    }
  }
`;
BodyWrapper.displayName = 'BodyWrapper';

const Body = ({ threadId }) => {
  const dispatch = useDispatch();

  const rawContent = useSelector(threadSelectors.getRawContent(threadId));
  const hasOldContent = useSelector(threadSelectors.hasOldContent(threadId));
  const content = useSelector(threadSelectors.getContentForEditor(threadId), shallowEqual);
  const media = useSelector(threadSelectors.getMedia(threadId), shallowEqual);
  const videos = useSelector(threadSelectors.getVideoUrls(threadId), shallowEqual);

  useEffect(() => {
    if (typeof content === 'undefined' && hasOldContent) {
      dispatch(threadActions.hydrateContentForEditor(threadId));
    }
  }, [content, threadId, dispatch, hasOldContent]);

  const renderContent = () => {
    if (!hasOldContent && rawContent) return <ParsedContent content={rawContent} />; // New stuff

    if (typeof content === 'undefined') return <ReplyLoading />;
    if (!content) return null;

    const BodyContent = typeof content === 'string'
      ? (
        <>
          {content.trim().split('\n').map((par, index) => (
            <p key={`thread-${threadId}-par-${index}`}>{par}</p>
          ))}
        </>
      ) : (
        <ReadOnlyEditor
          key={`thread-body-${threadId}`}
          content={content}
        />
      );

    return BodyContent;
  };

  return (
    <BodyWrapper>
      {renderContent()}
      <ContentMedia media={media} />
      {videos.map(url => (
        <ReactPlayer key={url} url={url} width="auto" />
      ))}
    </BodyWrapper>
  );
};

Body.propTypes = {
  threadId: PropTypes.string.isRequired,
};

Body.defaultProps = {
};

export default Body;
