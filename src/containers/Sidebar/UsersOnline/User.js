import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

import UserLink from 'components/UserLink';
import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';

const Container = styled.div`
  display: flex;
  font-size: 12px;
  box-sizing: border-box;
  padding-right: 8px;
  overflow: hidden;
  width: 125px;

  .avatar {
    margin-right: 8px;
  }

  .data {
    overflow: hidden;

    .displayname {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
  }
`;
Container.displayName = 'Container';

const Role = styled.div`
  color: ${props => props.theme.colors.secondary};
`;
Role.displayName = 'Role';

const User = ({ userId, style }) => {
  const role = useSelector(userSelectors.getRole(userId));

  if (!userId) return null;

  return (
    <UserLink userId={userId}>
      <Container style={style}>
        <UserAvatar userId={userId} showOnline={false} size="32px" />
        <div className="data">
          <UserDisplayName userId={userId} />
          <Role>{role}</Role>
        </div>
      </Container>
    </UserLink>
  );
};

User.propTypes = {
  userId: PropTypes.number.isRequired,
  style: PropTypes.shape({}),
};

User.defaultProps = {
  style: {},
};

export default User;
