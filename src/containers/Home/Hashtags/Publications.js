import React, {
  useState, useRef, useCallback, useEffect,
} from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedActions from 'state/feed/actions';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import Publication from 'components/Publication';
import EmptyState from 'components/EmptyState';
import Ad from 'components/Ad';

import { PUBLICATIONS_LIST_LIMIT } from '../../../constants';
import locales from '../i18n';

const HashtagFeed = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const params = useParams();

  const [publicationIds, setPublicationIds] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);
  const publicationsCount = useRef(0);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      isLoading.current = true;
      const ids = await dispatch(
        feedActions.loadHashtag(params.hashtag, publicationsCount.current),
      );
      setPublicationIds(prevIds => [...prevIds, ...ids]);
      isLoading.current = false;
      publicationsCount.current += ids.length;

      if (ids.length < PUBLICATIONS_LIST_LIMIT) {
        setFullyLoaded(true);
      }
    }
  }, [dispatch, params.hashtag]);

  useEffect(() => {
    publicationsCount.current = 0;
    setPublicationIds([]);
    setFullyLoaded(false);
  }, [params.hashtag]);

  if (fullyLoaded && !publicationIds.length) {
    return <EmptyState subtitle={t('No publications found')} />;
  }

  return (
    <>
      {publicationIds.map((id, index) => (
        <React.Fragment key={`pub-${id}`}>
          <Publication publicationId={id} />
          <Ad id="Feed hashtag" data={{ index }} />
        </React.Fragment>
      ))}

      {!fullyLoaded && (
        <VisibilitySensor onChange={load} partialVisibility>
          <SpinnerWrapper>
            <Spinner color="#999" />
          </SpinnerWrapper>
        </VisibilitySensor>
      )}
    </>
  );
};

HashtagFeed.propTypes = {
};

HashtagFeed.defaultProps = {
};

export default HashtagFeed;
