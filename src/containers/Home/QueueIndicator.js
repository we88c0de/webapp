import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import Button from 'components/Button';
import locales from './i18n';

const QueueButton = styled(Button).attrs({
  color: 'white',
  fontColor: 'black',
})`
  font-weight: 500;
  text-transform: none;
  position: fixed;
  margin-left: 250px;
  z-index: 100;
  width: 240px;
  margin-top: 8px;
  text-align: center;

  @media(max-width: 768px) {
    margin-left: calc((100vw / 2) - (240px / 2));
  }

  div {
    margin: 0 auto;
  }
`;
QueueButton.displayName = 'QueueButton';

const QueueIndicator = ({ onFlush }) => {
  const { t } = useTranslation(locales);

  const queueCount = useSelector(feedSelectors.getFeedQueueCount());

  if (!queueCount) return null;

  return (
    <QueueButton onClick={onFlush}>
      {t('{{count}} new publications', { count: queueCount, context: (queueCount === 1 ? 'SINGULAR' : 'PLURAL') })}
    </QueueButton>
  );
};

QueueIndicator.propTypes = {
  onFlush: PropTypes.func.isRequired,
};

QueueIndicator.defaultProps = {
};

export default QueueIndicator;
