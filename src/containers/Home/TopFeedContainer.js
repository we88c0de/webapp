import styled from 'styled-components';

const TopFeedContainer = styled.div`
  margin: 16px 16px 0 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
TopFeedContainer.displayName = 'TopFeedContainer';

export default TopFeedContainer;
