import React from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';

import { useSelector } from 'react-redux';
import * as feedSelectors from 'state/feed/selectors';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';

const ListBottom = ({ load }) => {
  const fullyLoaded = useSelector(feedSelectors.isFeedFullyLoaded());

  if (fullyLoaded) return null;

  return (
    <VisibilitySensor onChange={load} partialVisibility offset={{ bottom: -1000 }}>
      <SpinnerWrapper>
        <Spinner color="#999" />
      </SpinnerWrapper>
    </VisibilitySensor>
  );
};

ListBottom.propTypes = {
  load: PropTypes.func.isRequired,
};

ListBottom.defaultProps = {
};

export default ListBottom;
