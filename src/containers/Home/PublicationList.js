import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';

import Publication from 'components/Publication';
import Ad from 'components/Ad';

const getSelector = (type) => {
  if (type === 'trending') return feedSelectors.getFeed;
  return feedSelectors.getFeed;
};

const PublicationsList = ({ type }) => {
  const selector = getSelector(type);
  const publicationIds = useSelector(selector(), shallowEqual);

  return (
    <>
      {publicationIds.map((id, index) => (
        <React.Fragment key={`pub-${id}`}>
          <Publication key={`publication-${id}`} publicationId={id} />
          <Ad id={`Feed ${type}`} data={{ index }} />
        </React.Fragment>
      ))}
    </>
  );
};

PublicationsList.propTypes = {
  type: PropTypes.oneOf(['main', 'trending', 'media']).isRequired,
};

PublicationsList.defaultProps = {
};

export default PublicationsList;
