import React from 'react';
import styled from 'styled-components';
import { Trans } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import Warning from 'components/Warning';

import locales from './i18n';
import { EMAIL } from '../../constants';

const WarningBoxesWrapper = styled.div`
  margin: 16px;
`;
WarningBoxesWrapper.displayName = 'WarningBoxesWrapper';

const WarningBoxes = () => {
  const { t } = useTranslation(locales);

  const isEmailConfirmed = useSelector(authSelectors.isEmailConfirmed());
  const email = useSelector(authSelectors.getEmail());

  if (isEmailConfirmed) return null;

  return (
    <WarningBoxesWrapper>
      <Warning>
        <Trans t={t} i18nKey="warning.emailconfirm" ns="home" values={{ email, ouremail: EMAIL }}>
          <b>Your account is not confirmed!</b>
          <p>
            {'You won\'t '}
            be able to upload your avatar, personalize your profile or post some publications
            {' '}
            in the site until your e-mail address has been confirmed.
          </p>
          <p>
            {'We\'ve '}
            sent you the instructions to
            {' '}
            <b>your address</b>
            . If you
            {' didn\'t '}
            receive our e-mail, please write us to
            {' '}
            <b>our address</b>
            .
          </p>
        </Trans>
      </Warning>
    </WarningBoxesWrapper>
  );
};

WarningBoxes.propTypes = {
};

WarningBoxes.defaultProps = {
};

export default WarningBoxes;
