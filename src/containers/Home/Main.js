import React, {
  useEffect, useCallback, useRef, useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';
import * as feedActions from 'state/feed/actions';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import QueueIndicator from './QueueIndicator';
import PublicationList from './PublicationList';
import ListBottom from './ListBottom';
import locales from './i18n';

const MainFeed = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const isEmpty = useSelector(feedSelectors.isFeedEmpty());
  const [hasError, setHasError] = useState(false);
  const isLoading = useRef(false);

  useEffect(() => {
    dispatch(feedActions.flushQueue());
  }, [dispatch]);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      try {
        isLoading.current = true;
        await dispatch(feedActions.load());
        isLoading.current = false;
      } catch (error) {
        setHasError(true);
      }
    }
  }, [dispatch]);

  const retry = useCallback(() => {
    isLoading.current = false;
    setHasError(false);
    load();
  }, [load]);

  const onFlushQueue = useCallback(() => {
    document.body.parentElement.scrollTo(0, 0);
    dispatch(feedActions.flushQueue());
  }, [dispatch]);

  if (isEmpty) {
    return (
      <EmptyState
        title={t('Your feed is empty')}
        subtitle={t('We suggest you start following people you find interesting for a more personalized experience. You can explore what\'s hot right now in the trending section.')}
      >
        <Button to="/trending">{t('Go to Trending')}</Button>
      </EmptyState>
    );
  }

  if (hasError) {
    return (
      <EmptyState
        title={t('Oops!')}
        subtitle={t('There was a problem loading your feed. Our team has been alerted and is working on it right now. Please try again later.')}
      >
        <Button onClick={retry}>{t('Retry')}</Button>
      </EmptyState>
    );
  }

  return (
    <>
      <QueueIndicator onFlush={onFlushQueue} />
      <PublicationList type="main" />
      <ListBottom load={load} />
    </>
  );
};

MainFeed.propTypes = {
};

MainFeed.defaultProps = {
};

export default MainFeed;
