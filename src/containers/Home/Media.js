import React, { useState, useRef, useCallback } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import { useDispatch } from 'react-redux';

import * as feedActions from 'state/feed/actions';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import Publication from 'components/Publication';
import Ad from 'components/Ad';

const MediaFeed = () => {
  const dispatch = useDispatch();

  const [publicationIds, setPublicationIds] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);
  const publicationsCount = useRef(0);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      isLoading.current = true;
      const ids = await dispatch(feedActions.loadOnlyMedia(publicationsCount.current));
      setPublicationIds(prevIds => [...prevIds, ...ids]);
      isLoading.current = false;
      publicationsCount.current += ids.length;

      if (!ids.length) {
        setFullyLoaded(true);
      }
    }
  }, [dispatch]);

  return (
    <>
      {publicationIds.map((id, index) => (
        <React.Fragment key={`pub-${id}`}>
          <Publication publicationId={id} />
          <Ad id="Feed media" data={{ index }} />
        </React.Fragment>
      ))}

      {!fullyLoaded && (
        <VisibilitySensor onChange={load} partialVisibility offset={{ bottom: -1000 }}>
          <SpinnerWrapper>
            <Spinner color="#999" />
          </SpinnerWrapper>
        </VisibilitySensor>
      )}
    </>
  );
};

MediaFeed.propTypes = {
};

MediaFeed.defaultProps = {
};

export default MediaFeed;
