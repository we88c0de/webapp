/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import ismobile from 'ismobilejs';

import * as appSelectors from 'state/app/selectors';
import * as channelSelectors from 'state/channels/selectors';

import Messages from '../Content/Messages';
import ComposeMessage from '../Content/ComposeMessage';
import EditMessage from '../Content/ComposeMessage/EditMessage';
import Participants from './Participants';

const ContentWrapper = styled.div`
  flex: 1;
  display: flex;
  height: 100%;
  overflow: auto;
`;

const ChatArea = ({ id }) => {
  const showing = useSelector(appSelectors.isShowingParticipants());
  const isEditing = useSelector(channelSelectors.isEditing(id));

  return (
    <>
      <ContentWrapper>
        <Messages key={`chat-channelmessages-${id}`} type="channel" id={id} />
        {(showing || (!ismobile.phone && !ismobile.tablet)) && (
          <Participants key={`chat-participants-${id}`} id={id} />
        )}
      </ContentWrapper>

      {isEditing
        ? <EditMessage key={`compose-channel-edit-${id}`} type="channel" id={id} />
        : <ComposeMessage key={`compose-channel-${id}`} type="channel" id={id} />
      }
    </>
  );
};

ChatArea.propTypes = {
  id: PropTypes.string.isRequired,
};

export default ChatArea;
