import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';

import { Account } from 'components/Icons';
import InfoItem from 'components/InfoItem';

import locales from '../i18n';

const Members = ({ channelId }) => {
  const { t } = useTranslation(locales);
  const membersCount = useSelector(channelSelectors.membersCount(channelId));

  return (
    <InfoItem title={t('Members')}>
      <Account color={colors.grey} />
      {membersCount}
    </InfoItem>
  );
};

Members.propTypes = {
  channelId: PropTypes.string.isRequired,
};

export default Members;
