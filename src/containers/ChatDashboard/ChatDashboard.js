import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useHistory, useLocation, useParams } from 'react-router-dom';

import { useTitle, useTranslation, useHotKeys } from 'hooks';
import isMobile from 'utils/isMobile';
import * as appSelectors from 'state/app/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as channelsSelectors from 'state/channels/selectors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as appActions from 'state/app/actions';

import Loading from 'components/Loading';
import ErrorState from 'components/ErrorState';
import Layout from 'components/Layout';

import List from './List';
import Content from './DashboardContent';
import QuickSearchModal from './QuickSearchModal';
import locales from './i18n';

const ChatDashboard = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const location = useLocation();
  const { t } = useTranslation(locales);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn());
  const isChannelsInitialized = useSelector(channelsSelectors.isInitialized());
  const isMessengerInitialized = useSelector(messengerSelectors.isInitialized());
  const messengerHasFailed = useSelector(messengerSelectors.hasFailed());
  const locationHistory = useSelector(appSelectors.urlHistory(/\/chat\/(messengers|channels)\/(.*)/g), shallowEqual);

  const [showingQuickSearchModal, setShowingQuickSearchModal] = useState(false);

  useEffect(() => {
    if (!params.chatId || !userIsLoggedIn) {
      dispatch(appActions.uiLeftColumn(true));
    } else {
      dispatch(appActions.uiLeftColumn(false));
    }
  }, [params.chatId, dispatch, userIsLoggedIn]);

  const showQuickSearchModal = () => setShowingQuickSearchModal(true);
  const hideQuickSearchModal = () => setShowingQuickSearchModal(false);

  useHotKeys([{ key: 'k', metaKey: true }], showQuickSearchModal);

  useTitle(t('global:Chat'));

  if (locationHistory.length > 0 && !params.type && !location.search.includes('noredirect') && !isMobile) {
    history.replace(locationHistory[0]);
  }

  if (userIsLoggedIn && (!isMessengerInitialized || !isChannelsInitialized)) return <Loading />;
  if (messengerHasFailed) return <ErrorState message={t('error.fetch')} />;

  return (
    <>
      <Layout columns={userIsLoggedIn ? 2 : 1}>
        {userIsLoggedIn && <List />}
        <Content />
      </Layout>
      {showingQuickSearchModal && <QuickSearchModal close={hideQuickSearchModal} />}
    </>
  );
};

ChatDashboard.propTypes = {
};

export default ChatDashboard;
