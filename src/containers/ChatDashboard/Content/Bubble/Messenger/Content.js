import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import { mediaEqual, mediaGifEqual } from 'state/app/equalityFunctions';

import ContentMedia from 'components/ContentMedia';

import Message from './Message';
import InReplyTo from './InReplyTo';
import Audio from './Audio';
import MessageWrapper from '../Message';
import Gif from '../../Gif';

const Content = ({ messengerId, messageId }) => {
  const isEmojiOnly = useSelector(messengerSelectors.isMessageEmojiOnly(messageId));
  const gif = useSelector(messengerSelectors.getMessageGif(messageId), mediaGifEqual);
  const media = useSelector(messengerSelectors.getMessageMedia(messageId), mediaEqual);
  const inReplyTo = useSelector(messengerSelectors.getReplyingTo(messageId));
  const hasAudio = useSelector(messengerSelectors.messageHasAudio(messageId));

  return (
    <MessageWrapper big={isEmojiOnly} hasAudio={hasAudio}>
      {inReplyTo && <InReplyTo messengerId={messengerId} messageId={inReplyTo} />}
      {gif
        ? <Gif data={gif} />
        : (
          <>
            <ContentMedia media={media} />
            {hasAudio && <Audio messageId={messageId} />}
            <Message messageId={messageId} />
          </>
        )
      }
    </MessageWrapper>
  );
};

Content.propTypes = {
  messengerId: PropTypes.string.isRequired,
  messageId: PropTypes.string.isRequired,
};

Content.defaultProps = {
};

export default Content;
