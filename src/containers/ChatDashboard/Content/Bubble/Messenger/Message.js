import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import * as messengerActions from 'state/messengers/actions';

import ReadOnlyEditor from 'components/Editor/ReadOnlyEditor';
import ParsedContent from 'components/ParsedContent';

import LoadingMessage from '../LoadingMessage';

const Message = ({ messageId }) => {
  const dispatch = useDispatch();

  const content = useSelector(
    messengerSelectors.getMessageContentForEditor(messageId),
    shallowEqual,
  );

  useEffect(() => {
    if (typeof content === 'undefined') {
      dispatch(messengerActions.hydrateMessageForEditor(messageId));
    }
  }, [content, messageId, dispatch]);

  if (typeof content === 'undefined') return <LoadingMessage />;
  if (typeof content === 'string') {
    // NEW STUFF
    return <ParsedContent content={content} disallowed={['heading']} emojiOnlySize={40} />;
  }

  if (!content) return null;

  // OLD SUFF - For backward compatibility's sake
  return (
    <ReadOnlyEditor
      content={content}
    />
  );
};

Message.propTypes = {
  messageId: PropTypes.string.isRequired,
};

Message.defaultProps = {
};

export default Message;
