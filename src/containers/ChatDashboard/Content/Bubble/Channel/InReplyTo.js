import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import * as channelActions from 'state/channels/actions';
import { mediaGifEqual } from 'state/app/equalityFunctions';

import Busy from 'components/UserDisplayName/Busy';
import ParsedContent from 'components/ParsedContent';
import ContentMedia from 'components/ContentMedia';

import ReplyingTo from '../../ReplyingTo';
import DeletedReply from '../../DeletedReply';
import Gif from '../../Gif';

const InReplyTo = ({ channelId, messageId }) => {
  const dispatch = useDispatch();

  const [deleted, setDeleted] = useState(false);
  const authorId = useSelector(channelSelectors.getMessageAuthorId(messageId));
  const authorType = useSelector(channelSelectors.getMessageAuthorType(messageId));
  const content = useSelector(channelSelectors.getMessageContent(messageId));
  const gif = useSelector(channelSelectors.getMessageGif(messageId), mediaGifEqual);
  const media = useSelector(channelSelectors.getMessageMedia(messageId), shallowEqual);

  const busy = !deleted && !content && !media && !gif;

  useEffect(() => {
    const fetch = async () => {
      try {
        await dispatch(channelActions.fetchSingleMessage(channelId, messageId));
      } catch (error) {
        setDeleted(true);
      }
    };

    if (busy) {
      fetch();
    }
  }, [busy, channelId, messageId, dispatch]);

  const renderContent = useCallback(() => {
    if (busy) return <Busy />;
    if (deleted) return <DeletedReply />;

    return (
      <>
        {gif && <Gif data={gif} />}
        <ContentMedia media={media} />
        {content && <ParsedContent content={content} />}
      </>
    );
  }, [busy, content, deleted, gif, media]);

  return (
    <ReplyingTo fullWidth authorType={authorType} authorId={authorId} loading={busy}>
      {renderContent()}
    </ReplyingTo>
  );
};

InReplyTo.propTypes = {
  channelId: PropTypes.string.isRequired,
  messageId: PropTypes.string.isRequired,
};

InReplyTo.defaultProps = {
};

export default React.memo(InReplyTo);
