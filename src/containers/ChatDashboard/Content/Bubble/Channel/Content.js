import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import { mediaEqual, mediaGifEqual } from 'state/app/equalityFunctions';

import ContentMedia from 'components/ContentMedia';

import Message from './Message';
import InReplyTo from './InReplyTo';
import MessageWrapper from '../Message';
import Gif from '../../Gif';

const Content = ({ channelId, messageId }) => {
  const isEmojiOnly = useSelector(channelSelectors.isMessageEmojiOnly(messageId));
  const gif = useSelector(channelSelectors.getMessageGif(messageId), mediaGifEqual);
  const media = useSelector(channelSelectors.getMessageMedia(messageId), mediaEqual);
  const inReplyTo = useSelector(channelSelectors.getReplyingTo(messageId));

  return (
    <MessageWrapper big={isEmojiOnly}>
      {inReplyTo && <InReplyTo messageId={inReplyTo} channelId={channelId} />}
      {gif
        ? <Gif data={gif} />
        : (
          <>
            <ContentMedia media={media} />
            <Message messageId={messageId} />
          </>
        )
      }
    </MessageWrapper>
  );
};

Content.propTypes = {
  channelId: PropTypes.string.isRequired,
  messageId: PropTypes.string.isRequired,
};

Content.defaultProps = {
};

export default Content;
