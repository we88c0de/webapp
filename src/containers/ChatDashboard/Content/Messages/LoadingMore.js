import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import VisibilitySensor from 'react-visibility-sensor';

import Spinner from 'components/Spinner';

const Container = styled.div`
  text-align: center;
  margin: 20px 0;
`;

const LoadingMore = React.memo(({ onChange }) => (
  <Container>
    <VisibilitySensor onChange={onChange} partialVisibility>
      <Spinner color="#999" />
    </VisibilitySensor>
  </Container>
));

LoadingMore.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default LoadingMore;
