import React, { useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as channelsActions from 'state/channels/actions';
import * as channelsSelectors from 'state/channels/selectors';
import * as channelEqualityFunctions from 'state/channels/equalityFunctions';
import * as authSelectors from 'state/auth/selectors';

import { useScrollAtBottomSensor } from 'hooks';

import { CHANNEL_MESSAGES_TYPES } from '../../../../constants';
import ChannelActionMessage from '../ChannelActionMessage';
import ChannelMessage from '../Bubble/Channel';
import Wrapper from './Wrapper';
import Container from './Container';
import LoadingMore from './LoadingMore';
import Day from '../Day';

const ChannelMessages = ({ id }) => {
  const dispatch = useDispatch();

  const isLoadingMessages = useRef(false);
  const prevHeight = useRef(null);
  const messages = useSelector(
    channelsSelectors.getMessagesByChannelId(id),
    channelEqualityFunctions.messages,
  );
  const me = useSelector(authSelectors.getMe(), shallowEqual);
  const isFullyLoaded = useSelector(channelsSelectors.isFullyLoaded(id));
  const unreadCount = useSelector(channelsSelectors.getUnreadById(id));

  const [container, isAtBottom] = useScrollAtBottomSensor([messages]);

  useEffect(() => {
    dispatch(channelsActions.setActive(id));
    return () => dispatch(channelsActions.unmount(id));
  }, [dispatch, id]);

  const visibilityHandler = useCallback(() => {
    if (document.visibilityState === 'visible') dispatch(channelsActions.setActive(id));
    else dispatch(channelsActions.setActive(null));
  }, [dispatch, id]);

  useEffect(() => {
    visibilityHandler();

    document.addEventListener('visibilitychange', visibilityHandler);

    return () => {
      document.removeEventListener('visibilitychange', visibilityHandler);
      dispatch(channelsActions.setActive(null));
    };
  }, [unreadCount, dispatch, visibilityHandler]);

  useEffect(() => {
    dispatch(channelsActions.scrollAtBottomChange(id, isAtBottom));
  }, [isAtBottom, dispatch, id]);

  const loadMoreVisibilityChanged = useCallback(async (isVisible) => {
    if (isVisible && !isLoadingMessages.current) {
      isLoadingMessages.current = true;
      prevHeight.current = container.current ? container.current.scrollHeight : null;
      await dispatch(channelsActions.loadMoreMessages(id));
      isLoadingMessages.current = false;
    }
  }, [dispatch, id, container]);

  useEffect(() => {
    if (prevHeight.current) {
      // Restore scroll position after loading previous messages
      const nextHeight = container.current.scrollHeight;
      container.current.scrollTo(0, nextHeight - prevHeight.current);
      prevHeight.current = null;
    }
  }, [messages, container]);

  return (
    <Wrapper>
      <Container ref={container}>
        {!isFullyLoaded && messages.length > 0 && (
          <LoadingMore onChange={loadMoreVisibilityChanged} />
        )}
        {messages.map((message, index) => {
          if (!message) return <div />;

          const key = message.id || message.referenceId;
          if (message.type !== CHANNEL_MESSAGES_TYPES.MESSAGE) {
            return (
              <ChannelActionMessage key={`channel-actionmessage-${key}`} message={message} />
            );
          }

          const hideAuthor = (
            index > 0
            && messages[index - 1]
            && (
              messages[index - 1].authorId === message.authorId
              || (messages[index - 1].authorId === me.id && !message.authorId)
            )
            && messages[index - 1].type === CHANNEL_MESSAGES_TYPES.MESSAGE
            && moment(message.createdAt).diff(messages[index - 1].createdAt, 'seconds') < 60
          );
          const showDay = (
            index === 0 || !moment(messages[index - 1].createdAt).isSame(message.createdAt, 'day')
          );

          return (
            <React.Fragment key={`messenger-${key}`}>
              {showDay && <Day date={message.createdAt} />}
              <ChannelMessage
                key={`channel-message-${key}`}
                channelId={id}
                messageId={key}
                showAvatar={!hideAuthor}
              />
            </React.Fragment>
          );
        })}
      </Container>
    </Wrapper>
  );
};

ChannelMessages.propTypes = {
  id: PropTypes.string.isRequired,
};

export default ChannelMessages;
