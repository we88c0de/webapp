import React, { useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import * as messengerActions from 'state/messengers/actions';
import * as authSelectors from 'state/auth/selectors';

import { useScrollAtBottomSensor } from 'hooks';

import Wrapper from './Wrapper';
import Container from './Container';
import LoadingMore from './LoadingMore';
import DirectMessage from '../Bubble/Messenger';
import Day from '../Day';

const MessengerMessages = ({ id }) => {
  const dispatch = useDispatch();

  const isLoadingMessages = useRef(false);
  const prevHeight = useRef(null);

  const messages = useSelector(messengerSelectors.getMessagesByMessengerId(id), shallowEqual);
  const me = useSelector(authSelectors.getMe(), shallowEqual);
  const isFullyLoaded = useSelector(messengerSelectors.isFullyLoaded(id));
  const entity = useSelector(messengerSelectors.getById(id), shallowEqual);

  const [container, isAtBottom] = useScrollAtBottomSensor([messages]);

  useEffect(() => {
    dispatch(messengerActions.setActive(id));
    return () => dispatch(messengerActions.unmount(id));
  }, [dispatch, id]);

  const visibilityHandler = useCallback(() => {
    if (document.visibilityState === 'visible') dispatch(messengerActions.setActive(id));
    else dispatch(messengerActions.setActive(null));
  }, [dispatch, id]);

  useEffect(() => {
    visibilityHandler();

    document.addEventListener('visibilitychange', visibilityHandler);

    return () => {
      document.removeEventListener('visibilitychange', visibilityHandler);
      dispatch(messengerActions.setActive(null));
    };
  }, [entity.unreadCount, dispatch, visibilityHandler]);

  useEffect(() => {
    dispatch(messengerActions.scrollAtBottomChange(id, isAtBottom));
  }, [isAtBottom, dispatch, id]);

  const loadMoreVisibilityChanged = useCallback(async (isVisible) => {
    if (isVisible && !isLoadingMessages.current) {
      isLoadingMessages.current = true;
      prevHeight.current = container.current ? container.current.scrollHeight : null;
      await dispatch(messengerActions.loadMoreMessages(id));
      isLoadingMessages.current = false;
    }
  }, [dispatch, id, container]);

  useEffect(() => {
    if (prevHeight.current) {
      // Restore scroll position after loading previous messages
      const nextHeight = container.current.scrollHeight;
      container.current.scrollTo(0, nextHeight - prevHeight.current);
      prevHeight.current = null;
    }
  }, [messages, container]);

  return (
    <Wrapper>
      <Container ref={container}>
        {!isFullyLoaded && messages.length > 0 && (
          <LoadingMore onChange={loadMoreVisibilityChanged} />
        )}
        {messages.map((message, index) => {
          if (!message) return null;
          const key = message.id || message.referenceId;
          const hideAuthor = (
            index > 0
            && (
              messages[index - 1].authorId === message.authorId
              || (messages[index - 1].authorId === me.id && !message.authorId)
            )
            && moment(message.createdAt).diff(messages[index - 1].createdAt, 'seconds') < 60
          );
          const showDay = (
            index === 0 || !moment(messages[index - 1].createdAt).isSame(message.createdAt, 'day')
          );

          return (
            <React.Fragment key={`messenger-${key}`}>
              {showDay && <Day date={message.createdAt} />}
              <DirectMessage
                key={`messenger-dm-${key}`}
                messageId={key}
                messengerId={id}
                showAvatar={!hideAuthor}
              />
            </React.Fragment>
          );
        })}
      </Container>
    </Wrapper>
  );
};

MessengerMessages.propTypes = {
  id: PropTypes.string.isRequired,
};

export default MessengerMessages;
