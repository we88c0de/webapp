import React, {
  useState, useEffect, useRef, useCallback,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import VisibilitySensor from 'react-visibility-sensor';

import Api from 'state/api';
import { useTranslation, useTitle, useInputValue } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import InputSearch from 'components/Forms/InputSearch';
import PageTitle from 'components/PageTitle';
import Header from 'components/Header';
import {
  Container,
  InnerContainer,
  CreateText,
  CreateWrapper,
  Grid,
  SearchWrapper,
  TitleWrapper,
  Loader,
  LoadError,
} from 'components/Explore';

import List from './List';
import locales from '../i18n';

const LIMIT = 30;

const Explore = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('Explore channels'));

  const userIsLoggedIn = useSelector(authSelectors.loggedIn());

  const [channels, setChannels] = useState([]);
  const [loadError, setLoadError] = useState(null);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const filter = useInputValue('');
  const isLoading = useRef(false);
  const channelsCount = useRef(0);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      try {
        isLoading.current = true;

        const { data } = await Api.req.get('/chat/channels', {
          params: {
            explore: true,
            limit: LIMIT,
            skip: channelsCount.current,
          },
        });

        setChannels(currentChannels => [...currentChannels, ...data]);

        isLoading.current = false;
        channelsCount.current += data.length;

        if (data.length < LIMIT) {
          setFullyLoaded(true);
        }
      } catch (error) {
        setLoadError(error);
      }
    }
  }, []);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(false));
  }, [dispatch]);

  const filteredChannels = channels
    .filter(channel => channel.name.toLowerCase().includes(filter.value.toLowerCase()));

  const createLink = userIsLoggedIn ? '/chat/channels/create' : '/signup';

  return (
    <Container>
      <Header title={t('Find a channel for chat')} mobileOnly />
      <InnerContainer>
        <TitleWrapper>
          <PageTitle mobileOnly>{t('Find a channel for chat')}</PageTitle>
        </TitleWrapper>
        <SearchWrapper>
          <InputSearch placeholder={t('Search for a channel')} {...filter} />
        </SearchWrapper>
        <CreateWrapper>
          <CreateText>{t('Didn\'t find your kink? Create it!')}</CreateText>
          <Button to={createLink} className="small">{t('Create a channel')}</Button>
        </CreateWrapper>
        {loadError
          ? <LoadError message={loadError} />
          : (
            <Grid>
              <List data={filteredChannels} />

              {!fullyLoaded && (
                <VisibilitySensor onChange={load} partialVisibility>
                  <Loader />
                </VisibilitySensor>
              )}
            </Grid>
          )
        }
      </InnerContainer>
    </Container>
  );
};

export default Explore;
