import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import colors from 'utils/css/colors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as authSelectors from 'state/auth/selectors';

import ChatStateIcon from 'components/ChatStateIcon';
import { Gif, ImageMultiple } from 'components/Icons';

const LastMessageWrapper = styled.div`
  white-space: nowrap;
  overflow: hidden;
  width: 100%;
  font-weight: normal;
  color: ${colors.grey};
  margin-top: 4px;

  svg {
    width: 16px;
    margin: -2px 4px 0 0;

    &.gif {
      width: 24px;
      background-color: #EEE;
      padding: 0 2px;
      margin: 0;
    }

    &.images {
      width: 16px;
      background-color: #EEE;
      padding: 2px 4px;
      margin: 0;
    }
  }
`;

const LastMessage = ({ id }) => {
  const me = useSelector(authSelectors.getMe(), shallowEqual);
  const lastMessage = useSelector(messengerSelectors.getLastMessageByMessengerId(id));
  const lastMessageState = useSelector(
    messengerSelectors.getMessageState(lastMessage ? lastMessage.id : null),
  );
  const isTyping = useSelector(messengerSelectors.getIsTyping(id));

  const lastMessageOutgoing = lastMessage && lastMessage.authorId === me.id;

  if (isTyping || !lastMessage) return null;

  const { media } = lastMessage;
  const renderLastMessageContent = () => {
    if (media && media.gif) return <Gif className="gif" color="#666" />;
    if (media && media.images.length > 0) return <ImageMultiple className="images" color="#666" />;
    return <span>{lastMessage.rawContent}</span>;
  };

  return (
    <LastMessageWrapper>
      {lastMessageOutgoing && <ChatStateIcon state={lastMessageState} />}
      {renderLastMessageContent()}
    </LastMessageWrapper>
  );
};

LastMessage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default React.memo(LastMessage);
