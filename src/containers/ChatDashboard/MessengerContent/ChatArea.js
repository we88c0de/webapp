import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as userActions from 'state/users/actions';
import * as messengerSelectors from 'state/messengers/selectors';

import Messages from '../Content/Messages';
import EditMessage from '../Content/ComposeMessage/EditMessage';
import Blocked from './Blocked';
import ComposeMessage from '../Content/ComposeMessage';
import RequestChat from './RequestChat';
import WaitingForApproval from './WaitingForApproval';
import ResolveRequest from './ResolveRequest';

const Wrapper = styled.div`
  height: 100%;
  flex: 1;
  align-self: center;
  display: flex;
  overflow: auto;
`;

const ChatArea = ({ id, participant }) => {
  const dispatch = useDispatch();

  const needsRequest = useSelector(messengerSelectors.needsRequest(id));
  const isWaitingForApproval = useSelector(messengerSelectors.isWaitingForApproval(id));
  const isBlocked = useSelector(messengerSelectors.isBlocked(id));
  const isRejected = useSelector(messengerSelectors.isRejected(id));
  const needsResolveRequest = useSelector(messengerSelectors.needsResolveRequest(id));
  const requestMessage = useSelector(messengerSelectors.getRequestMessage(id));
  const userId = useSelector(messengerSelectors.getUserId(id));
  const isEditing = useSelector(messengerSelectors.isEditing(id));

  if (!isBlocked) {
    if (needsRequest) {
      return <Wrapper><RequestChat id={id} participant={participant} /></Wrapper>;
    }

    if (isWaitingForApproval) {
      return (
        <Wrapper><WaitingForApproval message={requestMessage} /></Wrapper>
      );
    }

    if (needsResolveRequest) {
      const requester = dispatch(userActions.fetchData(userId));

      return (
        <Wrapper>
          <ResolveRequest
            id={id}
            message={requestMessage}
            requester={requester}
            rejected={isRejected}
          />
        </Wrapper>
      );
    }
  }

  return (
    <>
      <Messages key={`chat-messages-${id}`} type="messenger" id={id} />
      <div>
        {!isBlocked ? (
          <>
            {isEditing
              ? <EditMessage key={`compose-messenger-edit-${id}`} type="messenger" id={id} />
              : <ComposeMessage key={`compose-messenger-${id}`} type="messenger" id={id} />
            }
          </>
        ) : (
          <Blocked />
        )}
      </div>
    </>
  );
};

ChatArea.propTypes = {
  id: PropTypes.string.isRequired,
  participant: PropTypes.shape({
    pronoun: PropTypes.string.isRequired,
  }).isRequired,
};

const equality = (prevProps, nextProps) => (
  prevProps.id === nextProps.id
  && prevProps.participant.id === nextProps.participant.id
  && prevProps.participant.loading === nextProps.participant.loading
);
export default React.memo(ChatArea, equality);
