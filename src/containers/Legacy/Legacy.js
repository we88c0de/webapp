import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useOnMount, useTitle } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Loading from 'components/Loading';

import { LEGACY_URL } from '../../constants';

const LegacyContainer = styled.div`
  width: 100%;
  height: 100%;

  @media(max-width: 767px) {
    margin-top: 64px;
  }

  iframe {
    width: 100%;
    height: 100%;
    border: 0;
    display: none;
  }
`;

const Legacy = ({ location, history }) => {
  const dispatch = useDispatch();
  useTitle();

  const me = useSelector(authSelectors.getMe(), shallowEqual);
  const legacy = useRef();

  const [loading, setLoading] = useState(true);

  const tokenQS = me ? me.jwt : '';

  useOnMount(() => {
    dispatch(appActions.uiLeftColumn(true));

    window.addEventListener('message', (event) => {
      if (!event.origin.includes(LEGACY_URL)) return;

      if (event.data.type === 'link') {
        if (event.data.target.includes(LEGACY_URL)) {
          history.push(event.data.target.replace(LEGACY_URL, ''));
        }
      }
    });

    const onLoad = () => {
      setLoading(false);
      legacy.current.style.display = 'block';
    };

    legacy.current.addEventListener('load', onLoad);

    return () => legacy.current.removeEventListener('load', onLoad);
  });

  const path = location.pathname.replace(/^\/@/, '/');

  return (
    <LegacyContainer>
      {loading && <Loading />}
      <iframe ref={legacy} title="Legacy" src={`${LEGACY_URL}${path}?token=${tokenQS}`} />
    </LegacyContainer>
  );
};

Legacy.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default withRouter(Legacy);
