import React from 'react';
import PropTypes from 'prop-types';
import { EditorState, convertFromRaw } from 'draft-js';

import ReadOnlyEditor from 'components/Editor/ReadOnlyEditor';
import ParsedContent from 'components/ParsedContent';

const Body = ({ content, rawContent }) => {
  try {
    // OLD STUFF

    const c = JSON.parse(content);
    // Fix for old replies
    // eslint-disable-next-line no-param-reassign
    c.blocks.filter(x => x.entityRanges.length > 0).forEach((x) => { x.text = x.text.replace(/📷/g, ' '); });
    Object.keys(c.entityMap).forEach((key) => {
      if (c.entityMap[key].type === 'IMAGE') {
        c.entityMap[key].type = 'IMG';
        c.entityMap[key].mutability = 'IMMUTABLE';
      }
    });

    return (
      <ReadOnlyEditor
        content={EditorState.createWithContent(convertFromRaw(c))}
      />
    );
  } catch (error) {
    // NEW STUFF
    return (
      <ParsedContent content={rawContent} />
    );
  }
};

Body.propTypes = {
  content: PropTypes.string.isRequired,
  rawContent: PropTypes.string.isRequired,
};

Body.defaultProps = {
};

export default Body;
