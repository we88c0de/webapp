import React, {
  useCallback,
  useState,
  useRef,
  useContext,
} from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import VisibilitySensor from 'react-visibility-sensor';
import moment from 'moment';
import { Link } from 'react-router-dom';

import Api from 'state/api';
import { useTranslation } from 'hooks';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import { Reply } from 'components/Icons';
import EmptyState from 'components/EmptyState';
import Ad from 'components/Ad';

import ThreadWrapper from './UI/ThreadWrapper';
import Title from './UI/Title';
import Subtitle from './UI/Subtitle';
import BodyWrapper from './UI/BodyWrapper';
import BodyShadow from './UI/BodyShadow';
import Body from './Body';
import locales from '../i18n';

const Replies = ({ userId }) => {
  const { t } = useTranslation(locales);
  const theme = useContext(ThemeContext);

  const [replies, setReplies] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);
  const lastReplyDate = useRef(null);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      const params = { authorId: userId };
      if (lastReplyDate.current) {
        params.before = lastReplyDate.current;
      }

      const { data } = await Api.req.get('/communities/replies', { params });

      if (!data.length) {
        setFullyLoaded(true);
      } else {
        setReplies(prevReplies => [
          ...prevReplies,
          ...data,
        ]);
        lastReplyDate.current = data[data.length - 1].createdAt;
      }
    }
  }, [userId]);

  return (
    <div>
      <div>
        {replies.map((reply, index) => (
          <React.Fragment key={`reply-${reply.id}`}>
            <Link to={`/+${reply.community.slug}/${reply.thread.slug}`}>
              <ThreadWrapper>
                <div className="header">
                  <Title>
                    <Reply color={theme.colors.secondary} />
                    {reply.thread.title}
                  </Title>
                  <Subtitle>{`${t('In')} ${reply.community.name} • ${t('At')} ${moment(reply.createdAt).format(t('date.format'))}`}</Subtitle>
                </div>
                <BodyWrapper>
                  <Body entityId={reply.id} content={reply.content} rawContent={reply.rawContent} />
                  <BodyShadow />
                </BodyWrapper>
              </ThreadWrapper>
            </Link>

            <Ad id="Profile Reply" data={{ index }} />
          </React.Fragment>
        ))}
      </div>

      {!fullyLoaded && (
        <SpinnerWrapper>
          <VisibilitySensor onChange={load} partialVisibility>
            <Spinner color="#999" />
          </VisibilitySensor>
        </SpinnerWrapper>
      )}

      {fullyLoaded && replies.length === 0 && (
        <EmptyState subtitle={t('No replies to show')} />
      )}
    </div>
  );
};

Replies.propTypes = {
  userId: PropTypes.number.isRequired,
};

Replies.defaultProps = {
};

export default Replies;
