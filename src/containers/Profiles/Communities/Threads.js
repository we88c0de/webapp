import React, { useCallback, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';
import moment from 'moment';
import { Link } from 'react-router-dom';

import Api from 'state/api';
import { useTranslation } from 'hooks';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import EmptyState from 'components/EmptyState';
import Ad from 'components/Ad';

import ThreadWrapper from './UI/ThreadWrapper';
import Title from './UI/Title';
import Subtitle from './UI/Subtitle';
import BodyWrapper from './UI/BodyWrapper';
import BodyShadow from './UI/BodyShadow';
import Body from './Body';
import { THREADS_LIST_LIMIT } from '../../../constants';
import locales from '../i18n';

const Threads = ({ userId }) => {
  const { t } = useTranslation(locales);

  const [threads, setThreads] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);
  const lastThreadDate = useRef(null);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      const params = { authorId: userId };
      if (lastThreadDate.current) {
        params.before = lastThreadDate.current;
      }

      const { data } = await Api.req.get('/communities/threads', { params });

      if (data.length < THREADS_LIST_LIMIT) {
        setFullyLoaded(true);
      }
      if (data.length > 0) {
        setThreads(prevThreads => [
          ...prevThreads,
          ...data,
        ]);
        lastThreadDate.current = data[data.length - 1].createdAt;
      }
    }
  }, [userId]);

  return (
    <div>
      <div>
        {threads.map((thread, index) => (
          <React.Fragment key={`thread-${thread.id}`}>
            <Link to={`/+${thread.community.slug}/${thread.slug}`}>
              <ThreadWrapper>
                <div className="header">
                  <Title>{thread.title}</Title>
                  <Subtitle>{`${t('In')} ${thread.community.name} • ${t('At')} ${moment(thread.createdAt).format(t('date.format'))}`}</Subtitle>
                </div>
                <BodyWrapper>
                  <Body
                    entityId={thread.id}
                    content={thread.content}
                    rawContent={thread.rawContent}
                  />
                  <BodyShadow />
                </BodyWrapper>
              </ThreadWrapper>
            </Link>

            <Ad id="Profile Thread" data={{ index, userId }} />
          </React.Fragment>
        ))}
      </div>

      {!fullyLoaded && (
        <SpinnerWrapper>
          <VisibilitySensor onChange={load} partialVisibility>
            <Spinner color="#999" />
          </VisibilitySensor>
        </SpinnerWrapper>
      )}

      {fullyLoaded && threads.length === 0 && (
        <EmptyState subtitle={t('No threads to show')} />
      )}
    </div>
  );
};

Threads.propTypes = {
  userId: PropTypes.number.isRequired,
};

Threads.defaultProps = {
};

export default Threads;
