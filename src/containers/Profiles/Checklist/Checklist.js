import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import Api from 'state/api';
import { useTranslation } from 'hooks';

import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import EmptyState from 'components/EmptyState';

import ChecklistWrapper from './Wrapper';
import NoAccess from './NoAccess';
import { LEGACY_URL } from '../../../constants';
import locales from '../i18n';

const Checklist = () => {
  const { t } = useTranslation(locales);
  const params = useParams();

  const [checklist, setChecklist] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await Api.req.get(`${LEGACY_URL}/api/users/${params.username}/checklist`);
        setChecklist(data.results);
      } catch (e) {
        setError(e.response.status);
      }
    };

    fetch();
  }, [params.username]);

  if (error === 403) return <NoAccess />;
  if (error) return <ChecklistWrapper><EmptyState title={t('You can\'t access this checklist at this moment')} subtitle={t('Try again later')} /></ChecklistWrapper>;
  if (checklist === null) return <SpinnerWrapper><Spinner color="#999" /></SpinnerWrapper>;

  const allItemsCount = checklist.reduce((acc, current) => (
    acc + current.items.filter(i => !!i.selection).length
  ), 0);
  if (allItemsCount === 0) return <EmptyState subtitle={t('This checklist is empty')} />;

  return (
    <ChecklistWrapper>
      {checklist.map((section) => {
        const items = section.items.filter(i => !!i.selection);

        if (!items.length) return null;
        return (
          <div key={`checklist-category-${section.category.id}`}>
            <h3>{section.category.name}</h3>
            <table>
              <thead>
                <tr>
                  <th />
                  <th>{t('Experience')}</th>
                  <th>{t('Interest')}</th>
                </tr>
              </thead>
              <tbody>
                {items.map(item => (
                  <tr key={`checklist-item-${item.id}`}>
                    <td>{item.name}</td>
                    <td>{item.selection.experience_text}</td>
                    <td>{item.selection.interest_text}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        );
      })}
    </ChecklistWrapper>
  );
};

Checklist.propTypes = {
};

Checklist.defaultProps = {
};

export default Checklist;
