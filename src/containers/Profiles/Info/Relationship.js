import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import UserLink from 'components/UserLink';
import UserDisplayName from 'components/UserDisplayName';

import locales from '../i18n';

const RelationshipWrapper = styled.div`
  a {
    ${props => props.theme.css.link};
  }
`;
RelationshipWrapper.displayName = 'RelationshipWrapper';

const Relationship = ({ data: { related_to: userId, type } }) => {
  const { t } = useTranslation(locales);

  return (
    <RelationshipWrapper>
      <span>{type.text}</span>
      {` ${t('global:of')} `}
      <UserLink userId={userId}><UserDisplayName userId={userId} /></UserLink>
    </RelationshipWrapper>
  );
};

Relationship.propTypes = {
  data: PropTypes.shape({
    related_to: PropTypes.number.isRequired,
    type: PropTypes.object.isRequired,
  }).isRequired,
};

Relationship.defaultProps = {
};

export default Relationship;
