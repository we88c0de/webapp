import React from 'react';
import moment from 'moment';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import Loading from 'components/Loading';
import Ad from 'components/Ad';

import Relationhip from './Relationship';
import locales from '../i18n';

const Table = styled.table`
  padding: 16px;
  border-collapse: separate;
  border-spacing: 0px 12px;
`;
Table.displayName = 'Table';

const Title = styled.td`
  font-weight: 600;
  padding: 4px 16px 4px 0;
  white-space: nowrap;
  vertical-align: top;
`;
Title.displayName = 'Title';

const Description = styled.td`
  word-break: break-word;

  p:first-child {
    margin-top: 0;
  }
`;
Description.displayName = 'Description';

const Online = styled.span`
  color: green;
`;
Online.displayName = 'Online';

const Info = () => {
  const { t } = useTranslation(locales);
  const params = useParams();

  const userId = useSelector(userSelectors.getByUsername(params.username));
  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const regdate = useSelector(userSelectors.getRegdate(userId));
  const lastlogin = useSelector(userSelectors.getLastLogin(userId));
  const work = useSelector(userSelectors.getWork(userId));
  const about = useSelector(userSelectors.getAboutMe(userId));
  const pronoun = useSelector(userSelectors.getPronoun(userId));
  const relationships = useSelector(userSelectors.getRelationships(userId));
  const isOnline = useSelector(userSelectors.isOnline(userId));
  const isFullyLoaded = useSelector(userSelectors.isFullyLoaded(userId));

  return (
    <>
      <Table>
        <tbody>

          {!isOrganization && (
            <tr>
              <Title>{t('Pronoun')}</Title>
              <Description>{t(`global:PRONOUN.${pronoun}`)}</Description>
            </tr>
          )}
          <tr>

            <Title>{t('Registered', { context: pronoun })}</Title>
            <Description>
              {moment(regdate).format(t('date.format'))}
            </Description>
          </tr>

          {!isOrganization && (
            <tr>
              <Title>{t('Last seen')}</Title>
              <Description>
                {isOnline ? (
                  <Online>Online</Online>
                ) : (
                  moment(lastlogin).format(t('date.format'))
                )}
              </Description>
            </tr>
          )}

          {work && (
            <tr>
              <Title>{t('Occupation')}</Title>
              <Description>
                {work}
              </Description>
            </tr>
          )}

          {!isOrganization && relationships && relationships.length > 0 && (
            <tr>
              <Title>{t('Relationships')}</Title>
              <Description>
                {relationships.map(relationship => <Relationhip key={`relationship-${relationship.id}`} data={relationship} />)}
              </Description>
            </tr>
          )}

          {about && (
            <>
              <tr>
                <Title>{t('About me')}</Title>
              </tr>
              <tr>
                <Description colSpan="2">
                  {/* eslint-disable-next-line react/no-array-index-key */}
                  {about.trim().split('\n').map((par, i) => <p key={`about-me-${i}`}>{par}</p>)}
                </Description>
              </tr>
            </>
          )}

          {!isFullyLoaded && (
            <tr>
              <td colSpan="2">
                <Loading />
              </td>
            </tr>
          )}
        </tbody>
      </Table>

      <Ad id="Profile Info Bottom" divider="top" data={{ userId }} />
    </>
  );
};

Info.propTypes = {
};

Info.defaultProps = {
};

export default Info;
