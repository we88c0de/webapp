import React, { useState, useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as feedActions from 'state/feed/actions';

import Publication from 'components/Publication';
import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import EmptyState from 'components/EmptyState';
import Ad from 'components/Ad';

import locales from '../i18n';

const Publications = ({ onlyMedia }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const params = useParams();
  const userId = useSelector(userSelectors.getByUsername(params.username));

  const [publicationIds, setPublicationIds] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);

  const load = useCallback(async (isVisible) => {
    if (isVisible && !isLoading.current) {
      const ids = await dispatch(feedActions.loadByAuthor(
        userId, publicationIds.length, onlyMedia,
      ));

      if (!ids.length) {
        setFullyLoaded(true);
      } else {
        setPublicationIds(prevPublications => [
          ...prevPublications,
          ...ids,
        ]);
      }
    }
  }, [userId, publicationIds.length, dispatch, onlyMedia]);

  return (
    <div>
      <div>
        {publicationIds.map((publicationId, index) => (
          <React.Fragment key={`pub-${publicationId}`}>
            <Publication publicationId={publicationId} />
            <Ad id="Profile Feed" data={{ index, userId }} />
          </React.Fragment>
        ))}
      </div>

      {!fullyLoaded && (
        <SpinnerWrapper>
          <VisibilitySensor onChange={load} partialVisibility>
            <Spinner color="#999" />
          </VisibilitySensor>
        </SpinnerWrapper>
      )}

      {fullyLoaded && publicationIds.length === 0 && (
        <EmptyState subtitle={t('No publications to show')} />
      )}
    </div>
  );
};

Publications.propTypes = {
  onlyMedia: PropTypes.bool,
};

Publications.defaultProps = {
  onlyMedia: false,
};

export default Publications;
