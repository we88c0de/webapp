import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

const LocationContainer = styled.div`
  font-size: 16px;
  color: #a7a7a7;
  margin: 4px 0 16px;
  text-align: center;

  img {
    height: 16px;
    margin-right: 8px;
  }
`;
LocationContainer.displayName = 'LocationContainer';

const Location = ({ userId }) => {
  const user = useSelector(userSelectors.getById(userId), shallowEqual);
  const isOrganization = useSelector(userSelectors.isOrganization(userId));

  if (isOrganization) return null;
  return (
    <LocationContainer>
      {user.countryISO && <img src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${user.countryISO.toLowerCase()}.png`} alt={user.country} />}
      {user.location}
    </LocationContainer>
  );
};

Location.propTypes = {
  userId: PropTypes.number.isRequired,
};

Location.defaultProps = {
};

export default Location;
