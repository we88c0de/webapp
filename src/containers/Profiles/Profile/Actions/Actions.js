import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import Own from './Own';
import Others from './Others';

const Actions = ({ userId }) => {
  const me = useSelector(authSelectors.getMe(), shallowEqual);

  if (!userId || !me) return null;
  if (userId === me.id) return <Own userId={userId} />;
  return <Others userId={userId} />;
};

Actions.propTypes = {
  userId: PropTypes.number.isRequired,
};

Actions.defaultProps = {
};

export default Actions;
