import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as userSelectors from 'state/users/selectors';

import { DotsVertical } from 'components/Icons';
import { Action } from 'components/Header';
import Menu, { Item as MenuItem } from 'components/Menu';

import UnknowModal from '../Modals/Unknow';
import BlockModal from '../Modals/Block';
import UnblockModal from '../Modals/Unblock';
import ReportModal from '../Modals/Report';
import locales from '../../i18n';

const Actions = ({ userId }) => {
  const { t } = useTranslation(locales);

  const pronoun = useSelector(userSelectors.getPronoun(userId));
  const knowing = useSelector(authSelectors.doIKnow(userId));
  const blocked = useSelector(authSelectors.isBlocked(userId));

  const [showingContextMenu, openContextMenu, closeContextMenu] = useOpenClose();
  const [showingUnknowModal, openUnknowModal, closeUnknowModal] = useOpenClose();
  const [showingBlockModal, openBlockModal, closeBlockModal] = useOpenClose();
  const [showingUnblockModal, openUnblockModal, closeUnblockModal] = useOpenClose();
  const [showingReportModal, openReportModal, closeReportModal] = useOpenClose();

  return (
    <>
      <Action onClick={openContextMenu} pressed={showingContextMenu}><DotsVertical className="more-actions" color="#AAA" /></Action>
      <Menu open={showingContextMenu} onClose={closeContextMenu} className="nouserlink">
        {knowing && <MenuItem onClick={openUnknowModal}>{t('Remove as known', { context: pronoun })}</MenuItem>}
        {!blocked && <MenuItem onClick={openBlockModal}>{t('Block')}</MenuItem>}
        {blocked && <MenuItem onClick={openUnblockModal}>{t('Unblock')}</MenuItem>}
        <MenuItem onClick={openReportModal}>{t('Report')}</MenuItem>
      </Menu>

      {/* Modals */}
      {showingUnknowModal && <UnknowModal userId={userId} close={closeUnknowModal} />}
      {showingBlockModal && <BlockModal userId={userId} close={closeBlockModal} />}
      {showingUnblockModal && <UnblockModal userId={userId} close={closeUnblockModal} />}
      {showingReportModal && <ReportModal userId={userId} close={closeReportModal} />}
    </>
  );
};

Actions.propTypes = {
  userId: PropTypes.number.isRequired,
};

Actions.defaultProps = {
};

export default Actions;
