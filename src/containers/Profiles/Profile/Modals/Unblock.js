import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import * as appActions from 'state/app/actions';
import * as authActions from 'state/auth/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from '../../i18n';

const UnblockModal = ({ userId, close }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const onConfirm = async () => {
    try {
      setLoading(true);
      await dispatch(authActions.unblock(userId));
      dispatch(appActions.addToast(t('User unblocked')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    close();
  };

  return (
    <Modal
      title={t('Unblock')}
      onCancel={close}
      actions={[(
        <Button
          key="unblock-confirm"
          onClick={onConfirm}
          loading={loading}
        >
          {t('global:Confirm')}
        </Button>
      )]}
    >
      {t('Are you sure you want to unblock this person?')}
    </Modal>
  );
};

UnblockModal.propTypes = {
  userId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

UnblockModal.defaultProps = {
};

export default UnblockModal;
