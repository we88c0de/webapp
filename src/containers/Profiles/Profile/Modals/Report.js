import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import * as appActions from 'state/app/actions';
import * as authActions from 'state/auth/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Radio from 'components/Forms/Radio';
import Toggle from 'components/Toggle';

import locales from '../../i18n';
import { REPORT_REASONS } from '../../../../constants';

const Report = ({ userId, close }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [selectedReason, setSelectedReason] = useState(null);
  const [block, setBlock] = useState(true);

  const onReasonChange = useCallback(reason => () => {
    setSelectedReason(reason);
  }, []);

  const onBlockChange = useCallback(() => {
    setBlock(b => !b);
  }, []);

  const onConfirm = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.report(userId, selectedReason, block));
      dispatch(appActions.addToast(t('User reported')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    close();
  }, [dispatch, close, t, userId, selectedReason, block]);

  return (
    <Modal
      title={t('Report')}
      onCancel={close}
      actions={[(
        <Button
          key="report-confirm"
          onClick={onConfirm}
          loading={loading}
          disabled={selectedReason === null}
        >
          {t('global:Confirm')}
        </Button>
      )]}
    >
      {Object.values(REPORT_REASONS).map(reason => (
        <Radio
          key={`report-reason-${reason}`}
          label={t(reason)}
          onSelect={onReasonChange(reason)}
          selected={selectedReason === reason}
        />
      ))}

      <Toggle
        label={t('Also block this user')}
        onChange={onBlockChange}
        active={block}
        position="left"
      />
    </Modal>
  );
};

Report.propTypes = {
  userId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

Report.defaultProps = {
};

export default Report;
