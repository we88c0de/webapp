import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CounterContainer = styled.div`
  flex: 1;
  cursor: pointer;

  &:hover {
    background-color: rgba(0, 0, 0, 0.1);
  }
`;
CounterContainer.displayName = 'CounterContainer';

const Value = styled.div`
  font-size: 24px;
  font-weight: 500;
`;
Value.displayName = 'Value';

const Label = styled.div`
  text-transform: uppercase;
  font-size: 12px;
  color: #a7a7a7;
`;
Label.displayName = 'Label';

const Counter = ({ label, value, onClick }) => (
  <CounterContainer onClick={onClick}>
    <Value>{value}</Value>
    <Label>{label}</Label>
  </CounterContainer>
);

Counter.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  onClick: PropTypes.func,
};

Counter.defaultProps = {
  onClick: null,
};

export default Counter;
