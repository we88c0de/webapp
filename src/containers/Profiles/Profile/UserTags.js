import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

const UserTagsWrapper = styled.div`
  margin-bottom: 32px !important;
`;
UserTagsWrapper.displayName = 'UserTagsWrapper';

const UserTag = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.9);
  border-radius: 4px;
  padding: 4px 8px;
  background: rgba(255, 255, 255, 0.2);
  margin: 0 8px;
`;
UserTag.displayName = 'UserTag';

const UserTags = ({ userId }) => {
  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const role = useSelector(userSelectors.getRole(userId));
  const orientation = useSelector(userSelectors.getOrientation(userId));

  if (isOrganization || (!role && !orientation)) return null;

  return (
    <UserTagsWrapper>
      {role && <UserTag>{role}</UserTag>}
      {orientation && <UserTag>{orientation}</UserTag>}
    </UserTagsWrapper>
  );
};

UserTags.propTypes = {
  userId: PropTypes.number.isRequired,
};

UserTags.defaultProps = {
};

export default UserTags;
