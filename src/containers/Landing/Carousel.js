import React, { useRef, useEffect } from 'react';
import styled from 'styled-components';

import screens from './screens.jpg';

const CarouselComponent = styled.div`
  background: url(${screens});
  background-size: cover;
  margin: 30px auto;
  height: 400px;
  width: 800px;
  transition: 0.5s;

  @media(max-width: 767px) {
    height: 160px;
    width: 320px;
  }
`;

const Carousel = () => {
  const carouselEl = useRef(null);

  useEffect(() => {
    const SCREENS = 5;
    let carouselIndex = 0;
    let carouselInterval;

    if (carouselEl.current) {
      const WIDTH = carouselEl.current.offsetWidth;
      carouselInterval = setInterval(() => {
        carouselIndex = (carouselIndex + 1) % SCREENS;
        carouselEl.current.style.backgroundPositionX = `${(carouselIndex * WIDTH * -1)}px`;
      }, 2500);
    }

    return () => clearInterval(carouselInterval);
  }, [carouselEl]);

  return (
    <CarouselComponent ref={carouselEl} />
  );
};

Carousel.propTypes = {
};

Carousel.defaultProps = {
};

export default Carousel;
