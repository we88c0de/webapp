import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import { useInputValue, useTranslation, useFocusOnMount } from 'hooks';

import AuthLayout from 'components/AuthLayout';
import Input from 'components/Forms/InputSimple';
import Button from 'components/Button';

import PasswordRecovered from './PasswordRecovered';
import locales from './i18n';

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const emailEl = useFocusOnMount();
  const [recovering, setRecovering] = useState(false);
  const [recovered, setRecovered] = useState(false);
  const email = useInputValue('');

  const recover = async () => {
    try {
      setRecovering(true);
      await dispatch(authActions.recover(email.value));
      setRecovered(true);
    } catch (err) {
      let error = err.message;
      if (err.response) {
        // eslint-disable-next-line
        error = err.response.data.error;
      } else if (err.request) {
        error = err.request;
      }

      dispatch(appActions.addError(error));
      setRecovering(false);
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') recover();
  };

  if (recovered) return <PasswordRecovered />;

  return (
    <AuthLayout
      title={t('Password recovery')}
      subtitle={t('Enter your e-mail and we will send you the instructions to reset your password.')}
    >
      <Input
        ref={emailEl}
        placeholder={t('E-mail')}
        {...email}
        disabbled={recovering}
        onKeyPress={handleKeyPress}
        withBorder
      />

      <Button
        onClick={recover}
        loading={recovering}
        content={t('Recover')}
      />
    </AuthLayout>
  );
};

export default ForgotPassword;
