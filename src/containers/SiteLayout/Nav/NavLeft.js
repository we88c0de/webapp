import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as appSelectors from 'state/app/selectors';

import HomeLink from './HomeLink';
import CommunitiesLink from './CommunitiesLink';
import ChatLink from './ChatLink';

const NavLeftComponent = styled.ul`
  display: flex;
  align-items: center;
  flex: 1;
  height: 100%;
  margin: 0;
`;

const NavLeft = () => {
  const active = useSelector(appSelectors.isNavLeftActive());

  return (
    <NavLeftComponent active={active}>
      <HomeLink />
      <CommunitiesLink />
      <ChatLink />
    </NavLeftComponent>
  );
};

NavLeft.propTypes = {
};

NavLeft.defaultProps = {
};

export default React.memo(NavLeft);
