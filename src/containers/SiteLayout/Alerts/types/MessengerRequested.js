import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import UserDisplayName from 'components/UserDisplayName';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const MessengerRequested = ({ author, messengerId, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={author.avatar} to={`/chat/messengers/${messengerId}`} read={read}>
      <strong><UserDisplayName userId={author.id} /></strong>
      {' '}
      {t('has sent you a chat request')}
    </AlertContainer>
  );
};

MessengerRequested.propTypes = {
  read: PropTypes.bool.isRequired,
  author: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    displayname: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
  messengerId: PropTypes.string.isRequired,
};

export default MessengerRequested;
