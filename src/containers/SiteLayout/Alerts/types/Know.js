import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const Know = ({ user, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={user.avatar} to={`/@${user.username}`} read={read}>
      {user.displayname}
      {' '}
      {t('indicated that he knows you')}
    </AlertContainer>
  );
};

Know.propTypes = {
  read: PropTypes.bool.isRequired,
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    displayname: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default Know;
