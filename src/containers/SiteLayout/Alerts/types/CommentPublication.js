import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import UserDisplayName from 'components/UserDisplayName';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const CommentPublication = ({
  comment,
  author,
  read,
}) => {
  const { t } = useTranslation(locales);
  const me = useSelector(authSelectors.getMe(), shallowEqual);

  return (
    <AlertContainer image={author.avatar} to={`/publications/${comment.publication.id}`} read={read}>
      <strong><UserDisplayName userId={author.id} /></strong>
      {' '}
      {comment.publication.authorId === me.id ? t('added a comment to your publication') : t('added a comment to a publication you are following')}
      {': '}
      {comment.rawContent}
    </AlertContainer>
  );
};

CommentPublication.propTypes = {
  comment: PropTypes.shape({
    id: PropTypes.string.isRequired,
    rawContent: PropTypes.string,
    publication: PropTypes.shape({
      id: PropTypes.string.isRequired,
      authorId: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  author: PropTypes.shape({
    id: PropTypes.number.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
  read: PropTypes.bool.isRequired,
};

export default CommentPublication;
