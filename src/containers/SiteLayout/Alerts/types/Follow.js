import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import UserDisplayName from 'components/UserDisplayName';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const Follow = ({ user, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={user.avatar} to={`/@${user.username}`} read={read}>
      <strong><UserDisplayName userId={user.id} /></strong>
      {' '}
      {t('is now following you')}
    </AlertContainer>
  );
};

Follow.propTypes = {
  read: PropTypes.bool.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    displayname: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default Follow;
