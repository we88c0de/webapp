import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const RelationshipAccepted = ({
  user,
  read,
}) => {
  const { t } = useTranslation(locales);
  if (!user) return null;

  return (
    <AlertContainer image={user.avatar} to="/users/relationships" read={read}>
      {t('relationship.accepted', { user: user.displayname })}
    </AlertContainer>
  );
};

RelationshipAccepted.propTypes = {
  read: PropTypes.bool.isRequired,
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    displayname: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default RelationshipAccepted;
