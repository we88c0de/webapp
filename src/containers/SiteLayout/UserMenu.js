import React, { useState, useCallback } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { useTranslation } from 'hooks';
// import * as appSelectors from 'state/app/selectors';
// import * as appActions from 'state/app/actions';
import * as authSelectors from 'state/auth/selectors';

import Avatar from 'components/UserAvatar';
import Menu, { Item } from 'components/Menu';
// import Toggle from 'components/Toggle';
import LanguageSelector from 'components/LanguageSelector';

import NavItem from './Nav/Item';
import locales from './i18n';

const NavAvatar = styled.div`
  width: 40px;
  margin: 0 0 0 10px;
  cursor: pointer;
  position: relative;

  img {
    /* border: 3px solid #c13e31; */
  }

  @media(max-width: 767px) {
    width: 32px;

    .avatar {
      width: 32px;
      height: 32px;

      img {
        width: 32px;
        height: 32px;
      }
    }
  }
`;

const UserMenu = React.memo(() => {
  const { t } = useTranslation(locales);
  // const dispatch = useDispatch();

  const me = useSelector(authSelectors.getMe(), shallowEqual);

  // const isSafeForWork = useSelector(appSelectors.isSafeForWork());
  const [profileMenuOpened, setProfileMenuOpened] = useState(false);

  const openProfileMenu = useCallback(() => {
    setProfileMenuOpened(true);
  }, []);
  const closeProfileMenu = useCallback(() => {
    setProfileMenuOpened(false);
  }, []);
  // const toggleSafeForWork = useCallback(() => {
  //   dispatch(appActions.toggleSafeForWork());
  // }, [dispatch]);

  if (!me) return null;

  return (
    <NavItem onClick={openProfileMenu} pressed={profileMenuOpened}>
      <NavAvatar>
        <Avatar userId={me.id} size="40px" showOnline={false} />
        <Menu
          maxHeight="auto"
          open={profileMenuOpened}
          onClose={closeProfileMenu}
          className="nav-dropdown"
        >
          {/*
            <Item><Link to="/profile/notifications">{t('global:Notifications')}</Link></Item>
          */}
          <Item><Link to={`/@${me.username}`}>{t('See my profile')}</Link></Item>
          <Item><Link to="/users/edit">{t('Edit my profile')}</Link></Item>
          <Item><Link to="/users/config">{t('Configuration')}</Link></Item>
          <Item><Link to="/users/relationships">{t('Relationships')}</Link></Item>
          <Item><Link to="/users/lists">{t('Contact lists')}</Link></Item>
          <Item><Link to={`/${me.username}/checklist`}>{t('Checklist')}</Link></Item>
          <Item><Link to="/bank">{t('SADEs')}</Link></Item>
          <Item><Link to="/user/notifications">{t('Notifications')}</Link></Item>
          <Item><Link to="/user/bots">{t('My chat bots')}</Link></Item>
          {/*
          <Item>
          <Toggle label={t('Safe for work')} active={isSafeForWork} onChange={toggleSafeForWork} />
          </Item>
          */}
          <Item><LanguageSelector /></Item>
          <Item><Link to="/logout">{t('Logout')}</Link></Item>
        </Menu>
      </NavAvatar>
    </NavItem>
  );
});

export default UserMenu;
