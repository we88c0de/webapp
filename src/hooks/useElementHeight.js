import { useRef, useEffect, useState } from 'react';

const useElementHeight = () => {
  const selectableEl = useRef(null);
  const [elHeight, setElHeight] = useState(0);

  useEffect(() => {
    if (selectableEl.current) {
      setElHeight(selectableEl.current.clientHeight);
    }
  }, [selectableEl]);

  return [elHeight, selectableEl];
};

export default useElementHeight;
