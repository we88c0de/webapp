import { useState } from 'react';

const useOpenclose = (initial = false) => {
  const [isOpen, setIsOpen] = useState(initial);
  const open = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }
    setIsOpen(true);
  };
  const close = () => {
    setIsOpen(false);
  };
  const toggle = () => {
    setIsOpen(currentValue => !currentValue);
  };

  return [isOpen, open, close, toggle];
};

export default useOpenclose;
