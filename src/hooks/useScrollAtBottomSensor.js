/* eslint-disable react-hooks/exhaustive-deps */

import {
  useEffect,
  useRef,
  useState,
} from 'react';

const OFFSET = 60;

const useScrollAtBottomSensor = (condition) => {
  const ref = useRef(null);
  const [isAtBottom, setIsAtBottom] = useState(true);
  const [initialized, setInitialized] = useState(false);

  const elementScroll = (e) => {
    const el = e.target;

    if (!initialized) {
      setInitialized(true);
      el.scrollTo(0, el.scrollHeight);
    } else {
      setIsAtBottom(el.scrollTop + el.clientHeight + OFFSET >= el.scrollHeight);
    }
  };

  useEffect(() => {
    const el = ref.current;
    el.addEventListener('scroll', elementScroll);

    return () => el.removeEventListener('scroll', elementScroll);
  }, [ref, initialized]);

  useEffect(() => {
    if (isAtBottom) {
      const el = ref.current;
      if (el) {
        // Timeout is for send the scroll to the end of the event loop
        setTimeout(() => el.scrollTo(0, el.scrollHeight), 0);
      }
    }
  }, condition);

  return [ref, isAtBottom];
};

export default useScrollAtBottomSensor;
