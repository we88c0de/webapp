import { useTranslation } from 'react-i18next';
import { useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import { USERS_PRONOUNS } from '../constants';

const useTranslationHook = (locale = { namespace: 'global', resources: {} }) => {
  const me = useSelector(authSelectors.getMe(), shallowEqual);

  const { t, i18n } = useTranslation(locale.namespace);
  i18n.addResourceBundle(
    i18n.language,
    locale.namespace,
    (locale.resources[i18n.language] || locale.resources.es || {}),
  );

  const context = me ? me.pronoun : USERS_PRONOUNS.NEUTRAL;

  const tt = (key, opts, ...props) => t(
    key,
    {
      context,
      ...opts,
    },
    ...props,
  );

  return { t: tt, i18n };
};

export default useTranslationHook;
